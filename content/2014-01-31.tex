% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 31.01.2014

\renewcommand{\printfile}{2014-01-31}

\subsection{Dekohärenz als (Teil-)Lösung}

Entscheidend ist die (unvermeidliche) Ankopplung an die Umgebung. Wir haben wieder ein System $\ket{s_i}$ und einen Apparat $\ket{A_i}$ mit Anfangszustand $\ket{A_0}$ und die Umgebung $\ket{E_i}$ mit Anfangszustand $\ket{A_0}$.
%
\begin{align*}
	\ket{\phi_0} = \sum_i c_i \ket{s_i} \otimes \ket{A_0} \otimes \ket{E_0}
	\to
	\ket{\phi_t} = \sum_i c_i \ket{s_i} \otimes \ket{A_i} \otimes \ket{E_i}
\end{align*}
%
Daraus ergibt sich die Dichtematrix
%
\begin{align*}
	\varrho_t = \sum_{i,j} c_j^* c_i \ket{s_i}\bra{s_j} \otimes \ket{A_i}\bra{A_j} \otimes \ket{E_i}\bra{E_j}
\end{align*}
%
Ausspuren der Umgebung
%
\begin{align*}
	\varrho_{s,A}
	&\equiv \tr_E \varrho_t \\
	&= \sum_\alpha \braket{E_\alpha|\varrho_t|E_\alpha} \\
	&= \sum_{i,j} \braket{E_j|E_i} c_j^* c_i \ket{s_i}\bra{s_j} \otimes \ket{A_i}\bra{A_j}
\intertext{falls $\braket{E_j|E_i} = \delta_{ij}$, dann}
	&= \sum_i |c_i|^2 \ket{s_i}\bra{s_i} \otimes \ket{A_i}\bra{A_i}
\end{align*}

Das bedeutet
%
\begin{enumerate}
	\item Die Ankopplung an eine Umgebung mit $\braket{E_i|E_j} = \delta_{ij}$ führt innerhalb der unitären Schrödingerdynamik zu einer Dichtematrix mit reinen Diagonalelementen, d.\,h.\ $|c_i|^2 = p_i$ entspricht den klassischen Wahrscheinlichkeiten (Die Kohärenzen verschwinden).
	\item Die Observablen, die auf $\braket{E_i|E_j} = \delta_{ij}$ führen, sind (praktisch) messbar.
\end{enumerate}

\subsection{Einfaches Modell}

Mehr zu diesem Modell findet man bei \fullcite{PhysRevD.26.1862} und \fullcite{PhysRevA.72.052113}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (0,0) node[left] {$\ket{0}$} -- +(0.5,0);
		\draw (0,0.5) node[left] {$\ket{1}$} -- +(0.5,0);
		\foreach \i in {1,2,3} {
			\draw (\i,0) node (n \i) {} -- node[below] {$\i$} +(0.5,0);
			\draw (\i,0.5) -- +(0.5,0);
		}
		\path (4,0) -- node[below] {$\cdots$} +(0.5,0);
		\draw (5,0) -- node[below] {N} +(0.5,0) node (n N) {};
		\draw (5,0.5) -- +(0.5,0);
		\draw[decorate,decoration=brace] ($(n N)+(0,-0.5)$) -- node[below] {$N$} ($(n 1)+(0,-0.5)$);
	\end{tikzpicture}
	\caption{Zwei-Niveau-System in einem Spin-Bad.}
	\label{fig:2014-01-31-1}
\end{figure}

Der Hamiltonoperator des Zwei-Niveau-Systems im Spin-Bad ist gegeben durch
\begin{align*}
	H
	&= H_S + H_E + H_{SE} \\
	&= 0 + 0 + \frac{1}{2} \sigma_z^{(S)} \otimes \underbrace{\sum_{i=1}^{N} g_i \sigma_z^{(i)}}_{\mathcal{R}}
\end{align*}

\begin{enumerate}
	\item $[\sigma_z^{(S)}, H] = 0$, also ist $\sigma_z^{(S)}$ erhalten.
	\item Die Eigenzustände von $\mathcal{R}$ sind
		\begin{align*}
			\ket{+}\ket{+}\ket{-}\ket{+}\cdots &= \ket{\mu} \; , \quad \text{mit } 0 \leq \mu 2^N - 1 \\
			E_\mu &= \sum_{i=1}^{N} (-1)^{n_i+1} g_i
		\end{align*}
	\item Eigenzustände des Gesamtsystems
		\begin{align*}
			\ket{0} \otimes \ket{\mu}, \ket{1} \otimes \ket{\mu}
		\end{align*}
	\item Allgemeinster Zustand
		\begin{align*}
			\ket{\phi} &= \sum_{\mu = 0}^{2^N - 1} c_\mu \ket{0} \otimes \ket{\mu} + d_\mu \ket{1} \otimes \ket{\mu}
		\end{align*}
	\item Anfangszustand für $t = 0$, faktorisierter Zustand
		\begin{align*}
			\ket{\psi(t=0)}
			&= (a \ket{0} + b \ket{1}) \otimes \sum_\mu c_\mu \ket{\mu} \\
			\ket{\psi(t)}
			&= \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H t} \ket{\psi(t=0)} \\
			&= a \ket{0} \otimes \ket{E_0(t)} + b \ket{1} \otimes \ket{E_1(t)}
		\intertext{mit}
			\ket{E_0(t)} &= \sum_{\mu = 0}^{2^N - 1} c_\mu \mathrm{e}^{\frac{\mathrm{i}}{\hbar} \frac{E_\mu}{2} t} \ket{\mu} \\
			\ket{E_1(t)} &= \sum_{\mu = 0}^{2^N - 1} c_\mu \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} \frac{E_\mu}{2} t} \ket{\mu}
		\end{align*}
	\item
		\begin{itemalign}
			\varrho_t
			&= \ket{\psi(t)}\bra{\psi(t)} \\
			&= \begin{multlined}[t]
				a a^* \ket{E_0(t)}\bra{E_0(t)} \otimes \ket{0}\bra{0} + b b^* \ket{E_1(t)}\bra{E_1(t)} \otimes \ket{1}\bra{1} \\
				+ a b^* \ket{E_0(t)}\bra{E_0(t)} \otimes \ket{0}\bra{1} + a^* b \ket{E_1(t)}\bra{E_0(t)} \otimes \ket{1}\bra{0}
			\end{multlined} && |\;\tr_E \\
			&= \begin{multlined}[t]
				a a^* \ket{0}\bra{0} + b b^* \ket{1}\bra{1} \\
				+ a b^* \braket{E_1(t)|E_0(t)} \ket{0}\bra{1} + a^* b \braket{E_0(t)|E_1(t)} \ket{1}\bra{0}
			\end{multlined}
		\end{itemalign}
	\item
		\begin{itemalign}
			r(t)
			&\equiv \braket{E_1(t)|E_0(t)} \\
			&= \sum_\mu |c_\mu|^2 \mathrm{e}^{\frac{\mathrm{i}}{\hbar} E_\mu t}
		\end{itemalign}
		Dies entspricht der Addition von $2^N$ Vektoren der typischen Länge $\frac{1}{2^N}$ mit Zufallsrichtung in zwei Dimensionen (Random Walk).
		\begin{align*}
			r^2(t)
			&= 4 D t \\
			&= 2 \cdot 2^N \left( \frac{1}{2^N} \right)^2
		\end{align*}
		Ein typisches $r(t) \sim \frac{1}{2^N} = \mathrm{e}^{-\frac{N}{2} \ln 2}$. Das heißt die Kohärenzen zerfallen auf einen Wert $\sim \mathrm{e}^{-\frac{N}{2} \ln 2}$ (exponentiell klein in $N$). (typische Zeitskala $r(t) \sim \mathrm{e}^{- \Gamma^2 t^2}$, $\Gamma = \Gamma(c_\mu,g_i)$)
\end{enumerate}

\minisec{Zeitabhängigkeit für Spezialfall}

\begin{align*}
	\ket{\psi(0)}
	&= (a \ket{0} + b \ket{1}) \otimes (\alpha \ket{0} + \beta \ket{1})^{\otimes N} \\
	E_0(t)
	&= \left[ \alpha \mathrm{e}^{\frac{\mathrm{i}}{\hbar} \frac{g t}{2}} \ket{0} + \beta \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} \frac{g t}{2}} \ket{1} \right]^{\otimes N} \\
	E_1(t)
	&= \left[ \alpha \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} \frac{g t}{2}} \ket{0} + \beta \mathrm{e}^{\frac{\mathrm{i}}{\hbar} \frac{g t}{2}} \ket{1} \right]^{\otimes N} \\
	r(t)
	&= \braket{E_1(t)|E_0(t)} \\
	&= \left( |\alpha|^2 \mathrm{e}^{\frac{\mathrm{i}}{\hbar} g t} + |\beta|^2 \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} g t} \right)^N \\
	&= \Biggl[ \underbrace{(|\alpha|^2 + |\beta|^2)}_{1} \underbrace{\cos\frac{gt}{\hbar}}_{\sim 1 - \frac{g^2 t^2}{2 \hbar^2}} + \mathrm{i} \underbrace{(|\alpha|^2 - |\beta|^2)}_{=\gamma} \sin\frac{gt}{\hbar} \Biggr]^N
\end{align*}
