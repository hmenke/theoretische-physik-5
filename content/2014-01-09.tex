% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 09.01.2014

\renewcommand{\printfile}{2014-01-09}

\section{Fermionische Vielteilchensysteme}

\subsection{Nicht-wechselwirkendes Elektronengas}
\index{Elektronengas!nicht wechselwirkend}

\subsubsection{Fermi-Kugel}
\index{Fermi-Kugel}

Seien $N$ Elektronen mit Spin $1/2$ in einem Kasten mit Volumen $V$ verteilt. Der Grundzustand minimiert die kinetische Energie
\[ \ket{\textnormal{GZ}} = \ket{F} = \prod_{|\bm{k}| < k_F} \prod_{\sigma = \pm 1} c_{\bm{k},\sigma}^\dag \ket{0} \]
$k_F$ ist definiert durch den Radius der Fermikugel im $k$-Raum.
%
\begin{align*}
	N
	&\stackrel{!}{=} 2 \sum_{|\bm{k}| < k_F} 1 \\
	&= 2 V \int_0^{k_F} \frac{\diff^3 k}{(2 \pi)^3} \\
	&= \frac{2 V}{8 \pi^3} 4 \pi \frac{k_F^3}{3} \\
	&= \frac{V k_F^3}{3 \pi^2} \\
	\leadsto
	k_F &= \sqrt[3]{3 \pi^2 n} \; , \quad \text{mit der Dichte } n = \frac{N}{V}
\end{align*}
%
Daher gilt für die \acct{Fermi-Energie}
\[ E_F \equiv \frac{\hbar^2}{2 m} k_F^2 = \frac{\hbar^2}{2 m} (3 \pi^2 n)^{2/3} \]
Die Gesamtenergie des Systems ergibt sich aus Integration über die Energie der einzelnen Elektronen
%
\begin{align*}
	E = \frac{\hbar^2}{2m} \frac{2 V}{(2 \pi)^3} \int_0^{k_F} \diff^3 k\ k^2 = N \frac{3}{5} E_F
\end{align*}
%
In atomaren Einheiten lautet die Fermienergie
%
\begin{align*}
	E_F
	&= \underbrace{\frac{e^2}{2 a_0}}_{\mathrm{Ry}} \frac{1}{r_S^2} \left( \frac{9 \pi}{4} \right)^{2/3} \\
	&\approx \SI{1}{\electronvolt} \approx \num{40}\, \kB T_\textnormal{room} \approx \SI{12000}{\kelvin}
\end{align*}
Für Natrium ist $r_S \approx 4$ (ohne Einheit).

\subsubsection{Teilchen-Loch Transformation}

\begin{theorem}[Definition]
	Wir führen Erzeuger und Vernichter ein mit
	%
	\begin{align*}
		a_{\bm{k},\sigma}^\dag &= c_{\bm{k},\sigma}^\dag \; , \quad \text{falls } |\bm{k}|>k_F \\
		( a_{\bm{k},\sigma} &= c_{\bm{k},\sigma} ) \\
		a_{\bm{k},\sigma}^\dag &= c_{\bm{k},\sigma} \; , \quad \text{falls } |\bm{k}|<k_F \\
		( a_{\bm{k},\sigma} &= c_{\bm{k},\sigma}^\dag ) \text{ erzeugt ein Loch in der Fermikugel}
	\end{align*}
\end{theorem}

Damit gilt
	%
\begin{enumerate}
	\item Die bekannten Kommutatorrelation gelten weiterhin.
	\item $a_{\bm{k},\sigma} \ket{F} = \ket{0}$
\end{enumerate}
	%
Für den Operator $a$ kann das Wick-Theorem angewendet werden. Es bleibt nur einen Kontraktion übrig
	%
\begin{align*}
	\braket{F| a_i a_i^\dag |F} &= 1 \\
	\braket{F| c_{\bm{k},\sigma} c_{\bm{k},\sigma}^\dag |F} &= 1 \; , \quad \text{für } k > k_F \\
	\braket{F| c_{\bm{k},\sigma}^\dag c_{\bm{k},\sigma} |F} &= 1 \; , \quad \text{für } k < k_F
\end{align*}

\subsubsection{Korrelationsfunktion}

\begin{enumerate}
	\item Dichte einer Spinkomponente:
		\begin{align*}
			\braket{w_\sigma(\bm{r})}
			&= \braket{F| \Psi_\sigma^\dag(\bm{r}) \Psi_\sigma(\bm{r}) |F} \\
			&= \frac{1}{V} \sum_{\bm{k},\bm{k}'} \mathrm{e}^{-\mathrm{i} (\bm{k}-\bm{k}') \bm{r}} \braket{F| c_{\bm{k},\sigma}^\dag c_{\bm{k},\sigma} |F} \\
			&= \frac{1}{V} \sum_{\bm{k},\bm{k}'} \delta_{\bm{k},\bm{k}'} \mathrm{e}^{-\mathrm{i} (\bm{k}-\bm{k}') \bm{r}} \Theta(k_F - |\bm{k}|) \\
			&= \frac{1}{(2 \pi)^3} 4 \pi \int_0^{k_F} k^2 \diff k \\
			&= \frac{n}{2}
		\end{align*}

	\item Ein-Teilchen Korrelationsfunktion
		\begin{align*}
			G_{\sigma,\sigma'}^{(1)}(\bm{r},\bm{r}')
			&\equiv \braket{F| \Psi_{\sigma}^\dag(\bm{r}) \Psi_{\sigma'}(\bm{r}') |F} \\
			&= \frac{1}{V} \sum_{\bm{k},\bm{k}'} \mathrm{e}^{-\mathrm{i} \bm{k} \cdot \bm{r} + \mathrm{i} \bm{k}' \cdot \bm{r}'} \braket{F| c_{\bm{k},\sigma}^\dag c_{\bm{k}',\sigma'} |F} \\
			&= \frac{1}{(2 \pi)^3} \int \diff^3 k\ \mathrm{e}^{-\mathrm{i} \bm{k} (\bm{r}-\bm{r}')} \delta_{\sigma,\sigma'} \\
			&= \delta_{\sigma,\sigma'} k_F^3 g(k_F |\bm{r}-\bm{r}'|)
		\end{align*}
		%
		mit
		%
		\begin{align*}
			g(x) &= \frac{1}{2 \pi^2 x^3} (\sin x - x \cos x) =
			\begin{cases}
				\frac{1}{6 \pi^2} & x \ll 1 \\
				- \frac{1}{2 \pi^2} \frac{\cos x}{x^2} & x \gg 1 \\
			\end{cases}
		\end{align*}

		\begin{figure}[htpb]
			\centering
			\begin{tikzpicture}
				\draw[->] (-0.3,0) -- (4,0) node[below] {$x$};
				\draw[->] (0,-1) -- (0,1.4) node[left] {$G_{\sigma,\sigma'}^{(1)}(x)$};
				\draw (0.1,1) -- (-0.1,1) node[left] {$\frac{n}{2}$};
				\draw[MidnightBlue] plot[smooth,samples=40,domain=0:3.8] (\x,{exp(-\x)*cos(2*pi*\x r)});
			\end{tikzpicture}	
			\caption{Graph der Ein-Teilchen Korrelationsfunktion.}
			\label{fig:2014-01-09-1}
		\end{figure}

	\item Zwei-Teilchen Korrelationsfunktion
		\begin{theorem}[Definition]
			\[ \ket{\phi_{\sigma'}(\bm{r}')} \equiv \Psi_{\sigma'}(\bm{r}') \ket{F} \]
			vernichtet ein Elektron am Ort $\bm{r}'$ mit Spin $\sigma'$.
		\end{theorem}
		Die Dichte am Ort $\bm{r}$ in diesem Zustand ist gegeben durch
		%
		\begin{align*}
			G_{\sigma,\sigma'}^{(2)}(\bm{r},\bm{r}')
			&\equiv \braket{\phi_{\sigma'}(\bm{r}')| n_\sigma(\bm{r}) |\phi_{\sigma}(\bm{r})} \\
			&= \braket{F| \Psi_{\sigma'}^\dag(\bm{r}') \Psi_{\sigma}^\dag(\bm{r}) \Psi_{\sigma}(\bm{r}) \Psi_{\sigma'}(\bm{r}') |F}
		\end{align*}
		%
		nach Transformation auf $c_{\bm{k}},c_{\bm{k}}^\dag$ und dann nach $a_{\bm{k}},a_{\bm{k}}^\dag$ kann das Wick-Theorem angewendet werden. Es bleiben danach drei Paare von Kontraktionen übrig. Ausgedrückt in $\Psi$ lautet das Ergebnis.
		%
		\begin{align*}
			G_{\sigma,\sigma'}^{(2)}(\bm{r},\bm{r}')
			&= \aligned[t]
				&\braket{F| \Psi_{\sigma'}^\dag(\bm{r}') \Psi_{\sigma}(\bm{r}) |F} \braket{F|\Psi_{\sigma}(\bm{r}) \Psi_{\sigma'}(\bm{r}') |F} \\
				&- \braket{F| \Psi_{\sigma'}^\dag(\bm{r}') \Psi_{\sigma}(\bm{r}) |F} \braket{F|\Psi_{\sigma}^\dag(\bm{r}) \Psi_{\sigma'}(\bm{r}') |F} \\
				&+ \braket{F| \Psi_{\sigma'}^\dag(\bm{r}') \Psi_{\sigma'}(\bm{r}') |F} \braket{F|\Psi_{\sigma}^\dag(\bm{r}) \Psi_{\sigma}(\bm{r}) |F}
			\endaligned \\
			&= \frac{n^2}{4} - \{ G_{\sigma,\sigma'}^{(1)}(\bm{r},\bm{r}') \}^2 \; , \quad \left( = \frac{n^2}{4} \text{ für } \sigma \neq \sigma' \right)
		\end{align*}

		\begin{figure}[htpb]
			\centering
			\begin{tikzpicture}
				\draw[->] (-0.3,0) -- (4,0) node[below] {$x$};
				\draw[->] (0,-0.3) -- (0,2) node[left] {$G_{\sigma,\sigma'}^{(2)}(x)$};
				\draw (0.1,1) -- (-0.1,1) node[left] {$\frac{n^2}{4}$};
				\draw[MidnightBlue] plot[smooth,samples=40,domain=0:3.8] (\x,{1-exp(-\x)*cos(2*pi*\x r)});
			\end{tikzpicture}
			\caption{Graph der Zwei-Teilchen Korrelationsfunktion.}
			\label{fig:2014-01-09-2}
		\end{figure}
\end{enumerate}

\subsection{Wechselwirkendes Elektronengas}
\index{Elektronengas!wechselwirkend}

\subsubsection{Hamiltonian für Jellium}
\index{Jellium}

Die Elektronen sitzen nun auf einem kontinuierlichen Hintergrund $\varrho(\bm{r})$ aus positiven Ionen.
%
\begin{align*}
	H &= H_\textnormal{ion} + H_\textnormal{el} + H_\textnormal{el-ion}
\end{align*}

\begin{itemize}
	\item 
		\begin{itemalign}
			H_\textnormal{ion}
			&= \frac{e^2}{2} \iint \diff \bm{r} \diff \bm{r}'\ \frac{\varrho(r) \varrho(r')}{|\bm{r}-\bm{r}'|} \\
			&= \frac{e^2}{2} \varrho^2 V \int \diff \bm{r}\ \frac{1}{|\bm{r}|} \sim R^2 \text{ (divergent für $V \to \infty$)}
		\end{itemalign}

	\item
		\begin{itemalign}
			H_\textnormal{el-ion}
			&= - e^2 \sum_{i=1}^{N} \int \diff \bm{r}\ \frac{\varrho(\bm{r})}{|\bm{r}-\bm{r}_i|} \\
			&= - \varrho e^2 \sum_{i=1}^{N} \int \diff \bm{r}\ \frac{1}{V} \Biggl( \sum_{\bm{q} \neq  0} \underbrace{\frac{4 \pi}{q^2} \mathrm{e}^{\mathrm{i} \bm{q} (\bm{r} - \bm{r}_i)}}_{\delta_{\bm{q},0}} + v|_{\bm{q} = 0} \Biggr) \\
			&= - \varrho e^2 N v|_{\bm{q} = 0}
		\end{itemalign}

	\item
		\begin{itemalign}
			H_\textnormal{el}
			&= \sum_{i=1}^{N} \frac{\bm{p}_i^2}{2 m} + \frac{e^2}{2} \sum_{i \neq j} \frac{1}{|\bm{r}_i - \bm{r}_j|} \\
			&= \sum_{i=1}^{N} \frac{\bm{p}_i^2}{2 m} + \frac{e^2}{2} \frac{1}{V} \Biggl(
				\sum_{\bm{q} \neq 0} \sum_{\bm{k},\sigma} \sum_{\bm{p},\sigma'}
				v_\bm{q} c_{\bm{k}+\bm{q},\sigma}^\dag c_{\bm{p}-\bm{q}',\sigma'}^\dag c_{\bm{p},\sigma'} c_{\bm{k},\sigma}
				+ \underbrace{v_{\bm{q}=0} c_{\bm{k},\sigma}^\dag c_{\bm{p},\sigma'}^\dag c_{\bm{p},\sigma'} c_{\bm{k},\sigma}}_{\equiv \mathcal{O}}
			\Biggr)
		\end{itemalign}
		%
		für später
		%
		\begin{align*}
			\braket{F| \mathcal{O} |F}
			&= \Braket{F| \sum_{\bm{k},\sigma} \sum_{\bm{p},\sigma'} c_{\bm{k},\sigma}^\dag c_{\bm{p},\sigma'}^\dag c_{\bm{p},\sigma'} c_{\bm{k},\sigma} |F} \\
			&= \varrho^2 V^2 - \varrho V \xrightarrow{N \to \infty} \varrho^2 V^2
		\end{align*}
		%
		alle $q = 0$ Terme kompensieren sich.
\end{itemize}
