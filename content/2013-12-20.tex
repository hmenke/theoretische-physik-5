% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 20.12.2013

\renewcommand{\printfile}{2013-12-20}

Für ein Ein-Teilchen-System gilt für die kinetische Energie
%
\begin{align*}
	T &= \sum_{k} \frac{\hbar^2 k^2}{2 m} \ket{k}\bra{k}
\end{align*}
%
Sind $N$ Teilchen vorhanden, so gilt
%
\begin{align*}
	T &= \sum_{i=1}^{N} \sum_{k} \frac{\hbar^2 k_i^2}{2 m} \ket{k_i}\bra{k_i}
\intertext{in zweiter Quantisierung für ein Teilchen}
	&= \sum_{k} \frac{\hbar^2 k^2}{2 m} n_k \\
	&= \sum_{k} \frac{\hbar^2 k^2}{2 m} a_k^\dag a_k \\
	&= \sum_{ij} T_{ij} a_j^\dag a_i
\end{align*}

\minisec{Wichtige Operatoren}

\begin{itemize}
	\item Teilchendichteoperator
		\begin{align*}
			n(\bm{r})
			&= \sum_{k,\ell} \braket{k|n(\bm{r})|\ell} a_k^\dag a_\ell \\
			&= \int \diff \bm{r}' \int \diff \bm{r}'' \sum_{k,\ell} \braket{k|\bm{r}'} \braket{\bm{r}'|n(\bm{r})|\bm{r}''} \braket{\bm{r}''|\ell} a_k^\dag a_\ell \\
			&= \int \diff \bm{r}' \int \diff \bm{r}'' \sum_{k,\ell} \Psi_k^*(\bm{r}') \braket{\bm{r}'|n(\bm{r})|\bm{r}''} \Psi_\ell(\bm{r}'') a_k^\dag a_\ell \\
			&= \int \diff \bm{r}' \int \diff \bm{r}'' \sum_{k} \Psi_k^*(\bm{r}') a_k^\dag \braket{\bm{r}'|n(\bm{r})|\bm{r}''} \sum_{\ell} \Psi_\ell(\bm{r}'')  a_\ell \\
			&= \int \diff \bm{r}' \int \diff \bm{r}''\ \Psi^\dag(\bm{r}') \delta(\bm{r} - \bm{r}') \delta(\bm{r}' - \bm{r}'') \Psi(\bm{r}'') \\
			&= \Psi^\dag(\bm{r}) \Psi(\bm{r})
		\intertext{im $k$-Raum}
			&= \frac{1}{V} \sum_{\bm{k}} \mathrm{e}^{-\mathrm{i} \bm{k} \cdot \bm{r}} \Psi_\bm{k}^\dag \sum_{\bm{k}'} \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \Psi_{\bm{k}'} \\
			n(\bm{r}) &\stackrel{!}{\eqcolon} \frac{1}{V} \sum_{\bm{q}} \mathrm{e}^{\mathrm{i} \bm{q} \cdot \bm{r}} n_\bm{q} \\
			n_\bm{q} &= \sum_{\bm{k}} \Psi_\bm{k}^\dag \Psi_{\bm{k} + \bm{q}}
		\end{align*}
		Kinetische Energie
		\begin{align*}
			T &= \frac{1}{2 m} \sum_{i=1}^{N} p_i^2 = \frac{\hbar^2}{2 m} \sum_{k} k^2 \Psi_\bm{k}^\dag \Psi_\bm{k}
		\end{align*}
	\item Zwei-Teilchen-Wechselwirkung
		\begin{align*}
			V
			&= \frac{1}{2} \sum_{i \neq j} V(\bm{r}_i,\bm{r}_j) \\
			&= \frac{1}{2} \sum_{k,\ell,m,n} a_k^\dag a_\ell^\dag a_n a_m V_{k\ell,mn} \\
			&= \frac{1}{2} \int \diff \bm{r} \int \diff \bm{r}'\ \Psi^\dag(\bm{r}) \Psi^\dag(\bm{r}') \; V(\bm{r},\bm{r}') \; \Psi(\bm{r}') \Psi(\bm{r})
		\intertext{für $V(\bm{r},\bm{r}') = V(\bm{r} - \bm{r}')$}
			&= \frac{1}{2 V} \sum_{\bm{q},\bm{p},\bm{k}} V_\bm{q} \Psi_{\bm{p} - \bm{q}}^\dag \Psi_{\bm{k} - \bm{q}}^\dag \Psi_\bm{k} \Psi_\bm{p}
		\intertext{mit}
			V_\bm{q} &= \int \diff \bm{r}\ \mathrm{e}^{-\mathrm{i} \bm{q} \bm{r}} V(\bm{r})
		\end{align*}

		\begin{figure}[htpb]
			\centering
			\begin{tikzpicture}
				\node[dot] (A) at (0,0) {};
				\node[dot] (B) at (1,0) {};
				\draw[dotted] (A) -- node[below] {$q$} (B);
				\draw[->] (A) +(225:1) node[below] {$\bm{k}$} -- (A);
				\draw[->] (A) -- +(135:1) node[above] {$\bm{k} - \bm{q}$};
				\draw[->] (B) +(-45:1) node[below] {$\bm{q}$} -- (B);
				\draw[->] (B) -- +(45:1) node[above] {$\bm{p} - \bm{q}$};
			\end{tikzpicture}
			\caption{Grafische Darstellung einer Zwei-Teilchen-Wechselwirkung.}
			\label{fig:2013-12-20-1}
		\end{figure}
\end{itemize}

Beziehen wir nun den Spin mit ein. $\Psi_\sigma^\dag(\bm{r})$ erzeugt ein Teilchen am Ort $\bm{r}$ mit Spin $\sigma$.
%
\begin{itemize}
	\item Gesamtdichte
		\begin{align*}
			n(\bm{r}) = \sum_{\sigma} \Psi_\sigma^\dag(\bm{r}) \Psi_\sigma(\bm{r})
		\end{align*}
	\item Spindichte
		\begin{align*}
			\bm{S}(\bm{r})
			&= \sum_{i=1}^{N} \bm{S}_i \delta(\bm{r}-\bm{r}_i) \\
			&= \sum_{\sigma\sigma'} \frac{\hbar}{2} \Psi_\sigma^\dag(\bm{r}) (\bm{\sigma})_{\sigma\sigma'} \Psi_{\sigma'}^\dag(\bm{r}')
		\end{align*}
\end{itemize}
%
Auch mit Ergänzung des Spin-Indexes bleiben die Kommutatoren unverändert:
%
\begin{align*}
	[\Psi_\sigma(\bm{r}),\Psi_{\sigma'}^\dag(\bm{r}')]_\pm = \delta(\bm{r} - \bm{r}') \delta_{\sigma,\sigma'}
\end{align*}

\subsubsection{Bewegungsgleichungen für Operatoren im Heisenbergbild}

Nach der Heisenbergschen Bewegungsgleichung, siehe Abschnitt~\ref{sec:Heisenberg-Bild} im Kapitel~\ref{chap:Prinzipien}.
%
\begin{align*}
	\frac{\diff}{\diff t} a_i &= \frac{\mathrm{i}}{\hbar} [H,a_i]_- \\
	\frac{\diff}{\diff t} a_i^\dag &= \frac{\mathrm{i}}{\hbar} [H,a_i^\dag]_-
\end{align*}
%
und mit $V(\bm{r})$ als Ein-Teilchen-Potential
%
\begin{align*}
	\mathrm{i} \hbar \frac{\partial}{\partial t} \Psi(\bm{r},t) &= \left( - \frac{\hbar^2}{2 m} \nabla^2 + V(\bm{r}) \right) \Psi(\bm{r},t) + \int \diff \bm{r}'\ \Psi^\dag(\bm{r}',t) \; V(\bm{r},\bm{r}') \; \Psi(\bm{r}',t) \; \Psi(\bm{r},t)
\end{align*}

\subsubsection{Wick-Theorem}
\index{Wick-Theorem}

Dieses Theorem ist ein Hilfsmittel um Erwartungswerte in der Besetzungszahldarstellung effektiv zu berechnen.

\begin{theorem}[Definition] Normalordnung.
	\begin{align*}
		\mathcal{N}(a^\dag \dotso a \dotso a^\dag \dotso a^\dag \dotso a \dotso a^\dag)
		&= (\pm 1)^{n_p} \underbrace{a^\dag \dotso a^\dag}_\textnormal{alle $a^\dag$} \underbrace{a \dotso a}_\textnormal{alle $a$}
	\end{align*}
	wobei $n_p$ die Anzahl der Paarvertauschungen ist.
\end{theorem}

\begin{example}
	Für Bosonen
	\begin{align*}
		\mathcal{N}(b_i b_j^\dag) &= b_j^\dag b_i \\
		\mathcal{N}(b_i b_j^\dag b_k^\dag) &= b_j^\dag b_k^\dag b_i = b_k^\dag b_j^\dag b_i
	\end{align*}

	Für Fermionen
	\begin{align*}
		\mathcal{N}(c_i c_i^\dag) &= - c_i^\dag c_i \\
		\mathcal{N}(c_i c_j^\dag c_k^\dag) &=  c_j^\dag c_k^\dag c_i = - c_k^\dag c_j^\dag c_i
	\end{align*}
\end{example}

\begin{theorem}[Definition]
	Eine Kontraktion eines Operatorpaars ist sein Erwartungswert im Grundzustand.
	\begin{align*}
		\braket{AB}_0
		&\equiv \braket{0 | AB | 0}
	\end{align*}
\end{theorem}

\begin{example}
	$ \aligned[t]
		\braket{a_i a_j}_0
		&= \braket{0 | a_i a_j | 0} \\
		&= 0 \\
		&= \braket{a_i^\dag a_j^\dag}_0 \\
		\braket{a_i^\dag a_j}_0
		&= \braket{0 | a_i^\dag a_j | 0} = 0 \\
		\braket{a_i a_j^\dag}_0
		&= \delta_{ij}
	\endaligned $
\end{example}

\begin{theorem}[Definition]
	Ein gewöhnliches Produkt von Erzeugern und Vernichtern ist die Summe von normalgeordneten Produkten, bei denen $0,1,2,\dotsc$ Kontraktionen auf allen möglichen Wegen durchgeführt wurden.
\end{theorem}

\begin{example}
	$ \aligned[t]
		A B = \mathcal{N}(A B) + \braket{A B}_0
	\endaligned $

	Für fermionische Erzeuger und Vernichter:
	\begin{align*}
		c_i c_j^\dag
		&\stackrel{?}{=} \mathcal{N}(c_i c_j^\dag) + \braket{0 | c_i c_j^\dag | 0} \\
		&= - c_j^\dag c_i + \delta_{ij}
	\end{align*}
	Beweis, siehe \textcite[S.~616, Appendix~C]{ballentine1998quantum}.
\end{example}

\begin{example}
	Für vier fermionische Operatoren
	\begin{multline*}
		A B C D
		= 
		\mathcal{N}(A B C D) + \mathcal{N}(A B) \braket{CD}_0 \\
		+ \braket{AB}_0 \mathcal{N}(C D) - \mathcal{N}(AC) \braket{BD}_0 \\
		- \mathcal{N}(BD) \braket{AC}_0 + \mathcal{N}(BC) \braket{AD}_0 \\
		+ \braket{AB}_0 \braket{CD}_0 - \braket{AC}_0 \braket{BD}_0 + \braket{AD}_0 \braket{BC}_0
	\end{multline*}
\end{example}

\begin{notice}
	Der Beweis benötigt nur zwei Eigenschaften:
	\begin{enumerate}
		\item $ a_i \ket{0} = 0$
		\item \enquote{$[A,B]_\pm = \delta_{ij}$}
	\end{enumerate}
	daher verallgemeinerungsfähig.
\end{notice}

\begin{notice}[Korollar:]
	\begin{align*}
		\braket{0 | \mathcal{N}(AB) | 0} &= 0 \\
		\braket{0 | \mathcal{N}(ABCD) | 0} &= 0
	\end{align*}
	und es folgt
	\begin{align*}
		\braket{0 | \mathcal{N}(ABCD) | 0}
		&= \braket{AB}_0 \braket{CD}_0 - \braket{AC}_0 \braket{BD}_0 + \braket{AD}_0 \braket{BC}_0
	\end{align*}
\end{notice}
