% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 24.10.2013

\renewcommand{\printfile}{2013-10-24}

\section{Gemische: Statistischer Operator}

\subsection{Motivation: Ein Spiel}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[
			O/.style={draw,rectangle},
			SG/.style={draw,rectangle,minimum size=1cm}
		]
		\node[O] (O) at (0,0) {O};
		\node[SG] (SGn1) at (2,0) {SG,$z$};
		\node[SG] (SGn2) at ($(SGn1)+(4,0)$) {SG,$\bm{n}$};
		\draw (O) edge[->] (SGn1);
		\draw[dotted] (SGn1.30)  -- ($(SGn1.30)!.5!(SGn2.150)$);
		\draw[dotted] (SGn1.-30) -- ($(SGn1.-30)!.5!(SGn2.-150)$);
		\draw[->] (SGn2)+(-2,0) -- node[above] {$\ket{\psi}$} (SGn2);
		\node[below] at (SGn1.-90) {Alice};
		\node[below] at (SGn2.-90) {Bob};
	\end{tikzpicture}
	\caption{Alice entscheidet welcher Strahl durchkommt.}
	\label{fig:2013-10-24-1}
\end{figure}

Im Aufbau von Abbildung~\ref{fig:2013-10-24-1} entscheidet Alice per Würfel, ob sie den oberen oder den unteren Strahl durchlässt.

Die Frage ist nun: Wie kann Bob seinen Eingangszustand $\ket{\psi}$ beschreiben?
%
\begin{enumerate}
	\item Bobs Experimente zeigen \[ \braket{\sigma_x} = \braket{\sigma_y} = \braket{\sigma_z} = 0 = \braket{\sigma_\bm{n}} \]
	
	\item Es gibt keinen Wert $\ket{\psi_0}$, welcher \[ \braket{\sigma_\bm{n}}_{\ket{\psi_0}} = 0 \; \forall \; n \] produziert.

		Vergleicht man dies mit den Postulaten 1 und 2, so bemerkt man, dass es sich um keinen reinen Zustand handelt. Bobs Kenntnis ist unvollständig.

	\item Bobs Eingangszustand $\ket{\psi}$ ist ein klassisches Ensemble, das mit der Wahrscheinlichkeit $p_+$ ($= 1/2$) den Zustand $\ket{z+}$ und mit $p_-$ ($=1/2$) den Zustand $\ket{z-}$ enthält (bzw.\ präpariert wurde).

	\item Bobs Messwerte in $\bm{n}$-Richtung sind immer noch $\pm 1$. Sei
		\begin{align*}
			\prob[ \sigma_z \hateq \pm 1 | \ket{\psi} ]
			&= p_{z \pm}
		\intertext{dann}
			\prob[ \sigma_x \hateq \pm 1 | \ket{\psi} ]
			&= p_{z +} \cdot \prob[ \sigma_x \hateq \pm 1 | \ket{z+} ] + p_{z -} \prob[ \sigma_x \hateq \pm 1 | \ket{z-} ] \\
			&= p_{z+} \frac{1}{2} + p_{z-} \frac{1}{2} = \frac{1}{2} (\underbrace{p_{z+} + p_{z-}}_{1}) = \frac{1}{2}
		\end{align*}
		%
		genauso \[ \prob[ \sigma_\bm{n} \hateq \pm 1 | \ket{\psi} ] = \frac{1}{2} \]
		folglich \[ \braket{\sigma_\bm{n}} = 0 \; \forall \; n \]
\end{enumerate}

\subsection[Dichtematrix]{Definition des statistischen Operators: Dichtematrix}\index{Dichtematrix}

$\varrho$ ist ein linearer Operator $\mathcal{H} \to \mathcal{H}$ mit den Eigenschaften
%
\begin{enumerate}
	\item $\varrho = \varrho^\dag$

	\item $\tr \varrho = 1$

	\item $\braket{\psi|\varrho|\psi} \geq 0 \; \forall \; \ket{\psi}$
\end{enumerate}
%
In Matrixdarstellung lauten diese Eigenschaften
%
\begin{enumerate}
	\item $\varrho_{nm}^* = \varrho_{nm}$

	\item $\displaystyle \sum\limits_{n} \varrho_{nn} = 1$

	\item $\displaystyle \sum\limits_{mn} c_{m}^* \varrho_{mn} d_{n} \geq 0 \; \forall \; c_m,d_n$
\end{enumerate}

\subsection{Gemisch}

Ein quantales Gemisch enthält mit klassischen Wahrscheinlichkeiten $p_i$ ($\sum_{i=1}^{N} p_i = 1$) die reinen Zustände $\ket{\psi_i}$ ($i=1,\dotsc,N$, im Allgemeinen $N \neq \dim \mathcal{H}$). Die Orthogonalität der Zustände $\ket{\psi_i}$ ist nicht verlangt, also \[ \braket{\psi_i|\psi_j} = \delta_{ij} \] muss nicht erfüllt sein.

Ein \acct*{quantales Gemisch}\index{Gemisch} wird durch einen \acct*{statistischen Operator}\index{Operator!statistischer} beschrieben:
%
\begin{align*}
	\varrho = \sum\limits_{i} p_i \mathcal{P}_{\ket{\psi_i}} = \sum\limits_{i} p_i \ket{\psi_i}\bra{\psi_i}
\end{align*}

Betrachten wir die Eigenschaften aus dem vorherigen Abschnitt, dann finden wir
%
\begin{enumerate}
	\item
		\begin{itemalign}
			\varrho^\dag = \sum\limits_i p_i \ket{\psi_i}\bra{\psi_i} = \varrho
		\end{itemalign}

	\item
		\begin{itemalign}
			\tr \varrho
			&= \tr \sum\limits_i p_i \ket{\psi_i}\bra{\psi_i} \\
			&= \sum\limits_i p_i \tr \ket{\psi_i}\bra{\psi_i} \\
			&= \sum\limits_i p_i \sum\limits_k \braket{k|\psi_i}\braket{\psi_i|k} \\
			&= \sum\limits_i p_i \sum\limits_k \braket{\psi_i|k}\braket{k|\psi_i} \\
			&= \sum\limits_i p_i \sum\limits_k \braket{\psi_i|\psi_i} \\
			&= 1
		\end{itemalign}

	\item
		\begin{itemalign}
			\braket{\psi|\varrho|\psi}
			&= \sum\limits_i p_i \braket{\psi|\psi_i}\braket{\psi_i|\psi} \\
			&= \sum\limits_i p_i |\braket{\psi|\psi_i}|^2 \geq 0
		\end{itemalign}
\end{enumerate}

\begin{example}
	Alice präpariert für Bob mit der Wahrscheinlichkeit $p_1$ den Zustand $\ket{z+}$ und mit $p_2$ den Zustand $\ket{x+}$. Berechnen wir nun den statistischen Operator.
	%
	\begin{align*}
		\varrho
		&= p_1 \ket{z+}\bra{z+} + p_2 \ket{x+}\bra{x+} \\
		&= p_1
		\begin{pmatrix}
			1 & 0 \\
		\end{pmatrix}
		\begin{pmatrix}
			1 \\
			0 \\
		\end{pmatrix}
		+ p_2
		\begin{pmatrix}
			\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\
		\end{pmatrix}
		\begin{pmatrix}
			\frac{1}{\sqrt{2}} \\
			\frac{1}{\sqrt{2}} \\
		\end{pmatrix} \\
		&=
		\begin{pmatrix}
			p_1 + \frac{1}{2} p_2 & \frac{1}{2} p_2 \\
			\frac{1}{2} p_2 & \frac{1}{2} p_2 \\
		\end{pmatrix}
	\end{align*}
\end{example}

\begin{notice}
	\begin{enumerate}
		\item Als Spezialfall enthält das Gemisch auch den reinen Zustand: \[ \varrho = 1 \ket{\psi_0}\bra{\psi_0} = \ket{\psi_0}\bra{\psi_0} = \mathcal{P}_{\ket{\psi_0}} \]

		\item Reiner Zustand $\iff \varrho^2 = \varrho$ \[ \varrho^2 = \ket{\psi_0}\braket{\psi_0|\psi_0}\bra{\psi_0} = \ket{\psi_0}\bra{\psi_0} = \varrho \]

		\item Die Darstellung eines Gemisches durch reine Zustände ist nicht eindeutig.
			\begin{example}
				Welcher Zustand kommt beim Stern-Gerlach-Versuch aus dem Ofen?
				%
				\begin{enumerate}
					\item Misst man mit einem Stern-Gerlach-Magneten in $z$-Richtung, so lautet die Dichtematrix
						\[ \varrho_1 = \frac{1}{2} \Big( \ket{z+}\bra{z+} + \ket{z-}\bra{z-} \Big) =
							\begin{pmatrix}
								\frac{1}{2} & 0 \\
								0 & \frac{1}{2} \\
							\end{pmatrix}
						\]

					\item Misst man mit einem Stern-Gerlach-Magneten in $x$-Richtung, so lautet die Dichtematrix
						\[ \varrho_1 = \frac{1}{2} \Big( \ket{x+}\bra{x+} + \ket{x-}\bra{x-} \Big) =
							\begin{pmatrix}
								\frac{1}{2} & 0 \\
								0 & \frac{1}{2} \\
							\end{pmatrix}
						\]
				\end{enumerate}
				%
				Man sieht, dass wir für unterschiedliche Zustände dieselben Dichtematrizen erhalten.
			\end{example}
	\end{enumerate}
\end{notice}

Es macht also keinen Sinn zu fragen aus welchen Zuständen ein gegebenes Gemisch aufgebaut ist.

\subsection{Die Postulate (allgemein)}

\begin{theorem}[Postulat 1]
	Ein quantenmechanisches System wird durch einen statistischen Operator $\varrho$ beschrieben.
\end{theorem}

\begin{theorem}[Postulat 2a]
	Jeder physikalischer Größe $A$ entspricht ein hermitescher Operator.
\end{theorem}

\begin{theorem}[Postulat 2b]
	Eine Messung von $A$ im Zustand $\varrho_0$ ergibt mit Sicherheit einen der Eigenwerte $a_\mu$ (von $A$).
	Die Wahrscheinlichkeit $a_\mu$ zu messen ist
	%
	\begin{align*}
		\prob[ A \hateq a_\mu | \varrho_0 ]
		&= \tr \varrho_0 \mathcal{P}_\mu \\
		&= \tr \sum\limits_i p_i \ket{\psi_i}\bra{\psi_i} \mathcal{P}_\mu \\
		&= \sum\limits_i p_i \tr \ket{\psi_i}\bra{\psi_i} \mathcal{P}_\mu \\
		&= \sum\limits_i p_i \underbrace{\tr \mathcal{P}_{\ket{\psi_i}} \mathcal{P}_\mu}_{\braket{\psi_i|\mathcal{P}_\mu|\psi_i}}
	\end{align*}
\end{theorem}

\begin{notice}[Konsequenz:]
	Der Erwartungswert (d.\,h.\ der Mittelwert vieler Messungen) in $\varrho_0$ ist
	%
	\begin{align*}
		\braket{A}_{\varrho_0}
		&= \sum\limits_{\nu} a_\nu \prob[ A \hateq a_\nu | \varrho_0 ] \\
		&= \sum\limits_{\nu} a_\nu \tr \varrho_0 \mathcal{P}_\nu \\
		&= \tr \varrho_0 \left( \sum\limits_{\nu} a_\nu \mathcal{P}_\nu \right) \\
		&= \tr \varrho_0 A
	\end{align*}
\end{notice}

\begin{theorem}[Postulat 2c]
	Unmittelbar nach der Messung des Messwerts $a_\mu$ ist das System im Gemisch $\varrho$ mit
	%
	\begin{align*}
		\varrho
		&= \frac{\mathcal{P}_\mu \varrho_0 \mathcal{P}_\mu}{\| \mathcal{P}_\mu \varrho_0 \mathcal{P}_\mu \|} \\
		&= \ket{a_\mu}\bra{a_\mu} \; , \quad \text{falls nicht entartet}
	\end{align*}
\end{theorem}

\begin{theorem}[Postulat 3]
	Nach einer Messung (bzw.\ der Präparation) entwickelt sich $\varrho$ nach der Liouville-von~Neumann-Gleichung:
	%
	\begin{align*}
		\partial_t \varrho
		&= \partial_t \left[ \sum\limits_{i} p_i \ket{\psi_i}\bra{\psi_i} \right] \\
		&= \sum\limits_{i} p_i \partial_t \left[ \ket{\psi_i}\bra{\psi_i} \right]
	\intertext{mit $\mathrm{i} \hbar \partial_t \ket{\psi_i} = H \ket{\psi_i}$}
		&= \sum\limits_{i} p_i \left( \frac{H \ket{\psi_i}\bra{\psi_i}}{\mathrm{i} \hbar} + \frac{\ket{\psi_i}\bra{\psi_i} H}{- \mathrm{i} \hbar} \right) \\
		&= - \frac{\mathrm{i}}{\hbar} \sum\limits_{i} p_i \left( H \ket{\psi_i}\bra{\psi_i} - \ket{\psi_i}\bra{\psi_i} H \right) \\
		\Aboxed{
			\partial_t \varrho &= - \frac{\mathrm{i}}{\hbar} [H,\varrho]
		}
	\end{align*}
	%
	Dies gilt ebenso für zeitabhängige Hamiltonoperatoren $H = H(t)$.

	Für einen zeitunabhängigen Hamiltonoperator gilt formal
	\[ \varrho(t) = \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H t} \varrho_0 \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H t} \]
\end{theorem}
