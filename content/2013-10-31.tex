% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 31.10.2013

\renewcommand{\printfile}{2013-10-31}

Betrachten wir den Zerfall in zwei Teilchen, wie in Abbildung~\ref{fig:2013-10-31} dargestellt.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\node[dot,outer sep=5pt] (A) at (0,0) {};
		\node[dot,outer sep=5pt] (r) at (2,0) {};
		\node[dot,outer sep=5pt] (l) at (-2,0) {};
		\draw[->] (A) -- (r);
		\draw[->] (A) -- (l);
		\draw[->] (l) -- +(60:-0.5) -- +(60:0.5) node[above] {$(1)$};
		\draw[->] (r) -- +(100:-0.5) -- +(100:0.5) node[above] {$(2)$};
	\end{tikzpicture}
	\caption{Zerfall eines Teilchens in zwei mit Drehimpulserhaltung.}
	\label{fig:2013-10-31}
\end{figure}

Das Resultat ist
%
\begin{equation*}
	\braket{B|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|B} = -1
\end{equation*}

\begin{proof}
	\begin{align*}
		\braket{\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}}_{\ket{B}}
		&= \tr\left( \ket{B}\bra{B} \sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)} \right) \\
		&= \aligned[t]
			& \braket{++|B}\braket{B|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|++}
			+ \braket{+-|B}\braket{B|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|+-} \\
			& + \braket{-+|B}\braket{B|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|-+}
			+ \braket{--|B}\braket{B|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|--}
		\endaligned
	\intertext{mit}
		\ket{B}\bra{B}
		&= \frac{1}{2} \left[ \ket{+-}\bra{+-} + \ket{-+}\bra{-+} - \ket{+-}\bra{-+} - \ket{-+}\bra{+-} \right]
	\intertext{mit $\braket{++|B} = \braket{--|B} = 0$}
		\braket{+-|B}\bra{B} &= \frac{1}{2} \left( \bra{+-} - \bra{-+} \right) \\
		\braket{-+|B}\bra{B} &= \frac{1}{2} \left( \bra{-+} - \bra{+-} \right)
	\intertext{damit folgt}
		\braket{\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}}_B
		&= \aligned[t]
			& \frac{1}{2} \braket{+-|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|+-}
			- \frac{1}{2} \braket{-+|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|+-} \\
			& + \frac{1}{2} \braket{-+|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|-+}
			- \frac{1}{2} \braket{+-|\sigma_\bm{n}^{(1)} \sigma_\bm{n}^{(2)}|-+}
		\endaligned \\
		&= \frac{1}{2} \braket{+|\sigma_\bm{n}^{(1)}|+} \braket{-|\sigma_\bm{n}^{(2)}|-} \\
		&= {\sigma_\bm{n}}_{++} {\sigma_\bm{n}}_{--} - {\sigma_\bm{n}}_{+-} {\sigma_\bm{n}}_{-+} \\
		&= - n_z^2 - (n_x - \mathrm{i} n_y) (n_x + \mathrm{i} n_y) \\
		&= n_x^2 + n_y^2 + n_z^2 \\
		&= -1
	\end{align*}

	Es wurde benutzt, dass
	%
	\begin{equation*}
		\sigma_\bm{n} =
		\begin{pmatrix}
			n_z & n_x - \mathrm{i} n_y \\
			n_x + \mathrm{i} n_y & - n_z \\
		\end{pmatrix}
	\end{equation*}
\end{proof}

\subsection{Einschub: Statistischer Operator und Partialspur}

Wir betrachten zwei Systeme in getrennten Hilberträumen $\mathcal{H}^{(1)}$ und $\mathcal{H}^{(2)}$. Diese Systeme werden durch entsprechende Dichtematrizen $\varrho^{(1)}$ und $\varrho^{(2)}$ beschrieben, in den Basen
%
\begin{align*}
	\mathcal{B}^{(1)} &\equiv \left\{ \ket{\phi_\ell} , \ell = 1,\dotsc,N \right\} \\
	\mathcal{B}^{(2)} &\equiv \left\{ \ket{\psi_k} , k = 1,\dotsc,M \right\}
\end{align*}

Sei $A \hateq A^{(1)} \otimes \mathds{1}^{(2)}$ eine Observable, die nur auf $\varrho^{(1)}$ wirkt
%
\begin{align*}
	\braket{A}
	&= \tr(\varrho A) = \tr(\varrho (A^{(1)} \otimes \mathds{1}^{(2)})) \\
	&= \sum\limits_{\ell=1}^{N} \sum\limits_{k=1}^{M} \bra{\phi_\ell} \otimes \braket{\psi_k|\varrho (A^{(1)} \otimes \mathds{1}^{(2)})|\phi_\ell} \otimes \ket{\psi_k} \\
	&= \sum\limits_{\ell=1}^{N} \left\langle \phi_\ell \middle| \sum\limits_{k=1}^{M} \braket{\psi_k|\varrho|\psi_k} A^{(1)} \middle| \phi_\ell \right\rangle \\
	&= \sum\limits_{\ell=1}^{N} \Braket{\phi_\ell | \tr^{(2)} \varrho A^{(1)} | \phi_\ell} \\
	&= \tr^{(1)}(\varrho^{(1)} A^{(1)})
\end{align*}
%
mit $\varrho^{(1)} = \tr^{(2)} \varrho$. Dies ist die \acct*{reduzierte Dichtematrix}\index{Dichtematrix!reduziert}.

\begin{example}
	Betrachte $\varrho = \ket{B}\bra{B}$
	%
	\begin{align*}
		\varrho
		&= \ket{B}\bra{B} \\
		&= \frac{1}{2} 
		\aligned[t]
			\Big\{
				& \big( \ket{+}\bra{+} \otimes \ket{-}\bra{-} \big)
				+ \big( \ket{-}\bra{-} \otimes \ket{+}\bra{+} \big) \\
				& - \big( \ket{+}\bra{-} \otimes \ket{-}\bra{+} \big)
				- \big( \ket{-}\bra{+} \otimes \ket{+}\bra{-} \big)
			\Big\}
		\endaligned
	\end{align*}
	%
	Bilden wir nun die reduzierte Dichtematrix.
	%
	\begin{align*}
		\varrho^{(1)}
		&= \tr^{(2)} \varrho \\
		&= \tr^{(2)} \ket{B}\bra{B} \\
		&= \braket{+|B}\braket{B|+} + \braket{-|B}\braket{B|-} \\
		&= \frac{1}{2} \Big\{
			\ket{-}\bra{-} + \ket{+}\bra{+}
		\Big\} \\
		&=
		\begin{pmatrix}
			\frac{1}{2} & 0 \\
			0 & \frac{1}{2} \\
		\end{pmatrix}
	\end{align*}
	%
	Dieses Resultat ist sehr interessant, denn obwohl wir einen reinen Zustand im Gesamtsystem betrachten erhalten wir eine Dichtematrix, die von einem Gemisch zeugt ($\varrho^2 \neq \varrho$). Das bedeutet, dass Experimente nur an einem Spin nicht zwischen dem Bell-Zustand und thermischen Zufall unterscheiden können.

	Insbesondere ist
	%
	\begin{align*}
		\braket{\sigma_\bm{n}^{(1)}}_{\ket{B}} = 0
	\end{align*}
	%
	für alle Richtungen $\bm{n}$.
\end{example}

Ein reiner Zustand in $\mathcal{H}$ lässt sich schreiben als
%
\begin{align*}
	\ket{\psi} &= \sum\limits_{n,m} c_{nm} \ket{\phi_n} \ket{\psi_m} \\
	\varrho &= \ket{\psi}\bra{\psi}
\end{align*}
%
Damit gilt für die reduzierte Dichtematrix
%
\begin{align*}
	\varrho^{(1)}
	&= \tr^{(2)} \varrho \\
	&= \sum\limits_{\ell=1}^{M} \left\langle \psi_\ell \middle| \sum\limits_{n,m} c_{nm} \ket{\phi_n}\ket{\psi_m} \sum\limits_{i,j} c_{ij}^* \bra{\phi_i}\langle \psi_j \middle| \psi_\ell \right\rangle \\
	&= \sum\limits_{n,i} \sum\limits_{\ell} c_{n\ell} c_{i\ell}^* \ket{\phi_n}\bra{\phi_n} \\
	&= \sum\limits_{n,i} \varrho_{ni}^{(1)} \ket{\phi_n}\bra{\phi_i}
\intertext{mit}
	\varrho_{ni}^{(1)} &= \sum\limits_{\ell} c_{n\ell} c_{i\ell}^*
\end{align*}

\begin{example}
	Spezialfall
	%
	\begin{align*}
		\ket{\psi} = \sum\limits_{n=1}^{N} d_n \ket{\phi_n} \ket{\psi_n}
	\end{align*}
	%
	Dann lautet die reduzierte Dichtematrix
	%
	\begin{align*}
		\varrho^{(1)}
		&= \tr^{(2)} \ket{\psi}\bra{\psi} \\
		&= \sum\limits_{\ell} \left\langle \psi_\ell \middle| \sum\limits_{n=1}^{N} d_n \ket{\phi_n} \ket{\psi_n} \sum\limits_{i=1}^{N} d_i^* \bra{\phi_i} \bra{\psi_i} \middle| \psi_\ell \right\rangle \\
		&= \sum\limits_{\ell} d_\ell d_\ell^* \ket{\phi_\ell} \bra{\phi_\ell}
	\end{align*}
\end{example}

\section{Quantale Realität}

\subsection{Einsteins Einwand gegen die Quantenmechanik}

Einstein wehrte sich beinahe sein ganzes Leben gegen die Quantenmechanik (1879-1955). Er veröffentlichte eine Arbeit zur Widerlegung zusammen mit Rosen und Podolsky, die heute als EPR-Argument bekannt ist. Sie ist zu finden unter \fullcite{PhysRev.47.777}.

Beim Zerfall eines Pions $\pi^0$ entsteht ein Elektron-Positron-Paar, siehe Abbildung~\ref{fig:2013-10-31-2}. Elektron und Positron fliegen in entgegengesetzte Richtung. In jeder Richtung wartet ein Beobachter mit einem Stern-Gerlach-Magneten, der den Magnet nach Belieben ausrichten kann. Die Strecke zwischen den beiden Beobachter sei so groß, dass keiner das Messergebnis des anderen sehen kann.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[
			SG/.style={draw,rectangle,minimum size=1cm}
		]
		\node[dot,outer sep=5pt,label={below:$\pi^0$}] (A) at (0,0) {};
		\node[SG,outer sep=5pt] (r) at (2,0) {SG,$A$};
		\node[SG,outer sep=5pt] (l) at (-2,0) {SG,$B$};
		\draw[->] (A) -- node[above] {$e^-$} (r);
		\draw[->] (A) -- node[above] {$e^+$} (l);
		\node[below=0.5cm] at (r) {Alice};
		\node[below=0.5cm] at (l) {Bob};
	\end{tikzpicture}
	\caption{Zerfall eines Pions. $\pi^0 \to e^- + e^+$}
	\label{fig:2013-10-31-2}
\end{figure}

Es gelten folgende Tatsachen:
%
\begin{enumerate}
	\item $A$ und $B$ entscheiden unabhängig in welche Richtung $\bm{n}_A$ und $\bm{n}_B$ sie den Spin messen:
		\begin{center}
			\begin{tabular}{cc}
				\toprule
				Messung von $A$ & Messung von $B$ \\
				\midrule
				$\bm{n}_{A,1} \hateq 1$ & $\bm{n}_{B,1} \hateq \ldots$ \\ 
				$\bm{n}_{A,2} \hateq 1$ & $\bm{n}_{B,2} \hateq \ldots$ \\ 
				$\vdots$ & $\vdots$ \\
				\bottomrule
			\end{tabular}
		\end{center}
	\item Der Befund zeigt, falls $\bm{n}_{A,i} = \bm{n}_{B,i}$ ist, ist das Ergebnis $\sigma_A \sigma_B = -1$
	\item Einsteins Argumente waren
		\begin{itemize}
			\item $A$ kann nach der Messung von $\sigma_{\bm{n}_A}$ den Wert von Bobs Messung in $\bm{n}_B = \bm{n}_A$-Richtung exakt voraussagen.
				\[ \prob[ \sigma_\bm{n}^B = -1 | \sigma_\bm{n}^A = +1 ] = +1 \]
			\item Alice Entscheidung welche Richtung $\bm{n}_A$ sie wählt, kann Bobs Spin nicht beeinflussen (Lokalität). Dies würde einer Signalübertragung mit Überlichtgeschwindigkeit einsprechen, also \[ 2 L > c \cdot \Delta t \]
			\item Also muss der Wert von Bobs Spin für jede Richtung $\bm{n}_B$ festgelegt sein, bevor Bob misst.
			\item In der Quantenmechanik gilt das nicht.
				\[ \Braket{B | \sigma_\bm{n}^B | B} = 0. \]
				Daraus folgerte Einstein, dass die Quantenmechanik also unvollständig sein muss
		\end{itemize}
\end{enumerate}


