% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 17.10.2013

\renewcommand{\printfile}{2013-10-17}

\chapter{Prinzipien der Quantenmechanik}\label{chap:Prinzipien}

\section[Die Postulate]{Die Postulate (für reine Zustände)}

\begin{theorem}[Postulat 1]
	Bei vollständiger Kenntnis (d.\,h.\ nach vollständiger Präparation) wird ein quantenmechanisches System durch einen normierten Vektor $\ket{\psi_0} \in \mathcal{H}$ beschrieben.
\end{theorem}

\begin{theorem}[Postulat 2a]
	Jeder physikalischen Größe $A$ entspricht ein \acct*{hermitescher Operator}\index{Operator!hermitesch}
	%
	\begin{align*}
		A
		&= \sum\limits_{n} a_n \ket{n}\bra{n} \\
		&= \sum\limits_{\nu} a_\nu P_\nu \quad \text{mit} \quad P_\nu = \sum\limits_{\left\{n\middle\vert a_n=a_\nu\right\}} \ket{n}\bra{n} & \text{(für entartete $a_n$)}
	\end{align*}
	%
	mit den Eigenwerten $a_n$ und den Eigenvektoren $\ket{n}$, sodass $A \ket{n} = a_n \ket{n}$.
\end{theorem}

Postulat 2 enthält den Fall, dass die Eigenwerte \acct*{entartet} sind.

\begin{theorem}[Postulat 2b]
	Eine Messung von $A$ im Zustand $\ket{\psi_0}$ ergibt mit Sicherheit einen der Eigenwerte $a_\nu$. Die \acct{Wahrscheinlichkeit} $a_\nu$ zu messen ist
	%
	\begin{align*}
		\prob[A \hateq a_\mu | \ket{\psi_0} ]
		&= \braket{\psi_0|P_\mu|\psi_0} \; , \quad P_\mu = \sum\limits_{\left\{n\middle\vert a_n=a_\nu\right\}} \ket{n}\bra{n} \\
		&= \sum\limits_{k} \braket{\psi_0|P_\mu|k} \braket{k|\psi_0} \\
		&= \sum\limits_{k} \braket{k|\psi_0} \braket{\psi_0|P_\mu|k} \\
		&= \tr(P_{\psi_0} P_\mu)
	\end{align*}
	%
	wobei $P_{\psi_0}$ der Projektor auf den Zustand $\ket{\psi_0}$ ist. Bei Nichtentartung gilt
	%
	\begin{align*}
		P_\mu = \ket{m}\bra{m} = \braket{\psi_0|m} \braket{m|\psi_0} = |\psi_0(m)|^2
	\end{align*}
\end{theorem}

\begin{notice}[Konsequenz:]
	Damit ist der Mittelwert (\acct{Erwartungswert}) nach vielen Messungen der Größe $A$ im jeweils identisch präparierten Zustand $\psi_0$
	%
	\begin{align*}
		\braket{A}_{\ket{\psi_0}}
		&= \sum\limits_{\nu} a_\nu \prob[ A \hateq a_\nu | \ket{\psi_0} ] \\
		&= \sum\limits_{\nu} a_\nu \braket{\psi_0|P_\nu|\psi_0} \\
		&\overset{\text{(P1)}}{{}={}} \braket{\psi_0|A|\psi_0}
	\end{align*}
	%
	Im Allgemeinen ist dieses Ergebnis nicht gleich einem der $a_\nu$.
\end{notice}

\begin{theorem}[Postulat 2c]
	Unmittelbar nach der Messung des Wertes $a_\mu$ ist das System im Zustand
	%
	\begin{align*}
		\ket{\psi}
		&= \frac{P_\mu \ket{\psi_0}}{\| P_\mu \ket{\psi_0} \|}
	\end{align*}
	%
	bei Nichtentartung gilt $\ket{\psi} = \ket{\mu}$.
\end{theorem}

\begin{theorem}[Postulat 3]
	Dieses Postulat heißt auch dynamisches Postulat.

	Nach einer Messung (Präparation) entwickelt sich der Zustand $\ket{\psi(t)}$ nach der \acct{Schrödingergleichung}:
	%
	\begin{align*}
		\mathrm{i} \hbar \partial_t \ket{\psi(t)} = H(t) \ket{\psi(t)}
	\end{align*}
	%
	mit dem Hamiltonoperator $H(t)$.
\end{theorem}

\section[Spin \texorpdfstring{$1/2$}{1/2} und Stern-Gerlach-Experimente]{Spin \boldmath $1/2$ und Stern-Gerlach-Experimente}\index{Stern-Gerlach-Experimente}

\subsection{Versuchsaufbau}

\begin{figure}[htpb]
	\centering
	% % By Romeo Van Snick
	\tdplotsetmaincoords{75}{140}
	\begin{tikzpicture}[tdplot_main_coords,scale=0.7]
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](0,-2,0)--(0,2,0) coordinate (A)--(0,2,2) coordinate (B)--(0,1,2) coordinate (C)--(0,1,1) coordinate (Q)--(0,-1,1) coordinate (D)--(0,-1,2) coordinate (E)--(0,-2,2) coordinate (F)--cycle;

		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](F)++(-6,0,0)--(F)--(E)--++(-6,0,0)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](D)++(-6,0,0)--(D)--(E)--++(-6,0,0)--cycle;

		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](0,-1,3)--(0,0,2) coordinate (G)--(0,1,3) coordinate (H)--(0,1,4) coordinate (I)--(0,-1,4) coordinate (J)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](G)++(-6,0,0)--(G)--(H)--++(-6,0,0)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](H)++(-6,0,0)--(H)--(I)--++(-6,0,0)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](J)++(-6,0,0)--(J)--(I)--++(-6,0,0)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](Q)++(-6,0,0)--(Q)--(D)--++(-6,0,0)--cycle;

		\foreach \x in{0,2,4,6}{
		\draw[->] (-\x,0,2)--(-\x,1,1);
		\draw[->] (-\x,0,2)--(-\x,0.5,1);
		\draw[->] (-\x,0,2)--(-\x,0,1);
		\draw[->] (-\x,0,2)--(-\x,-0.5,1);
		\draw[->] (-\x,0,2)--(-\x,-1,1);
		}


		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](0,-2,0)--(0,2,0) coordinate (A)--(0,2,2) coordinate (B)--(0,1,2) coordinate (C)--(0,1,1) coordinate (Q)--(0,-1,1) coordinate (D)--(0,-1,2) coordinate (E)--(0,-2,2) coordinate (F)--cycle;

		\draw(-9,0,1.8) coordinate (R);


		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](R)++(0,-0.5,-0.5)--++(0,0,1) coordinate (R3)--++(0,1,0) coordinate (R2)--++(0,0,-1) coordinate (R1)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](R1)++(-1,0,0)--(R1)--(R2)--++(-1,0,0)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](R3)++(-1,0,0)--(R3)--(R2)--++(-1,0,0)--cycle;

		\tdplotsetrotatedcoords{0}{90}{0}
		\tdplotdrawarc[tdplot_rotated_coords,fill=black,style={thick,draw=gray}]{(R)}{0.1}{0}{360}{}{}
		\draw[thick](R)--(-5,0,1.8) coordinate (M);

		\draw[thick](M)--(5,0,2) coordinate (M1);
		\draw[thick](M)--(5,0,1.6) coordinate (M2);

		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](C)++(-6,0,0)--(C)--(B)--++(-6,0,0)--cycle;
		\draw[style={fill=lightgray!50,draw=none},style={thick,draw=gray}](A)++(-6,0,0)--(A)--(B)--++(-6,0,0)--cycle;


		\draw(5,0,0) coordinate (S);
		\draw[style={fill=lightgray,opacity=0.6,draw=none},style={thick,draw=gray}](S)--++(0,2,0)--++(0,0,3.6)--++(0,-4,0)--++(0,0,-3.6)--cycle;

		\draw(5,0,1.8) coordinate (K);

		\tdplotdrawarc[tdplot_rotated_coords,fill=black]{(M1)}{0.03}{0}{360}{}{}
		\tdplotdrawarc[tdplot_rotated_coords,fill=black]{(M2)}{0.03}{0}{360}{}{}
		\draw(R1)++(-1,0,2.5) coordinate (R4);
		\draw(S)++(2,0,0) coordinate (S1);
	\end{tikzpicture}
	\caption{Versuchsaufbau der Stern-Gerlach-Experimente}
\end{figure}

\minisec{Ergebnis}

\begin{enumerate}
	\item\label{itm:2013-10-17-1} Jedes Atom wird entweder einen festen Betrag nach oben oder nach unten ausgelenkt.

	\item\label{itm:2013-10-17-2} Beide Ergebnisse sind gleich wahrscheinlich.

	\item Werden Magnet und Schirm in der $x,z$-Ebene gedreht, bleiben die Befunde aus \ref{itm:2013-10-17-1} und \ref{itm:2013-10-17-2} erhalten.
\end{enumerate}

\subsection{Klassische Analyse}

Die Hamilton-Funktion für ein Silberatom lautet
%
\begin{align*}
  H = \frac{\bm{p}^2}{2 m} - \bm{\mu} \cdot \bm{B}(\bm{r}) ,
\end{align*}
%
wobei $\bm{\mu}$ das magnetische Moment des Atoms ist. Die Kraft auf das Silberatom ist gegeben durch:
%
\begin{align*}
  \bm{F} = \nabla (\bm{\mu} \cdot \bm{B}(\bm{r})) .
\end{align*}
%
Der dominante Teil der Kraft ist die $z$-Komponente
%
\begin{align*}
  F_z = \bm{\mu} \frac{\partial \bm{B}}{\partial z} \simeq \mu_z \underbrace{\left. \frac{\partial B_z}{\partial z} \right|_{z=0}}_{\text{const.}} .
\end{align*}
%
Wir erwarten, dass $\bm{\mu}$ unpolarisiert ist, das heißt jeder Wert $\mu_z = | \bm{\mu} | \cos\theta$ auf der Kugel $(\theta,\varphi)$ gleich wahrscheinlich ist. Das bedeutet für die Wahrscheinlichkeit:
%
\begin{align*}
  p(\theta) = \frac{2 \pi}{4 \pi} \sin \theta .
\end{align*}

\begin{figure}[htpb]
  \centering
  \begin{tikzpicture}
	  \draw (0,-2) -- (0,2);
	  \draw[DarkOrange3] (0,1.8) -- (0.8,1.8)   node[dot,label={right:{quant. mech.}}] {};
	  \draw[DarkOrange3] (0,-1.8) -- (0.8,-1.8) node[dot,label={right:{quant. mech.}}] {};
	  \draw[MidnightBlue,rotate=-90] plot[samples=100,domain=-1.8:1.8] (\x,{0.8*exp(-\x*\x)});
	  \node[MidnightBlue,right] at (0.8,0) {klass. mech.};
  \end{tikzpicture}
  \caption{Wahrscheinlichkeitsverteilung auf dem Schirm}
\end{figure}

Versucht man, diese Beobachtung zu erklären, indem man vollständiger Polarisation in $\pm z$-Richtung annimmt, erwartet man bei Orientierung in $x$-Richtung keine Auslenkung. Beobachtet wird jedoch die gleiche Auslenkung wie bei Orientierung in $z$-Richtung. Die Stern-Gerlach-Experimente sind also klassisch nicht zu erklären.

\subsection{Hintereinanderausführung: Schlüsselexperimente}

Wir verwenden im Folgenden die Kurzdarstellung aus Abbildung~\ref{fig:2013-10-17-1}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[
			O/.style={draw,rectangle},
			SG/.style={draw,rectangle,minimum size=1cm}
		]
		\node[O] (O) at (0,0) {O};
		\node[SG] (SGz) at (2,0) {SG,$\bm{n}$};
		\draw (O) edge[->] (SGz);
		\draw[->] (SGz.30) -- node[above] {$+$} ++(1,0) node[right] {weiterführen};
		\draw[-|] (SGz.-30) -- node[below] {$-$} ++(1,0) node[right] {blockieren};
	\end{tikzpicture}
	\caption{Schematischer Aufbau eines Stern-Gerlach-Experiments.}
	\label{fig:2013-10-17-1}
\end{figure}

\begin{example}[\ForwardToEnd \hspace{0.5em} Experiment 1]~
	\begin{center}
		\begin{tikzpicture}[
				O/.style={draw,rectangle},
				SG/.style={draw,rectangle,minimum size=1cm}
			]
			\node[O] (O) at (0,0) {O};
			\node[SG] (SGn1) at (2,0) {SG,$\bm{n}$};
			\node[SG] (SGn2) at ($(SGn1.30)+(2,0)$) {SG,$\bm{n}$};
			\draw (O) edge[->] (SGn1);
			\draw[->] (SGn1.30)  -- node[above] {$+$} (SGn2);
			\draw[-|] (SGn1.-30) -- node[below] {$-$} ++(1,0);
			\draw[->] (SGn2.30)  -- node[above] {$+$} ++(1,0);
		\end{tikzpicture}
	\end{center}

	\begin{align*}
		\prob\left[ \sigma_n = + 1 \Big| \ket{n_{+}} \right] = 1 \\
		\prob\left[ \sigma_n = - 1 \Big| \ket{n_{+}} \right] = 0
	\end{align*}
\end{example}

\begin{example}[\ForwardToEnd \hspace{0.5em} Experiment 2a]~
	\begin{center}
		\begin{tikzpicture}[
				O/.style={draw,rectangle},
				SG/.style={draw,rectangle,minimum size=1cm}
			]
			\node[O] (O) at (0,0) {O};
			\node[SG] (SGn1) at (2,0) {SG,$\bm{n}$};
			\node[SG] (SGn2) at ($(SGn1.30)+(2,0)$) {SG,$\bm{n}$};
			\node[SG] (SGn3) at ($(SGn2.30)+(2,0)$) {SG,$\bm{n}$};
			\draw (O) edge[->] (SGn1);
			\draw[->] (SGn1.30)  -- node[above] {$+$} (SGn2);
			\draw[-|] (SGn1.-30) -- node[below] {$-$} ++(1,0);
			\draw[->] (SGn2.30)  -- node[above] {$+$} (SGn3);
			\draw[-|] (SGn2.-30) -- node[below] {$-$} ++(1,0);
			\draw[->] (SGn3.30)  -- node[above] {$+$} ++(1,0) node[right] {$1/2$};
			\draw[-|] (SGn3.-30) -- node[below] {$-$} ++(1,0) node[right] {$1/2$};
		\end{tikzpicture}
	\end{center}

	\begin{align*}
		\prob\left[ \sigma_z = \pm 1 \Big| \ket{x_{+}} \right] = \frac{1}{2} \\
		\prob\left[ \sigma_z = \pm 1 \Big| \ket{x_{+}} \right] = \frac{1}{2}
	\end{align*}
\end{example}

\begin{example*}[\ForwardToEnd \hspace{0.5em} Experiment 2b]
	Analog zu Experiment 2a, ersetze SG,$x$ durch SG,$y$.
\end{example*}

\begin{example}[\ForwardToEnd \hspace{0.5em} Experiment 3]~
	\begin{center}
		\begin{tikzpicture}[
				O/.style={draw,rectangle},
				SG/.style={draw,rectangle,minimum size=1cm}
			]
			\node[O] (O) at (0,0) {O};
			\node[SG] (SGn1) at (2,0) {SG,$z$};
			\node[SG] (SGn2) at ($(SGn1.30)+(2,0)$) {SG,$\bm{n}$};
			\draw (O) edge[->] (SGn1);
			\draw[->] (SGn1.30)  -- (SGn2);
			\draw[-|] (SGn1.-30) -- ++(1,0);
			\draw[->] (SGn2.-30) -- ++(1,0) node[right] {$\cos^2 \theta/2$};
			\draw[->] (SGn2.30)  -- ++(1,0) node[right] {$\sin^2 \theta/2$};
		\end{tikzpicture}
	\end{center}

	\begin{align*}
		\prob\left[ \sigma_n = + 1 \Big| \ket{n_{+}} \right] = 1 \\
		\prob\left[ \sigma_n = - 1 \Big| \ket{n_{+}} \right] = 0
	\end{align*}
\end{example}

