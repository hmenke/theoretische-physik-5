% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 30.01.2014

\renewcommand{\printfile}{2014-01-30}

\subsection{Quantenkorrekturen}

Betrachte ein Teilchen in einer Dimension im Potential $V(x)$. Dann gilt für die Zustandssumme
%
\begin{align*}
	Z(\beta)
	&= \tr \mathrm{e}^{-\beta H} \\
	&= \int_{-\infty}^{\infty} \diff x\ K_E(x,\tau;x,0) \\
	&= \int_{-\infty}^{\infty} \diff x \int_{x(0) = x}^{x(\tau) = x} \mathcal{D}[x(\tau)]\ \exp\left( -\frac{1}{\hbar} \int_{0}^{\tau_b = \beta \hbar} \diff \tau\ \left[ \frac{m}{2} \left( \frac{\diff x}{\diff \tau} \right)^2 + V(x(\tau)) \right] \right)
\end{align*}
%
Kommend von hohen Temperaturen gilt $\beta \ll 1$ und damit $\tau_b \ll 1$. Die dominanten Pfade können sich nicht weit von $x$ bewegen. Daher machen wir eine Abschätzung
%
\begin{align*}
	-\frac{1}{\hbar} \int_{0}^{\beta \hbar} \diff \tau\ \frac{m}{2} \left( \frac{\diff x}{\diff \tau} \right)^2
	&\approx - \frac{1}{\hbar} \frac{m}{2} \frac{(\Delta x)^2}{\beta \hbar} \\
	(\Delta x)^2 &\approx \frac{2 \beta \hbar^2}{m}
\end{align*}
%
Wir können $V(x)$ aus dem Integral ziehen:
%
\begin{align*}
	Z(\beta)
	&=
	\begin{multlined}[t]
		\int_{-\infty}^{\infty} \diff x\ \exp\left( -\frac{1}{\hbar} \beta \hbar V(x) \right) \int_{x(0) = x}^{x(\tau) = x} \mathcal{D}[x(\tau)] \\
		\exp\Biggl( -\frac{1}{\hbar} \int_{0}^{\tau_b = \beta \hbar} \diff \tau\ \Biggl[ \frac{m}{2} \left( \frac{\diff x}{\diff \tau} \right)^2 + \underbrace{V(x(\tau)) - V(x)}_{=0} \Biggr] \Biggr)
	\end{multlined}
\end{align*}
%
Dabei ist
\[ \int_{x(0) = x}^{x(\tau) = x} \mathcal{D}[x(\tau)] \exp\Biggl( -\frac{1}{\hbar} \int_{0}^{\tau_b = \beta \hbar} \diff \tau\ \frac{m}{2} \left( \frac{\diff x}{\diff \tau} \right)^2 \Biggr) = \sqrt{\frac{m}{2 \pi \hbar (\beta \hbar)}} \]
der Propagator für ein freies quantenmechanisches Teilchen.
%
\begin{align*}
	Z(\beta)
	&\approx \frac{1}{\hbar} \sqrt{\frac{m}{2 \pi \beta}} \int_{-\infty}^{\infty} \diff x\ \mathrm{e}^{-\beta V(x)}
\end{align*}
%
Wir haben herausgefunden, dass für hohe Temperaturen aus der Quantenstatistik die klassische Statistik folgt.

\begin{notice}
	Eine sorgfältige Entwicklung liefert die Wigner-Kirkwood-Reihe:
	\[ Z = \int_{-\infty}^{\infty} \frac{\diff x}{\lambda_T} \mathrm{e}^{\beta V(x)} \left( 1 - \frac{\beta^2 \hbar^2}{24 m} \left( \frac{\partial V}{\partial x} \right)^2 + \ldots \right) \]
\end{notice}

\subsection{Verbindung zur klassischen statistischen Mechanik eines eindimensionalen Systems}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.5,0) -- (4.5,0) node[below] {$x$};
		\draw (0,-0.1) node[below] {$x=0$} -- (0,1.5);
		\draw (4,-0.1) node[below] {$L$} -- (4,1.5);
		\draw[DarkOrange3,->] (0.5,0) -- node[right] {$\ell(x)$} (0.5,1.2);
		\draw[MidnightBlue] plot[smooth] coordinates {(0,0.5) (0.5,1.2) (1,1) (1.5,0.7) (2,1.2) (2.5,0.6) (3,2) (3.5,1) (4,1.2)};
	\end{tikzpicture}
	\caption{Auslenkung einer Saite aus der Ruhelage.}
	\label{fig:2014-01-30-1}
\end{figure}

Die Energie des in Abbildung~\ref{fig:2014-01-30-1} abgebildeten Systems wird beschrieben durch
%
\begin{align*}
	H[\ell(x)]
	&= \int_0^L \diff x\ \left\{ \frac{k}{2} \left( \frac{\partial \ell}{\partial x} \right)^2 + V(\ell(x)) \right\}
\end{align*}
%
Dies entspricht einer Dehnungsenergie (Spannung) für eine Konfiguration. Liegt die Saite nun in einem Wärmebad, so erhalten wir viele Konfigurationen. Die Zustandssumme lautet dann
%
\begin{align*}
	Z(\beta) = \int \mathcal{D}[\ell(x)] \mathrm{e}^{-\beta H}
\end{align*}
%
Dies ist äquivalent zur quantalen Dynamik eines null-dimensionalen Punktteilchens im Potential $V(\ell)$. Betrachte dazu die Korrespondenzen zur klassischen statistischen Mechanik
%
\begin{center}
	\begin{tabular}{cc}
		\toprule
		quantal & klassisch statistisch \\
		\midrule
		Masse $m$ & Spannungsmodul $k$ \\
		Zeit $t$ & Ort $x$ \\
		$\hbar$ & Temperatur $\beta^{-1}$ \\
		Gesamtzeit $t_b - t_a$ & Länge $L$ \\
		\bottomrule
	\end{tabular}
\end{center}

\chapter{Offene Quantensysteme}

\section{Motivation: Dekohärenz}

\subsection{Schrödingers Katze}
\index{Schrödingers Katze}

\begin{figure}[htpb]
	\centering
	\def\cat[#1]#2(#3){%
		% Amazing graphics !!!
		\begin{scope}[shift={(#3)},scale=#1]
			\draw (0,0) rectangle (1,0.5);
			\draw (0,-0.1) -- (0.2,0) -- (0.4,-0.1);
			\draw (0.6,-0.1) -- (0.8,0) -- (1,-0.1);
			\ifx#21
			\draw (0.1,0.75) circle (0.2);
			\draw (-0.05,1) -- (0,1.2) -- (0.05,1);
			\draw (0.15,1) -- (0.2,1.2) -- (0.25,1);
			\node at (0,0.8) {$\cdot$};
			\node at (0.2,0.8) {$\cdot$};
			\node at (0.7,0.8) {$\ket{L}$};
			\else
			\draw (-0.25,0.25) circle (0.2);
			\draw (-0.5,0.1) -- (-0.7,0.15) -- (-0.5,0.2);
			\draw (-0.5,0.3) -- (-0.7,0.35) -- (-0.5,0.4);
			\node at (-0.3,0.15) {$\times$};
			\node at (-0.3,0.35) {$\times$};
			\node at (0.7,0.8) {$\ket{D}$};
			\fi
		\end{scope}
	}
	\begin{tikzpicture}
		\begin{scope}
			\draw (0,0) rectangle (3,3);
			\cat[1]{1}(1,1);
			\node[label={above:Atom}] at (2.5,2) {$\ket{e}$};
			\node[below] at (1.5,0) {\parbox{3cm}{\raggedright%
				Katze lebt, Atom noch nicht zerfallen bei $t=0$.
				\[ \ket{\phi_0} = \ket{L} \otimes \ket{e} \]
			}};
		\end{scope}
		\begin{scope}[xshift=3.5cm]
			\draw (0,0) rectangle (3,3);
			\cat[1]{0}(1,1);
			\node[label={above:Atom}] at (2.5,2) {$\ket{g}$};
			\node[below] at (1.5,0) {\parbox{3cm}{\raggedright%
				Katze tot, Atom zerfallen bei $t=\infty$.
				\[ \ket{\phi_1} = \ket{D} \otimes \ket{g} \]
			}};
		\end{scope}
	\end{tikzpicture}
	\caption{Schrödingers Gedankenexperiment zur Katze im Kasten.}
	\label{fig:2014-01-30-2}
\end{figure}

Für einen Zeitpunkt $t$ dazwischen befindet sich das System im Zustand
\[ \ket{\phi} = c_0(t) \ket{\phi_0} + c_1(t) \ket{\phi_1} \]

\begin{notice}[Fragen:]
	\begin{enumerate}
		\item Was bedeutet $\phi(t)$ vor der Messung?
		\item Warum beobachten wir klassische Objekte nie in Superposition? Warum haben klassische Teilchen immer einen festen Ort?
		\item Wo ist die Grenze zwischen mikroskopischer und makroskopischer Welt?
	\end{enumerate}
\end{notice}

\subsection{Messproblematik}

%\todo[inline]{Literatur --- Zurek, Schlosshammer: Dekoheränz}

Von Neumann überlegte sich ein System mit der Basis
\[ \{ \ket{s_i}, i=1,\dotsc,N \} \]
und einem Apparat mit
\[ \{ \ket{A_0}, \ket{A_i} \} \]

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (-1,-1) rectangle (1,1);
		\foreach \i[count=\j] in {-90,160,120,60,20} {
			\draw[->] (0,0) -- (\i:1) node[label={\i:$\pgfmathparse{int(\j-1)}A_{\pgfmathresult}$}] {};
		}
	\end{tikzpicture}
	\caption{Apparat mit verschiedenen Zuständen. $A_0$ ist der Zustand vor der Messung.}
	\label{fig:2014-01-30-3}
\end{figure}

Zum Zeitpunkt $t=0$ gilt
\[ \ket{\phi_0} = \ket{s_i} \otimes \ket{A_0} \]
Wünschenswert wäre nun eine Entwicklung
\[ \ket{\phi_t} = \ket{s_i} \otimes \ket{A_i} \]
aber es gilt
\[ \ket{\phi_0} = \left( \sum_i c_i \ket{s_i} \right) \otimes \ket{A_0} \]
mit der zwangsläufigen Entwicklung
%
\begin{align*}
	\ket{\phi_t} &= \sum_i c_i \ket{s_i} \otimes \ket{A_i} \\
	\varrho_t &= \ket{\phi_t}\bra{\phi_t} = \sum_{i,j} c_j^* c_i \ket{s_j}\bra{s_i} \otimes \ket{A_j}\bra{A_i}
\end{align*}
%
Am liebsten hätten wir eine reduzierte Dichtematrix wie
%
\begin{align*}
	\varrho_\textnormal{red} = \sum_i |c_i|^2 \ket{s_i}\bra{s_i} \otimes \ket{A_i}\bra{A_i}
\end{align*}
%
Dann wäre $p_i = |c_i|^2$ die klassische Wahrscheinlichkeit den Zeiger bei $A_i$ zu haben.

\begin{example}
	Verschärftes Problem. Das System ist dieses Mal ein Spin $1/2$ System
	\[ \ket{z+} \ket{A_0} \to \ket{z+} \ket{A_{z+}} \]
	mit einem Apparat, der mit dem Spin $1/2$ in Kontakt steht
	\[ \ket{z-} \ket{A_0} \to \ket{z-} \ket{A_{z-}} \]
	Dann
	%
	\begin{align*}
		\ket{\phi_0} = \frac{1}{\sqrt{2}} (\ket{z+} - \ket{z-})\ket{A_0}
		\to
		\ket{\phi_t}
		&= \frac{1}{\sqrt{2}} (\ket{z+}\ket{A_{z+}} - \ket{z-}\ket{A_{z-}}) \\
		&= \frac{1}{\sqrt{2}} (\ket{x+}\ket{A_{x+}} - \ket{x-}\ket{A_{x-}})
	\end{align*}
	%
	Dieser Apparat würde $x$ und $z$ messen. Das heißt, solange die Kohärenzen im Spiel sind, kann man nicht wissen welche Observable überhaupt gemessen werden kann.
\end{example}
