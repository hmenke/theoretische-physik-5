% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 22.11.2013

\renewcommand{\printfile}{2013-11-22}

\begin{notice}[Erinnerung:]
	Für die Übergangsrate bei adiabatischem Einschalten einer konstanten Störung gilt in erster Ordnung
	%
	\begin{align*}
		\Gamma_{a \to b}^{(1)} &= \frac{|\braket{b|V|a}|^2}{\hbar} 2 \pi \delta(E_a - E_b)
	\end{align*}
	%
	und in zweiter Ordnung
	%
	\begin{align*}
		\Gamma_{a \to b}^{(2)} &= \frac{2\pi}{\hbar^2} \lim\limits_{\eta \to 0} \left|
			\sum_{c} \frac{\braket{b|V|c}\braket{c|V|a}}{(E_a - E_c) + \mathrm{i}\eta\hbar}
		\right|^2 \delta(E_b - E_a)
	\end{align*}

	Eine sehr vage Interpretation dieser Form ist: \enquote{Die $\ket{c}$ sind virtuelle Zustände, für die keine Energieerhaltung gelten muss.}
\end{notice}

\subsubsection{Floquet-Störungstheorie}
\index{Floquet!Störungstheorie}

Der Hamiltonoperator sei gegeben durch
%
\begin{align*}
	H(t) = H_0(t) + \lambda H_1(t) \; , \quad \lambda \ll 1
\end{align*}
%
Unser Ziel ist es die verallgemeinerte Schrödingergleichung zu lösen. Als Ausgangspunkt sei das Problem für $H_0(t)$ bereits gelöst. Es sind also Zustände bekannt, die folgende Gleichung erfüllen
%
\begin{align*}
	\Bigl(\mathrm{i}\hbar\partial_t - H_0(t)\Bigr) \fket{u_n^{(0)}} = \varepsilon_n^{(0)} \fket{u_n^{(0)}}
\end{align*}
%
Die $\varepsilon_n^{(0)}$ seien nicht entartet und wir wenden nun die nicht entartete Störungstheorie an.

Ansatz: Wir entwickeln die Energie und Zustände in Potenzen von $\lambda$.
%
\begin{align*}
	\varepsilon_n(\lambda) &= \varepsilon_n^{(0)} + \lambda \varepsilon_n^{(1)} + \lambda^2 \varepsilon_n^{(2)} + \dots \\
	\fket{u_n(\lambda)} &= \fket{u_n^{(0)}} + \lambda \fket{u_n^{(1)}} + \lambda^2 \fket{u_n^{(2)}} + \dots
\end{align*}
%
Wir setzen den Ansatz ein
%
\begin{align*}
	\Bigl(\mathrm{i}\hbar\partial_t - H_0(t) - \lambda H_1(t)\Bigr) \fket{u_n(\lambda)} = \varepsilon_n(\lambda) \fket{u_n(\lambda)}
\end{align*}
%
und erhalten die verschiedenen Ordnungen der Störungsreihe. Ein Koeffizientenvergleich in den Potenzen von $\lambda$ ergibt:
%
\begin{align}
	\Bigl(\mathrm{i}\hbar\partial_t - H_0(t)\Bigr) \fket{u_n^{(0)}} &= \varepsilon_n^{(0)} \fket{u_n^{(0)}} \tag{$0$}\label{eq:2013-11-22-0} \\
	\Bigl(\mathrm{i}\hbar\partial_t - H_0(t)\Bigr) \fket{u_n^{(1)}} - H_1(t) \fket{u_n^{(0)}} &= \varepsilon_n^{(1)} \fket{u_n^{(0)}} + \varepsilon_n^{(0)} \fket{u_n^{(1)}} \tag{$1$}\label{eq:2013-11-22-1} \\
	\Bigl(\mathrm{i}\hbar\partial_t - H_0(t)\Bigr) \fket{u_n^{(2)}} - H_1(t) \fket{u_n^{(1)}} &= \varepsilon_n^{(1)} \fket{u_n^{(1)}} + \varepsilon_n^{(0)} \fket{u_n^{(2)}} + \varepsilon_n^{(2)} \fket{u_n^{(0)}} \tag{$2$}\label{eq:2013-11-22-2}
\end{align}

Ohne Beschränkung der Allgemeinheit soll gelten
\[ \fbraket{u_n^{(0)} | u_n^{(k)}} = \delta_{0,k} \]

Wir berechnen nun die Eigenwertkorrekturen.
%
\begin{description}
	\item[$\fbra{u_n^{(0)}}\eqref{eq:2013-11-22-1}$:]
		\begin{itemalign}
			- \fbraket{u_n^{(0)} | H_1 | u_n^{(0)}} = \varepsilon_n^{(1)}
		\end{itemalign}
		Die Eigenwertkorrekturen erster Ordnung sind die Matrixelemente der Störung in den ungestörten Zuständen.
	\item[$\fbra{u_n^{(0)}}\eqref{eq:2013-11-22-2}$:]
		\begin{itemalign}
			- \fbraket{u_n^{(0)} | H_1 | u_n^{(1)}} = \varepsilon_n^{(2)}
		\end{itemalign}
		Man muss also die Korrektur erster Ordnung kennen, bevor die Korrektur zweiter Ordnung berechnet werden kann.
\end{description}

Als nächstes sind die Zustandskorrekturen an der Reihe. Dazu entwickeln wir $\fket{u_n^{(1)}}$ in der ungestörten Basis.
%
\begin{align*}
	\fket{u_n^{(1)}} &= \sum_{m \neq n} c_m^{(n)} \fket{u_m^{(0)}}
\end{align*}
%
Dies wird eingesetzt in \eqref{eq:2013-11-22-1}.
%
\begin{align}
	\sum_{m \neq n} c_m^{(n)} \left( \varepsilon_m^{(0)} - \varepsilon_n^{(0)} \right) \fket{u_m^{(0)}} - H_1(t) \fket{u_n^{(0)}} = \varepsilon_n^{(1)} \fket{u_n^{(0)}} \tag{$\ast$}\label{eq:2013-11-22-stern1}
\end{align}
%
Wir wenden von der linken Seite her $\fket{u_k^{(0)}}$ an, wobei $k \neq n$.
%
\begin{gather*}
	c_k^{(n)}\left( \varepsilon_k^{(0)} - \varepsilon_n^{(0)} \right) - \fbraket{u_k^{(0)} | H_1(t) | u_n^{(0)}} = 0 \\
	c_k^{(n)} = \frac{\fbraket{u_k^{(0)} | H_1(t) | u_n^{(0)}}}{\varepsilon_k^{(0)} - \varepsilon_n^{(0)}}
\end{gather*}
%
Für die Zustandskorrektur gilt also
%
\begin{align*}
	\fket{u_n^{(1)}} &= \sum_{m \neq n} \frac{\fbraket{u_m^{(0)} | H_1(t) | u_n^{(0)}}}{\varepsilon_m^{(0)} - \varepsilon_n^{(0)}} \fket{u_m^{(0)}}
\end{align*}

Das Vorgehen für die zweite Ordnung verläuft analog.
%
\begin{align*}
	\fket{u_n^{(2)}} = \sum_{m \neq n} d_m^{(n)} \fket{u_n^{(0)}}
\end{align*}
%
Dies setzen wir ein in \eqref{eq:2013-11-22-2}.
%
\begin{align*}
	\sum_{m \neq n} d_m^{(n)} \left( \varepsilon_m^{(0)} - \varepsilon_n^{(0)} \right) \fket{u_m^{(0)}} - H_1(t) \fket{u_n^{(1)}}
	&= \varepsilon_n^{(1)} \fket{u_n^{(1)}} + \varepsilon_n^{(2)} \fket{u_n^{(0)}}
\intertext{von links mit $\fbra{u_k^{(0)}}$}
	d_k^{(n)} \left( \varepsilon_k^{(0)} - \varepsilon_n^{(0)} \right) - \fbraket{u_k^{(0)} | H_1(t) | u_n^{(1)}}
	&= \varepsilon_n^{(1)} \fbraket{u_k^{(0)} | u_n^{(1)}}
\end{align*}
%
Es ergibt sich also in zweiter Ordnung:
%
\begin{align*}
	d_k^{(n)} &= \frac{\varepsilon_n^{(1)} \fbraket{u_k^{(0)} | u_n^{(1)}} + \fbraket{u_k^{(0)} | H_1(t) | u_n^{(1)}}}{\varepsilon_k^{(0)} - \varepsilon_n^{(0)}} \\
	\fket{u_n^{(2)}} &= \sum_{m \neq n} \frac{\varepsilon_n^{(1)} \fbraket{u_k^{(0)} | u_n^{(1)}} + \fbraket{u_k^{(0)} | H_1(t) | u_n^{(1)}}}{\varepsilon_k^{(0)} - \varepsilon_n^{(0)}} \fket{u_n^{(0)}}
\end{align*}

\begin{notice}[Bemerkungen:]
	\begin{enumerate}
		\item Höhere Ordnungen werden analog berechnet.

		\item Die korrigierten Zustände \[ \fket{u_n(\lambda)} \simeq \fket{u_n^{(0)}} + \lambda \fket{u_n^{(1)}} + \lambda^2 \fket{u_n^{(2)}} \] sind im Allgemeinen nicht mehr normiert.
	\end{enumerate}
\end{notice}

\subsection{Exponentieller Zerfall metastabiler Zustände}

Phänomenologisch kennt man exponentielle Zerfallsgesetze für metastabile Zustände.
%
\begin{align*}
	|\braket{\psi(t)|\psi(t_0)}|^2 = \mathrm{e}^{-\frac{\Gamma_0}{\hbar} t}
\end{align*}
%
Mit den Größen
%
\begin{itemize}
	\item $\Gamma_0/\hbar$: Zerfallsrate

	\item $\hbar/\Gamma_0$: Mittlere Lebensdauer

	\item $\Gamma_0$: Linienbreite im Energiespektrum
\end{itemize}

\begin{notice}[Frage:]
	Können wir dieses Zerfallsverhalten aus der Quantenmechanik herleiten?
\end{notice}

Das System sei zum Zeitpunkt $t_0 = 0$ im Zustand $\ket{\psi}$. Die Wahrscheinlichkeit, dass das System zur Zeit $t>0$ immer noch im Zustand $\ket{\psi}$ ist, lautet
%
\begin{align*}
	p_\psi(t) = |A_{\psi \to \psi}(t)|^2 = |\braket{\psi|\mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H t}|\psi}|^2
\end{align*}
%
Es wird eine Entwicklung für kleine Zeiten durchgeführt:
%
\begin{align*}
	p_\psi(t)
	&= \left|\Braket{\psi|\mathds{1} - \frac{\mathrm{i}}{\hbar} H t - \frac{1}{2 \hbar^2} H^2 t^2 + \mathcal{O}(t^3)|\psi}\right|^2 \\
	&= \left|1 - \frac{\mathrm{i}}{\hbar} \braket{H}_\psi t - \frac{1}{2 \hbar^2} \braket{H^2}_\psi t^2 + \mathcal{O}(t^3)\right|^2 \\
	&= 1 - \frac{1}{\hbar^2} \underbrace{\left( \braket{H^2}_\psi - \braket{H}_\psi^2 \right)}_{=(\Delta H)_\psi^2} t^2 + \mathcal{O}(t^3) \\
	&= 1 - \frac{1}{\hbar^2} (\Delta H)_\psi^2 t^2 + \mathcal{O}(t^3)
\end{align*}

\begin{notice}[Problem:]
	$p(t)$ ist parabolisch in $t$ für kleine Zeiten. Es ergibt sich also kein exponentieller Zerfall.
\end{notice}

Wir führen eine genauere Betrachtung durch, indem wir $\ket{\psi}$ in Eigenzuständen $\ket{E_n}$ von $H$ entwickeln:
%
\begin{align*}
	\ket{\psi} &= \sum_{n} \ket{E_n}\braket{E_n|\psi}
\end{align*}
%
damit
%
\begin{align*}
	A_{\psi \to \psi}(t)
	&= \sum_{n} |\braket{E_n|\psi}|^2 \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} E_n t} \\
	&\equiv \int \diff E\ \eta(E)\ \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} E t} \\
	\eta(E)
	&= \sum_{n} |\braket{E_n|\psi}|^2 \delta(E-E_n) \; , \quad \text{(Spektralfunktion)}
\end{align*}

Da $H$ ein kontinuierliches Spektrum hat, ist $\eta(E)$ eine kontinuierliche Funktion von $E$.
$\eta(E)$ und $A_{\psi \to \psi}(t)$ sind durch Fouriertransformation verbunden.
Wähle $\eta(E)$ so, dass $A_{\psi \to \psi}(t)$ exponentiell von $t$ abhängt
%
\begin{align*}
	\eta(E) = \frac{\frac{1}{2} \frac{\Gamma_0}{\pi}}{(E-E_0)^2 + \left( \frac{\Gamma_0}{2} \right)^2}
\end{align*}
%
damit
%
\begin{align*}
	A_{\psi \to \psi}(t)
	&= \exp\left\{ - \frac{1}{2} \frac{\Gamma_0}{\hbar} t - \frac{\mathrm{i}}{\hbar} E_0 t \right\} \\
	p_\psi(t)
	&= \mathrm{e}^{-\frac{\Gamma_0}{\hbar} t}
\end{align*}
%
Das kontinuierliche Spektrum und die Lorentz-förmige Spektralfunktion führen auf den exponentiellen Zerfall.

\begin{notice}[Bemerkungen:]
	\begin{enumerate}
		\item Durch den ersten Ansatz sind wir auf einen Widerspruch für kleine Zeiten gestoßen.
			%
			\begin{align*}
				\braket{H^n}_\psi
				&= \int \diff E\ E^n |\braket{E|\psi}|^2 \; , \quad n \geq 1 \\
				&= \int \diff E\ E^n \eta(E) = +\infty \; , \quad \text{für Lorentz-förmiges $\eta(E)$}
			\end{align*}
			%
			Die Taylor-Entwicklung für kleine Zeiten existiert nicht.

		\item In realen Systemen ist das Energiespektrum von unten beschränkt. Das heißt
			%
			\begin{align*}
				\eta(E) &= 0 \; , \quad E < E_\textrm{min}
			\end{align*}
			%
			Dies führt zu verlangsamtem Zerfall für lange Zeiten.
	\end{enumerate}
\end{notice}

Siehe dazu auch \fullcite{PhysRev.123.1503}.
