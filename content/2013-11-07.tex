% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 07.11.2013

\renewcommand{\printfile}{2013-11-07}

\subsection[Spin \texorpdfstring{$1/2$}{1/2} mit verborgenen Parametern]{Spin {\boldmath $1/2$} mit verborgenen Parametern}

Sämtliche Parameter eines quantenmechanischen Systems sind gegeben durch den Zustand
\[ \ket{\psi} = \ket{z+} \]

\begin{notice*}[Einstein:]
	Es gibt einen Parameter $\lambda$ verteilt mit $P(\lambda)$, der angibt, welcher Wert bei einer Messung in $\bm{n}$-Richtung herauskommt:
	\[ \tau \left( \bm{n},\lambda \middle| \ket{z+} \right) = \pm 1. \]
\end{notice*} 

\begin{notice*}[Aufgabe:]
	Konstruiere $\tau \left( \bm{n},\lambda \middle| \ket{z+} \right)$ und $p(\lambda)$ so, dass alle Voraussagen der Quantenmechanik erfüllt sind.
\end{notice*}

Lösung (vgl.\ Abb.~\ref{fig:2013-11-07-1})
%
\begin{enumerate}
	\item $\bm{\lambda}$ sei Eigenvektor auf einer Kugel
	\item
		\begin{itemalign}
			\tau\left(\bm{n},\bm{\lambda}|\Ket{z+}\right) =
			\begin{cases}
				+1 &\text{für $\bm{e}_z \cdot \bm{n} > \bm{\lambda} \cdot \bm{n}$} \\
				-1 &\text{für $\bm{e}_z \cdot \bm{n} < \bm{\lambda} \cdot \bm{n}$} 
			\end{cases}
		\end{itemalign}
	\item $p(\lambda)$ sei $1/(4\pi)$ gleich verteilt auf der Kugel
\end{enumerate}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (0,-2) -- (0,2.5) node[right] {$z$};
		\draw[->,DarkOrange3] (0,0) -- (45:2);
		\draw[->,DarkOrange3,dotted] (45:2) -- (90:2);
		\draw[->,DarkOrange3,dotted] (0,0) -- (-135:2);
		\draw[->,Purple] (0,0) -- (-60:2);
		\draw[->,Purple,dotted] (-135:0.5) -- (-60:2);
		\draw[->] (45:1) arc [start angle=45, end angle=90, radius=1];
		\draw[red] (180:2) arc [start angle=180, end angle=270, radius=2];
		\draw[blue] (-90:2) arc [start angle=-90, end angle=180, radius=2];
		\node at (67.7:0.7) {\color{DimGray} $\vartheta$}; %[67.7]
		\node[below right] at (45:1) {\color{DarkOrange3} $\bm{n}$};   %[45]
		\node[above right] at (-60:1) {\color{Purple} $\bm{\lambda}$}; %[30]
	\end{tikzpicture}
	\caption{Verborgene Parameter auf der Bloch-Einheitskugel.}
	\label{fig:2013-11-07-1}
\end{figure}

Dann gilt:
%
\begin{align*}
	\Braket{\sigma_n}_{\textrm{Einstein}}
	&= \int \tau\left(\bm{n},\bm{\lambda}|\Ket{z+}\right) \cdot p(\bm{\lambda}) \diff \Omega_\lambda \\
	&= \left(A_{\textrm{blau}} - A_{\textrm{rot}}\right) \cdot \frac{1}{4 \pi} \\
	&= \frac{1}{4 \pi} \left(2\pi \left(1+\cos\vartheta\right) - 2 \pi \left(1-\cos\vartheta\right)\right) \\
	&= \cos\vartheta .
\end{align*}
%
Dieses Ergebnis reproduziert also die Quantenmechanik.

\subsection[Bellsche Ungleichung]{Bellsche Ungleichung (1964)}

Bell zeigt, dass die Quantenmechanik und jede Theorie mit verborgenen Parametern für zwei Spin $1/2$ Teilchen zu messbaren Differenzen führen muss.

\minisec{Einsteins Variante:}

Für jeden Spin gibt es Funktionen $\tau_i\left(\bm{n}_i,\lambda|\ket{B}\right) = \pm 1$ $(i=1,2)$. Die perfekten Korrelationen ($\Braket{\sigma_\bm{n}^1 \cdot \sigma_\bm{n}^2}_{\ket{B}} = -1$) lauten:
%
\begin{align}
	\tau_1(\bm{n},\lambda) = -\tau_2(\bm{n},\lambda). \label{eq:2013-11-07-1}
\end{align}

Die Vorhersage für die quantenmechanische Korrelationsmessung lautet:
%
\[ E(n_1,n_2) = \Braket{B|\sigma_\bm{n}^1 \cdot \sigma_\bm{n}^2|B} \]

Nach Einsteins Rechenmethode:
%
\begin{align*}
	E(n_A,n_B)
	&= \int p(\lambda) \tau_1(\bm{n}_1,\lambda) \tau_2(\bm{n}_2,\lambda) \diff \lambda 
\intertext{Unter Verwendung von \eqref{eq:2013-11-07-1} folgt:}
	&= \int -p(\lambda) \tau_1(\bm{n}_1,\lambda) \tau_1(\bm{n}_2,\lambda) \diff \lambda 
\end{align*}

Dann folgt:
%
\begin{align*}
	E(\bm{n},\bm{m}) - E(\bm{n},\bm{\ell}) &= \int p(\lambda) \left[-\tau_1(\bm{n},\lambda)\tau_1(\bm{m},\lambda) + \tau_1(\bm{n},\lambda) \tau_1(\bm{\ell},\lambda)\right] \diff \lambda  \\
	&= \int \underbrace{p(\lambda)}_{\geq0} \bigg\{\underbrace{-\tau_1(\bm{n},\lambda)\tau_1(\bm{m},\lambda)}_{= \pm 1} \underbrace{\big[ 1-\tau_1(\bm{m},\lambda) \tau_1(\bm{\ell},\lambda) \big]}_{\geq 0} \bigg\} \diff \lambda  
\end{align*}
%
\begin{notice*}
	Es gilt \[ \left|\sum x_i\right| \leq \sum |x_i|. \] Da ein Integral im weiteren Sinne einer Summe entspricht lässt sich dies übertragen.
\end{notice*}
%
\begin{align*}
	\left|E(\bm{n},\bm{m}) - E(\bm{n},\bm{\ell})\right|
	&\leq \int p(\lambda) \left(1- \tau_1(\bm{m},\lambda) \tau_1(\bm{\ell},\lambda)\right) \\
	&= 1+ E(\bm{m},\bm{\ell})
\end{align*}
%
Dies ist die \acct{Bellsche Ungleichung} für zwei Spin $1/2$ Teilchen.\index{Bellsche Ungleichung!für zwei Spin $1/2$ Teilchen}

Für $E$ werden die quantenmechanischen Eigenwerte eingesetzt. Diese werden berechnet mittels
%
\begin{align*}
	E(\bm{n},\bm{m})
	&= \Braket{B|\sigma_\bm{n}^{(1)} \otimes \sigma_\bm{m}^{(2)}|B} \\
	&= \frac{1}{2} \left\langle \bra{+-} - \bra{-+} \middle| \sigma_\bm{n}^{(1)} \otimes \sigma_\bm{m}^{(2)} \middle| \ket{+-} - \ket{-+} \right\rangle \\
	&= \frac{1}{2} \left\langle \bra{+-} - \bra{-+} \middle| \sigma_z^{(1)} \left( \sigma_z^{(2)} \cos\vartheta + \sigma_x^{(2)} \sin\vartheta \right) \middle| \ket{+-} - \ket{-+} \right\rangle \\
	&= - \cos\vartheta
\end{align*}

Die linke Seite der Bellschen Ungleichung lautet also
\[ \left|E(\bm{n},\bm{m}) - E(\bm{n},\bm{\ell})\right| = \left|-\cos\vartheta + \cos(2\vartheta)\right| \]
und für die rechte Seite gilt
\[ 1 - \cos\vartheta . \]

Es muss also gelten
\[
	\tikz[baseline=(L.base)]{\node[draw=DarkOrange3,text=black,rounded corners,rectangle,inner sep=1ex] (L) {$\left|-\cos\vartheta + \cos(2\vartheta)\right|$};}
	\leq
	\tikz[baseline=(R.base)]{\node[draw=MidnightBlue,text=black,rounded corners,rectangle,inner sep=1ex] (R) {$1 - \cos\vartheta$};}
\]

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$\vartheta$};
		\draw[->] (0,-1.5) -- (0,1.5);
		\draw plot[domain=0:pi] (\x,{-cos(0.5*deg(\x))});
		\draw plot[domain=0:pi] (\x,{cos(deg(\x))});
		\draw[DarkOrange3,dotted] plot[domain=0:pi] (\x,{-cos(0.5*deg(\x))+cos(deg(\x))});
		\draw[DarkOrange3] plot[domain=0:pi] (\x,{abs(-cos(0.5*deg(\x))+cos(deg(\x)))});
		\draw[MidnightBlue] plot[domain=0:pi] (\x,{1-cos(0.5*deg(\x))});
		\draw (0.1,1) -- (-0.1,1) node[left] {$1$};
		\draw (0.1,-1) -- (-0.1,-1) node[left] {$-1$};
		\draw (pi,0.1) -- (pi,-0.1) node[below] {$\frac{\pi}{2}$};
		\node[above right] at (pi,1) {\color{DarkOrange3} linke Seite};
		\node[below right] at (pi,1) {\color{MidnightBlue} rechte Seite};
	\end{tikzpicture}
	\caption{Plot der Bellschen Ungleichung.}
	\label{fig:2013-11-07-3}
\end{figure}

Aus dem Bild in Abb.~\ref{fig:2013-11-07-3} ist zu erkennen, dass die linke Seite größer ist als die rechte. Das ist offensichtlich ein Widerspruch zur Bellschen Ungleichung.

Damit lässt sich experimentell entscheiden, ob die Quantenmechanik oder eine (beliebige) Einsteintheorie gilt.
Experimente zeigen, dass die Quantenmechanik richtig ist, siehe dazu auch \fullcite{PhysRevLett.49.91}. Eine Theorie mit lokalen verborgenen Parametern ist also ausgeschlossen.

