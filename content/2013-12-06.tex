% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 06.12.2013

\renewcommand{\printfile}{2013-12-06}

\subsection{Überblick: Allgemeine Prozesse}

Betrachten wir einen Hamiltonian $H_1$ in erster Ordnung. Wir wenden die Dipolnäherung an, also
%
\begin{align*}
	\mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \approx 1 + \mathrm{i} \bm{k} \cdot \bm{r} - \frac{(\bm{k} \cdot \bm{r})^2}{2} + \ldots
\end{align*}
%
Betrachten wir die einzelnen Terme:
%
\begin{itemize}
	\item $\Braket{a|\bm{r}|b}$: Dipolmatrixelement
	\item $\mathrm{i} \bm{k} \bm{r}$: Magnetische Dipolstrahlung
	\item $(\bm{k} \cdot \bm{r})^2$: Elektrischer Quadrupol
\end{itemize}

\paragraph{Weitere Prozesse:} Streuung von Photonen an Elektronen

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[scale=0.9]
	\begin{scope}
		\node[draw,rectangle,minimum size=1cm] (A) at (0,0) {};
		\draw (A.north west) -- node[below left] {$b$} ++(135:1);
		\draw (A.south west) -- node[above left] {$a$} ++(225:1);
		\draw[decorate,decoration=snake] (A.north east) -- node[below right] {$\bm{k}',\alpha'$} ++(45:1);
		\draw[decorate,decoration=snake] (A.south east) -- node[above right] {$\bm{k},\alpha$} ++(315:1);
	\end{scope}
	\node at (2,0) {$=$};
	\begin{scope}[xshift=3.5cm]
		\draw (0,0.5) node[dot] (A) {} -- (0,-0.5) node[dot] (B) {};
		\draw (A) -- node[below left] {$b$} ++(135:1);
		\draw (B) -- node[above left] {$a$} ++(225:1);
		\draw[decorate,decoration=snake] (A) -- node[below right] {$\bm{k}',\alpha'$} ++(45:1);
		\draw[decorate,decoration=snake] (B) -- node[above right] {$\bm{k},\alpha$} ++(315:1);
	\end{scope}
	\node at (5,0) {$+$};
	\begin{scope}[xshift=6.5cm]
		\draw (0,0) node[dot] (A) {};
		\draw (A) -- node[below left] {$b$} ++(135:1.5);
		\draw (A) -- node[above left] {$a$} ++(225:1.5);
		\draw[decorate,decoration=snake] (A) -- node[below right] {$\bm{k}',\alpha'$} ++(45:1.5);
		\draw[decorate,decoration=snake] (A) -- node[above right] {$\bm{k},\alpha$} ++(315:1.5);
	\end{scope}
	\node at (8.5,0) {$+$};
	\begin{scope}[xshift=10cm]
		\draw (0.5,-0.5) node[dot] (A) {};
		\draw (0,0.5) node[dot,label={left:$\bm{p}'$}] (B) {};
		\draw (A) -- node[below left] {$(n',k',\ell')$} (B);
		\draw (A) -- ++(240:1) node[right] {$\bm{p}$};
		\draw (B) -- ++(60:1) node[right] {$\bm{p}$};
		\draw[decorate,decoration=snake] (A) -- ++(10:1) coordinate (A1);
		\draw[decorate,decoration=snake] (B) -- ++(10:1) coordinate (B1);
	\draw (A1) -- node[right] {$(n,k,\ell)$} (B1);
	\end{scope}
	\end{tikzpicture}
	\caption{Der Term im Kasten wird als Selbstenergie des Elektrons bezeichnet (freier oder gebundener Lamb-Shift).}
	\label{fig:2013-12-06-1}
\end{figure}

\section{CQED und Jaynes-Cummings-Modell}

\subsection{Motivation}

Bisher haben viele Photonen in vielen Moden und vielen Polarisationen mit der Materie interagiert. Die einzige Möglichkeit zur Lösung solcher Probleme war die Störungstheorie. Die führte uns auf Fermis Goldene Regel.

Nun führen wir eine starke Vereinfachung ein. Ein Atom wird als Zwei-Niveau-System modelliert, also
%
\begin{align*}
	H_m &= \frac{\hbar \omega_m}{2} \sigma_z
\end{align*}

Das Strahlungsfeld wird auf eine Mode heruntergebrochen
%
\begin{align*}
	H_c &= \hbar \omega_c \left( a^\dag a + \cancel{$\displaystyle \frac{1}{2}$} \right)
\end{align*}
%
Dies wird praktisch realisiert durch eine Cavity aus zwei Spiegeln.

\subsection{Jaynes-Cummings-Modell}
\index{Jaynes-Cummings-Modell}

Nun müssen das Atom und das Strahlungsfeld wechselwirken. Diese Wechselwirkung kommt zustande, indem wir ein Atom im Grundzustand vernichten und eins im angeregten Zustand erzeugen (et vice versa).
%
\begin{align*}
	H
	&= H_m + H_c + H_\textnormal{WW} \\
	&= \frac{\hbar \omega m}{2} \sigma_z + \hbar \omega_c a^\dag a - \frac{\mathrm{i} \hbar \Omega_0}{2} (a \sigma_+ - a^\dag \sigma_-)
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (-0.1,0) node[left] {$0$} -- (2,0);
		\draw[->] (0,-1.5) -- (0,4) node[left] {$E/\hbar$};
		\draw (0.1,1) -- (-0.1,1) node[left] {$\omega_m/2$};
		\draw (0.1,-1) -- (-0.1,-1) node[left] {$-\omega_m/2$};
		\foreach \y [count=\i] in {-1.2,0.8,1.8,2.8} {
			\draw[MidnightBlue] (0.5,\y) -- (1.5,\y) node[right] {$|g,\pgfmathparse{int(\i - 1)}\pgfmathresult\rangle$};
		}
		\foreach \y [count=\i] in {1.2,2.2,3.2} {
			\draw[DarkOrange3] (0.5,\y) -- (1.5,\y) node[right] {$|e,\pgfmathparse{int(\i - 1)}\pgfmathresult\rangle$};
		}
		\foreach \y [count=\i] in {-1,1,2,3} {
			\node at (5,\y) {\pgfmathparse{int(\i - 1)}\pgfmathresult};
		}
		\node[right] at (2.5,2) {Dubletts};
		\draw[decorate,decoration=brace] (2.5,1.2) -- node[right] {$\Delta = \omega_m - \omega_c$} (2.5,0.8);
	\end{tikzpicture}
	\caption{Das Spektrum des ungestörten Hamiltonian ($H_\textnormal{WW}=0$).}
	\label{fig:2013-12-06-2}
\end{figure}

Definiere $\mathcal{N}$:
%
\begin{align*}
	\mathcal{N} &\equiv \frac{1}{2} (1 + \sigma_z) + a^\dag a \\
	[\mathcal{N},H] &= [\mathcal{N},H_\textnormal{WW}] = 0
\end{align*}
%
$\mathcal{N}$ ist also eine Erhaltungsgröße und liefert eine gute Quantenzahl. Man diagonalisiert im Dublett, also in der Basis $\{ \ket{e,n}, \ket{g,n+1} \}$.
%
\begin{align*}
	H_n &= \hbar \omega_c \left( n + \frac{1}{2} \right) \mathds{1} + V_n \\
	V_n &= \frac{\hbar}{2}
	\begin{pmatrix}
		\Delta & - \mathrm{i} \Omega_n \\
		+ \mathrm{i} \Omega_n & - \Delta \\
	\end{pmatrix}
\end{align*}
%
mit $\Omega_n = \Omega_0 \sqrt{n + 1}$.

\begin{proof}
	Wähle zum Beispiel:
	%
	\begin{align*}
		\Braket{e,n|H_\textnormal{WW}|g,n+1}
		&= \Braket{e,n| \left( - \frac{\mathrm{i} \hbar \Omega_0}{2} \right) (a \sigma_+ - a^\dag \sigma_-) |g,n+1} \\
		&= - \frac{\mathrm{i} \hbar \Omega_0}{2} \sqrt{n+1} \\
		&= - \frac{\mathrm{i} \hbar \Omega_n}{2}
	\end{align*}
\end{proof}

Das heißt, das Dublett verhält sich wie ein Spin $1/2$ im Magnetfeld.
%
\begin{align*}
	V_n = \frac{\hbar}{2} \left( \Delta \Sigma_z + \Omega_n \Sigma_g \right)
\end{align*}
%
Das Magnetfeld verläuft in Richtung
%
\begin{align*}
	\tan \theta_n = \frac{\Omega_n}{\Delta}
\end{align*}

Berechnet man die Eigenzustände und Eigenenergien von $V_n$, so findet man
%
\begin{align*}
	E_n^\pm &= \left( n + \frac{1}{2} \right) \hbar \omega_c \pm \frac{\hbar}{2} \sqrt{ \Delta^2 + \Omega_n^2 } \\
	\ket{+n} &= \cos\frac{\theta_n}{2} \ket{e,n} + \mathrm{i} \sin\frac{\theta}{2} \ket{g,n+1} \\
	\ket{-n} &= \sin\frac{\theta_n}{2} \ket{e,n} - \mathrm{i} \cos\frac{\theta}{2} \ket{g,n+1}
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-3.3,0) -- (3.5,0) node[below] {$\Delta/\Omega_n$};
		\draw[->] (-3,-0.3) -- (-3,4) node[left] {$E_n^{\pm}$};
		\draw[MidnightBlue] plot[domain=-3:3] (\x,{0.5*sqrt(\x*\x+1)+2});
		\node[MidnightBlue,above] at (0,2.5) {$\ket{+,n}$};
		\draw[MidnightBlue] plot[domain=-3:3] (\x,{-0.5*sqrt(\x*\x+1)+2});
		\node[MidnightBlue,below] at (0,1.5) {$\ket{-,n}$};
		\draw[DarkOrange3] plot[domain=-3:3] (\x,{0.5*\x+2});
		\node[DarkOrange3,below right] at (2,3) {$\ket{e,n}$};
		\draw[DarkOrange3] plot[domain=-3:3] (\x,{-0.5*\x+2});
		\node[DarkOrange3,above right] at (2,1) {$\ket{g,n+1}$};
	\end{tikzpicture}
	\caption{Grafische Darstellung der \acct{dressed states}.}
	\label{fig:2013-12-06-3}
\end{figure}

Es existieren zwei Extremfälle:
%
\begin{itemize}
	\item Resonanz bei $\Delta = 0$. Die effektive Magnetfeldrichtung zeigt in $y$-Richtung.
	\item Large detuning $|\Delta| \gg \Omega_n$.
\end{itemize}

\subsection{Resonanz}

Sei nun per Konstruktion
%
\begin{align*}
	\Delta = 0 \; , \quad
	\theta_n = \frac{\pi}{2} \; , \quad
	\ket{\pm,n} = \frac{1}{\sqrt{2}} \left( \ket{e,n} \pm \mathrm{i} \ket{g,n+1} \right)
\end{align*}
%
für $t=0$
%
\begin{align*}
	\ket{\psi(0)} &= \ket{e,n} = \frac{1}{\sqrt{2}} \left( \ket{+n} + \ket{-n} \right) \\
	\ket{\psi(t)} &= \cos\frac{\Omega_n t}{2} \ket{e,n} + \sin\frac{\Omega_n t}{2} \ket{g,n+1}
\end{align*}
%
Man kann dies als spontane oszillierende Emission betrachten.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$t$};
		\draw[->] (0,-0.3) -- (0,1.5) node[left] {$P_e(t)$};
		\draw[MidnightBlue] plot[samples=41,smooth,domain=0:4] (\x,{cos(pi/2 * \x r)^2});
		\draw (1,0.1) -- (1,-0.1) node[below] {$\pi/\Omega_n$};
	\end{tikzpicture}
	\caption{Wahrscheinlichkeit für den angeregten Zustand.}
	\label{fig:2013-12-06-4}
\end{figure}

Sei das Lichtfeld anfänglich in einem kohärenten Glauber-Zustand:
%
\begin{align*}
	\ket{\alpha}
	&= \mathrm{e}^{-\frac{|\alpha|^2}{2}} \sum_n \frac{\alpha^n}{n!} \ket{n} \\
	&\equiv \sum_n c_n \ket{n} \\
	p_n &= c_n^2 \\
	\ket{\psi(0)} &= \sum_n c_n \ket{e,n} \\
	\ket{\psi(t)} &= \sum_n c_n \left( \cos\frac{\Omega_n t}{2} \ket{e,n} + \sin\frac{\Omega_n t}{2} \ket{g,n+1} \right) \\
	P_e(t) &= \sum_n c_n^2 \cos^2\frac{\Omega_n t}{2} = \sum_n p_n \cos^2\frac{\Omega_0 \sqrt{n+1} t}{2}
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$t$};
		\draw[->] (0,-0.3) -- (0,1.5) node[left] {$P_e(t)$};
		\draw[MidnightBlue] plot[samples=100,smooth,domain=0:4] (\x,{cos(pi/2*\x r)^2*(random()-0.5)+0.5});
		\draw (pi/4,0.1) -- (pi/4,-0.1) node[below] {$t_\textnormal{coll} = \frac{\pi}{\Omega_0}$};
		\draw[<-] (3*pi/4,1) -- (3*pi/4,1.5) node[above] {revival $t_r = \frac{4 \pi}{\Omega_0} \sqrt{\bar{n}} \equiv |\alpha|^2$};
	\end{tikzpicture}
	\caption{Wahrscheinlichkeit des angeregten Zustandes.}
	\label{fig:2013-12-06-5}
\end{figure}

Klassicher Limes: $\sqrt{\bar{n}} = \frac{1}{\Omega_0} \to 0$.

\subsection{Starke Verstimmung}

Sei nun $\Delta \gg \Omega_n$:
%
\begin{align*}
	E_n^\pm
	&\approx \left( n + \frac{1}{2} \right) \hbar \omega_c \pm \frac{\hbar}{2} \left( \Delta + \frac{\Omega_n^2}{2 \Delta} \right)
\end{align*}
%
Es sind keine Übergänge mehr möglich, demnach bleibt ein Effekt.
%
\begin{itemize}
	\item Vom Standpunkt des Atoms aus:
		%
		\begin{align*}
			\Delta E_e
			&\equiv E\bigl( \ket{+,n} \bigr) - E\bigl( \ket{e,n} \bigr) \\
			&= \frac{\hbar}{4} \frac{\Omega_n^2}{\Delta} = \frac{\hbar}{4 \Delta} \Omega_0^2 (n+1) \\
			&= \hbar s_0 (n+1) \\
			\Delta E_g
			&= - \hbar s_0 n
		\end{align*}
		%
		also
		%
		\begin{align*}
			\delta \omega_m &= (2n+1) s_0
		\end{align*}
		%
		Die Übergangsfrequenz wird verschoben und erzeugt einen entsprechenden Lamb-Shift.

	\item Vom Standpunkt des Strahlungsfeldes aus:
		Die $\ket{+,n}$-Niveaus $E_{n+1}^\pm - E_n^\pm = \pm \hbar s_0$. Die Frequenz wird um $\delta \omega_c = \pm s_0$ verschoben. Das Atom wirkt wie ein transparentes Dielektrikum.
\end{itemize}

\subsection{Messung eines Subsystems durch ein zweites}

Die $E_n^\pm$, die gegeben sind durch
%
\begin{align*}
	E_n^\pm = \frac{\hbar \omega_m}{2} + \left( n + \frac{1}{2} \right) \hbar \omega_c
	\begin{dcases}
		+ \hbar s_0 & n+1 \\
		- \hbar s_0 & n \\
	\end{dcases}
\end{align*}
%
sind Eigenwerte zu
%
\begin{align*}
	H_\textnormal{eff} &=
	\frac{\hbar  \omega_c}{2} \left( a^\dag a + \frac{1}{2} \right)
	+ \frac{\hbar}{2} \omega_m \sigma_z
	+ \hbar s_0 \sigma_z a^\dag a
	+ \hbar s_0 \sigma_+ \sigma_-
\end{align*}

Es gibt zwei mögliche Gruppierungen.
%
\begin{itemize}
	\item
		\begin{itemalign}
			V_1 = \frac{\hbar}{2} \sigma_z (\underbrace{\omega_m + s_0 a^\dag a}_{B_\textnormal{eff}})
		\end{itemalign}
	\item
		\begin{itemalign}
			V_2 = \frac{\hbar}{2} (\omega_m + 2 s_0 \sigma_z) a^\dag a
		\end{itemalign}
\end{itemize}
