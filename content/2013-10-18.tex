% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 18.10.2013

\renewcommand{\printfile}{2013-10-18}

\section[Quantenmechanik des Spin \texorpdfstring{$1/2$}{1/2} Systems]{Quantenmechanik des Spin \boldmath $1/2$ Systems}

\subsection{Hilbertraum \texorpdfstring{$\mathcal{H}$}{H} und \texorpdfstring{$\sigma_z$}{Sigma z}-Darstellung}

Die Stern-Gerlach-Experimente zeigen, dass es für den Spin offensichtlich zwei linear unabhängige Zustände gibt. Der Hilbertraum hat in diesem Falle die Dimension
\[
	\dim(\mathcal{H}) = 2 .
\] 
Als Basis werden folgende Vektoren gewählt:
%
\begin{align*}
	\{ \Ket{1}, \Ket{2}\} &\hateq \left\{ \begin{pmatrix} 1 \\ 0 \end{pmatrix} , \begin{pmatrix} 0 \\ 1 \end{pmatrix} \right\} \hateq  \{ \Ket{z+}, \Ket{z-}\} 
\end{align*}
%
Dies sind die Eigenvektoren des zu $\sigma_z$ gehörenden Operators. Dessen Eigenwerte sind $\pm 1$:
%
\begin{align*}
	\sigma_z \Ket{z+} &= 1 \cdot \Ket{z+} \\
	\sigma_z \Ket{z-} &= -1 \cdot \Ket{z-}
\end{align*}

Für den $\sigma_z$ Operator lässt sich also folgende Matrixdarstellung finden:
%
\begin{align*}
	\sigma_z &= \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}
\end{align*}

Für einen beliebigen Zustand $\Ket{\psi}$ gilt unter anderem:
%
\begin{align*}
	\Ket{\psi} &= c_1 \Ket{z+} + c_2 \Ket{z-} \; , \quad c_1,c_2 \in \mathbb{C}
\end{align*}

Für die \acct{Normierung} gilt folgende Definition: 
%
\begin{align*}
	1 &\stackrel{!}{=} \Braket{\psi|\psi} \\
	&= c_1 c_1^\ast + c_2 c_2^\ast \\
	&= |c_1|^2 + |c_2|^2
\end{align*}

\begin{example}[\ForwardToEnd \hspace{0.5em} Experiment 1]~
	\begin{center}
		\begin{tikzpicture}[
				O/.style={draw,rectangle},
				SG/.style={draw,rectangle,minimum size=1cm}
			]
			\node[O] (O) at (0,0) {O};
			\node[SG] (SGn1) at (2,0) {SG,$n$};
			\node[SG] (SGn2) at ($(SGn1.30)+(2,0)$) {SG,$n$};
			\draw (O) edge[->] (SGn1);
			\draw[->] (SGn1.30)  -- node[above] {$+$} (SGn2);
			\draw[-|] (SGn1.-30) -- node[below] {$-$} ++(1,0);
			\draw[->] (SGn2.30)  -- node[above] {$+$} ++(1,0);
		\end{tikzpicture}
	\end{center}

	\begin{align*}
		\prob[\sigma_z \hateq +1 | \Ket{z+}]
		&=  \Braket{\psi |P_{\Ket{z+}}|\psi} \\
		&= \Braket{z+|z+}\Braket{z+|z+} = 1 \\
		\prob[\sigma_z \hateq -1 | \Ket{z+}]
		&= |\Braket{z-|z+}|^2 = 0
	\end{align*}
\end{example}

\subsection{Spin-Operator}

\begin{notice*}[Frage:] 
	Welcher Operator $\sigma_n$ gehört zur physikalischen Größe \enquote{Spin in $n$-Richtung}?
\end{notice*}

Wir wissen bereits:
%
\begin{itemize}
	\item $\sigma_n$ entspricht einer hermiteschen $2 \times 2$-Matrix, der Form: 
	%
	\begin{align*}
		\sigma_n &= \begin{pmatrix} a & b \\ b^\ast & d \end{pmatrix} 
	\end{align*}
	%
	mit den Eigenwerten $\pm 1$.
	\item Somit ist die Spur der Matrix, gleich der Summe der Eigenwerte:
	%
	\begin{align*}
		\tr(\sigma_n) &= \sum \, \text{Eigenwerte} = 0 \\
		\det(\sigma_n) &= -1 \\
		\implies a &\stackrel{!}{=} -d
	\end{align*}
	%
	\item Somit gilt also für die Determinante:
	%
	\begin{align*}
		\det(\sigma_n) &= ad-bb^\ast = -a^2-bb^\ast = -1
	\end{align*}
	%
\end{itemize}

Somit lässt sich der \acct{Spin-Operator $\sigma_n$} wie folgt parametrisieren:
%
\begin{align*}
	\sigma_n &= \begin{pmatrix} \cos(\beta) & \exp(-\mathrm{i}\alpha) \sin(\beta) \\  \exp(\mathrm{i}\alpha) \sin(\beta) & -\cos(\beta) \end{pmatrix}
\end{align*}
%
mit  $\alpha = \alpha(\bm{n})$ und $\beta = \beta(\bm{n})$.

Für die Eigenvektoren gilt somit:
%
\begin{align*}
	\sigma_n \cdot
	\begin{pmatrix}
		\cos\left(\frac{\beta}{2}\right) \\
		\exp(\mathrm{i} \alpha )  \sin\left(\frac{\beta}{2}\right)
	\end{pmatrix}
	&= +1
	\begin{pmatrix}
		\cos\left(\frac{\beta}{2}\right) \\
		\exp(\mathrm{i} \alpha)  \sin\left(\frac{\beta}{2}\right)
	\end{pmatrix}
	&& {\color{DarkOrange3}\Ket{n+}} \\
	\sigma_n \cdot
	\begin{pmatrix}
		\sin\left(\frac{\beta}{2}\right) \\
		-\exp(\mathrm{i} \alpha) \cos\left(\frac{\beta}{2}\right)
	\end{pmatrix} &= -1
	\begin{pmatrix}
		\sin\left(\frac{\beta}{2}\right) \\
		-\exp(\mathrm{i} \alpha) \cos\left(\frac{\beta}{2}\right)
	\end{pmatrix}
	&& {\color{DarkOrange3}\Ket{n-}}\\
\end{align*}

\minisec{Bestimmung von $\sigma_x$}

Gesucht sind also folgende Funktionen für $\sigma_n$, um $\sigma_x$ zu erhalten:
\[ \alpha(x) \text{ und } \beta(x) \]
sodass für den Eigenvektor $\Ket{x+}$ gilt:
\[ \Ket{x+} =
	\begin{pmatrix}
		\cos\left(\frac{\beta_x}{2}\right) \\
		\exp(\mathrm{i} \alpha_x)  \sin\left(\frac{\beta_x}{2}\right)
	\end{pmatrix}
\]
Dazu wird vom Stern-Gerlach Experiment 2a ausgegangen:

\begin{center}
	\begin{tikzpicture}[
			O/.style={draw,rectangle},
			SG/.style={draw,rectangle,minimum size=1cm}
		]
		\node[O] (O) at (0,0) {O};
		\node[SG] (SGn1) at (2,0) {SG,$z$};
		\node[SG] (SGn2) at ($(SGn1.30)+(2,0)$) {SG,$x$};
		\draw (O) edge[->] (SGn1);
		\draw[->] (SGn1.30)  -- node[above] {$\Ket{z+}$} (SGn2);
		\draw[-|] (SGn1.-30) -- ++(1,0);
		\draw[->] (SGn2.30)  -- ++(1,0);
		\draw[->] (SGn2.-30) -- ++(1,0);
	\end{tikzpicture}
\end{center}

Für die Wahrscheinlichkeit gilt also:
%
\begin{align*}
	\prob[\sigma_x \hateq +1 | \Ket{z+}] &\stackrel{!}{=} \frac{1}{2} \\
	&\stackrel{\text{P2}}{=} |\Braket{x+|z+}|^2 \\
	&= \left|
		\begin{pmatrix}
			\cos\left(\frac{\beta_x}{2}\right) & \exp(-\mathrm{i} \alpha_x)  \sin\left(\frac{\beta_x}{2}\right)
		\end{pmatrix}
		\begin{pmatrix}
			1 \\
			0
		\end{pmatrix}
	\right|^2 \\
	&= \cos^2\left(\frac{\beta_x}{2}\right)
\end{align*}

Ebenso folgt für:
%
\begin{align*}
	\prob[\sigma_x \hateq -1 | \Ket{z+}] &\stackrel{!}{=} \frac{1}{2} \\
	&= \sin^2\left(\frac{\beta_x}{2}\right)
\end{align*}
%
Beide ermittelten Wahrscheinlichkeiten entsprechen dem experimentell ermittelten Ergebnis von $1/2$ genau dann, wenn:
%
\begin{align*}
	\beta_x &= \frac{\pi}{2} \\
	\implies \sigma_x &= 
	\begin{pmatrix}
		0 & \exp(-\mathrm{i}\alpha_x) \\
		\exp(\mathrm{i} \alpha_x) & 0
	\end{pmatrix} 
\end{align*}
%
Für die Bestimmung von $\alpha_x$ wird zunächst analog zur Bestimmung der $\sigma_y$-Matrix vorgegangen.

\minisec{Bestimmung von $\sigma_y$}

Analog zur Bestimmung von $\sigma_x$ folgt für $\sigma_y$:
%
\begin{align*}
	\beta_y &= \frac{\pi}{2} \\
	\implies \sigma_y &=
	\begin{pmatrix}
		0 & \exp(-\mathrm{i}\alpha_y) \\
		\exp(\mathrm{i} \alpha_y) & 0
	\end{pmatrix} 
\end{align*}
%
Um nun $\alpha_x$ und $\alpha_y$ zu bestimmen, wird das Stern-Gerlach-Experiment 2c betrachtet:

\begin{center}
	\begin{tikzpicture}[
			O/.style={draw,rectangle},
			SG/.style={draw,rectangle,minimum size=1cm}
		]
		\node[O] (O) at (0,0) {O};
		\node[SG] (SGn1) at (2,0) {SG,$x$};
		\node[SG] (SGn2) at ($(SGn1.30)+(2,0)$) {SG,$y$};
		\draw (O) edge[->] (SGn1);
		\draw[->] (SGn1.30)  -- node[above] {$\Ket{x+}$} (SGn2);
		\draw[-|] (SGn1.-30) -- ++(1,0);
		\draw[->] (SGn2.30)  -- ++(1,0) node[right] {$1/2$};
		\draw[->] (SGn2.-30) -- ++(1,0) node[right] {$1/2$};
	\end{tikzpicture}
\end{center}

Für die Wahrscheinlichkeit folgt nach diesem Experiment:
%
\begin{align*}
	\prob[\sigma_y \hateq +1 | \Ket{x+}] &\stackrel{!}{=} \frac{1}{2} \\
	&= |\Braket{y+|x+}|^2 \\
	&= \left|
	        \begin{pmatrix}
	      	  \cos\left(\frac{\beta}{2}\right) & \exp(-\mathrm{i} \alpha_y) \sin\left(\frac{\beta}{2}\right)
	        \end{pmatrix}
	        \begin{pmatrix}
	      	  \cos\left(\frac{\beta}{2}\right) \\
	      	  \exp(\mathrm{i} \alpha_x )  \sin\left(\frac{\beta}{2}\right)
	        \end{pmatrix}
	\right|^2 \\
	&= \left|
		\begin{pmatrix}
	      	\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \exp(-\mathrm{i} \alpha_y)
	      \end{pmatrix}
	      \begin{pmatrix}
	      	\frac{1}{\sqrt{2}} \\
	      	\frac{1}{\sqrt{2}} \exp(\mathrm{i} \alpha_x)
	      \end{pmatrix}
	\right|^2 \\
	&= \left|
		\frac{1}{2} + \frac{1}{2} \exp(\mathrm{i}(\alpha_x - \alpha_y))
	\right|^2 \\
	&= \frac{1}{2} \bigg|
		\underbrace{1+ \exp(\mathrm{i}(\alpha_x - \alpha_y))}_{\stackrel{!}{=} \sqrt{2} }
	\bigg|^2 \\
	\implies \alpha_x - \alpha_y &= \pm \frac{\pi}{2}
\end{align*}

Nach Konvention sind die beiden Winkel wie folgt festgelegt:
\[ \alpha_x = 0 \text{ und } \alpha_y = \frac{\pi}{2} \]

\subsection{Paulimatrizen}

Mit den Erkenntnissen aus dem vorigen Abschnitt lauten die \acct{Pauli-Matrizen} und ihre Eigenvektoren in endgültiger Form:
%
\begin{align*}
% Sigma x
	\sigma_x &=
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
	&& \Ket{x+} = \frac{1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		1
	\end{pmatrix}
	&& \Ket{x-} = \frac{1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		-1
	\end{pmatrix} \\ 
% Sigma y
	\sigma_y &=
	\begin{pmatrix}
		0 & -\mathrm{i} \\
		\mathrm{i} & 0
	\end{pmatrix}
	&& \Ket{y+} = \frac{1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		\mathrm{i}
	\end{pmatrix}
	&& \Ket{y-} = \frac{1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		-\mathrm{i}
	\end{pmatrix} \\
% Sigma z
	\sigma_z &=
	\begin{pmatrix}
		1 & 0 \\
		0 & -1
	\end{pmatrix}
	&& \Ket{z+} =
	\begin{pmatrix}
		1 \\
		0
	\end{pmatrix}
	&& \Ket{z-} =
	\begin{pmatrix}
		0 \\
		1
	\end{pmatrix}
\end{align*}
%
Die Pauli-Matrizen besitzen folgende Eigenschaften:
%
\begin{enumerate}
	\item $\sigma_i\sigma_j = \delta_{ij} + \mathrm{i} \varepsilon_{ijk}\sigma_k$
	\item $\sigma_i^2 = \mathds{1}$
	\item $\sigma_x \sigma_y = \mathrm{i} \sigma_z$
	\item $[\sigma_i,\sigma_j]= 2 \mathrm{i} \varepsilon_{ijk} \sigma_k$
	\item $\bm{\sigma}= \begin{pmatrix} \sigma_x \\ \sigma_y \\ \sigma_z \end{pmatrix} $
	\item $(\bm{\sigma} \cdot \bm{a}) (\bm{\sigma} \cdot \bm{b}) = \bm{a}\bm{b} \mathds{1} + \mathrm{i} \bm{\sigma} (\bm{a}\times\bm{b}) $ 
\end{enumerate}

\subsection{Allgemeines \texorpdfstring{$\sigma_n$}{sigma-n}}

\begin{center}
	\begin{tikzpicture}[
			O/.style={draw,rectangle},
			SG/.style={draw,rectangle,minimum size=1cm}
		]
		\node[O] (O) at (0,0) {O};
		\node[SG] (SGn1) at (2,0) {SG,$\bm{n}$};
		\node[SG] (SGn2) at ($(SGn1.30)+(2,0)$) {SG,$x$};
		\draw (O) edge[->] (SGn1);
		\draw[->] (SGn1.30)  -- node[above] {$\Ket{n+}$} (SGn2);
		\draw[-|] (SGn1.-30) -- ++(1,0);
		\draw[->] (SGn2.30)  -- ++(1,0);
		\draw[->] (SGn2.-30) -- ++(1,0);
	\end{tikzpicture}
\end{center}

Für die Bildung des Erwartungswertes folgt:
%
\begin{align*}
	\Braket{\sigma_x}_{\Ket{n+}} &= \Braket{n+|\sigma_x|n+} \\
	\Braket{\sigma_y}_{\Ket{n+}} &= \Braket{n+|\sigma_y|n+} \\
	\Braket{\sigma_z}_{\Ket{n+}} &= \Braket{n+|\sigma_z|n+}
\end{align*}
%
Wir wissen:
%
\begin{align*}
	\Braket{\sigma_{x/y/z}} &= \begin{cases} +1 & \text{falls} \; \bm{n}= +\hat{\bm{x}} \\ -1 & \text{falls} \; \bm{n}= -\hat{\bm{x}} \end{cases}
\end{align*}
%
$\Braket{\sigma_{x/y/z}}$ kann zu einen dreidimensionalen Vektor zusammengefasst werden:
\[ \Braket{\bm{\sigma}_{\Ket{n+}}} \]
Aus der Rotationssymmetrie und der Skizze in Abbildung~\ref{fig:2013-10-18-1} folgt somit:
\[ \Braket{\bm{\sigma}}_{\Ket{n+}} = \bm{n} =
	\begin{pmatrix}
		\sin\theta \cos\phi \\
		\sin\theta \sin\phi \\
		\cos\theta
	\end{pmatrix}
\]

\begin{figure}[htpb]
	\centering
	\tdplotsetmaincoords{60}{110}
	\begin{tikzpicture}[tdplot_main_coords]
		\coordinate (O) at (0,0,0);
		\draw[->] (0,0,0) -- (2,0,0) node[below left]{$x$};
		\draw[->] (0,0,0) -- (0,2,0) node[below right]{$y$};
		\draw[->] (0,0,0) -- (0,0,2) node[right]{$z$};

		\tdplotsetcoord{P}{2}{40}{50}
		\draw[->] (O) -- (P);
		\draw[dotted] (O) -- (Pxy);
		\draw[dotted] (P) -- (Pxy);
		\draw[dotted] (Px) -- (Pxy);
		\draw[dotted] (Py) -- (Pxy);
		\tdplotdrawarc[DarkOrange3,->]{(O)}{1.2}{0}{50}{below}{$\phi$}

		\tdplotsetthetaplanecoords{50}
		\tdplotdrawarc[DarkOrange3,->,tdplot_rotated_coords]{(0,0,0)}{1.5}{0}{40}{above right}{$\theta$}
	\end{tikzpicture}
	\caption{Winkel in Kugelkoordinaten.}
	\label{fig:2013-10-18-1}
\end{figure}

Für die Erwartungswerte der einzelnen Pauli-Matrizen gilt somit:
%
\begin{align*}
% Sigma x
	\Braket{n+|\sigma_x|n+} &=
	\begin{pmatrix}
		\cos\left(\beta/2\right) & \exp(-\mathrm{i} \alpha) \sin(\beta/2)
	\end{pmatrix}
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
	\begin{pmatrix}
		\cos\left(\beta/2\right) \\
		\exp(\mathrm{i} \alpha) \sin(\beta/2)
	\end{pmatrix} \\
	&= \cos\alpha \sin\beta \stackrel{!}{=} \sin\theta \cos\phi \\
% Sigma y
	\Braket{n+|\sigma_y|n+} &= \sin\alpha \sin\beta \\
	&\stackrel{!}{=} \sin\theta \sin\phi \\
% Sigma z
	\Braket{n+|\sigma_z|n+} &= \cos\beta \\
	&\stackrel{!}{=} \cos\theta \\
	\implies \alpha &= \phi \text{ und } \beta = \theta \\
	\implies \sigma_n &=
	\begin{pmatrix}
		\cos\theta & \exp(-\mathrm{i} \phi) \sin\theta \\
		\exp(\mathrm{i} \phi) \sin\theta & -\cos\theta
	\end{pmatrix}
\end{align*}

\section[Dynamik des Spin \texorpdfstring{$1/2$}{1/2}: Larmor-Präzession]{Dynamik des Spin \boldmath $1/2$: Larmor Präzession}

Wir legen ein konstantes Magnetfeld $\bm{B} = B \bm{e}_z$ an.

Der Hamiltonoperator lautet:
%
\begin{align*}
	H &= -\bm{\mu} \cdot \bm{B} = g \mu_B \frac{1}{2} \bm{\sigma} \cdot \bm{B} = \frac{\hbar}{2} \omega \sigma_z 
\end{align*}
%
Wobei hier aber nur der Spin-Anteil betrachtet wurde. Außerdem gilt:
\[ \omega = \frac{g \mu_B B_z}{\hbar} \] 

Die Schrödinger-Gleichung lautet somit:
%
\begin{align*}
	\mathrm{i} \hbar \frac{\diff}{\diff t} \ket{\psi(t)} &= \frac{\hbar}{2} \omega \sigma_z \ket{\psi(t)} 
\intertext{mit:}
	\ket{\psi(t)} &=
	\begin{pmatrix}
		c_+(t) \\
		c_-(t)
	\end{pmatrix} \\
	\implies \mathrm{i} \hbar \frac{\diff}{\diff t}
	\begin{pmatrix}
		c_+(t) \\
		c_-(t)
	\end{pmatrix}
	&= \frac{\hbar}{2} \omega
	\begin{pmatrix}
		1 & 0 \\
		0 & -1
	\end{pmatrix}
	\begin{pmatrix}
		c_+(t) \\
		c_-(t)
	\end{pmatrix} \\
	\mathrm{i} \partial_t c_{\pm}(t) &= \frac{\omega}{2} (\pm 1) c_{\pm} (t) \\
	\implies c_+(t) &= \exp\left(-\frac{\mathrm{i} \omega}{2} t \right) c_+(0) \\
	c_-(t) &= \exp\left(\frac{\mathrm{i} \omega}{2} t \right) c_-(0) 
\end{align*}

Wir erhalten somit für die Lösung:
%
\begin{align*}
	\implies \ket{\psi(t)} &= \exp\left(-\frac{\mathrm{i} \omega}{2} t \right) c_+(0)  \ket{z+} +  \exp\left(\frac{\mathrm{i} \omega}{2} t \right) c_-(0)  \ket{z-}
\end{align*}

Somit folgt für die Wahrscheinlichkeiten:
%
\begin{align*}
	\prob[\sigma_z \hateq +1 | \ket{\psi(t)}] &= |\Braket{z+|\psi(t)}|^2 = |c_+(0)|^2
\end{align*}
%
Diese Wahrscheinlichkeit ist also unabhängig von der Zeit, aber:
%
\begin{align*}
	& \prob[\sigma_x \hateq 1 | \ket{\psi(t)}] = |\Braket{x+|\psi(t)}|^2 \\
	={}& \left| \exp\left(-\frac{\mathrm{i} \omega}{2} t \right) c_+(0) \Braket{x+|z+} + \exp\left(\frac{\mathrm{i} \omega}{2} t \right) c_-(0) \Braket{x+|z-} \right|^2 \\
	={}& \left| \exp\left(-\frac{\mathrm{i} \omega}{2} t \right) c_+(0) \frac{1}{\sqrt{2}} + \exp\left(\frac{\mathrm{i} \omega}{2} t \right) c_-(0) \frac{1}{\sqrt{2}} \right|^2 \\
	={}& \frac{1}{2} \left| \exp\left(-\frac{\mathrm{i} \omega}{2} t \right) c_+(0) + \exp\left(\frac{\mathrm{i} \omega}{2} t \right) c_-(0) \right|^2
\end{align*}
%
für $\ket{\psi(0)} = \ket{x+}$ folgt:
%
\begin{align*}
	c_+(0) &= \frac{1}{\sqrt{2}}  \\
	c_-(0) &= \frac{1}{\sqrt{2}}
\end{align*}
%
Für die Wahrscheinlichkeit gilt dann:
%
\begin{align*}
	\prob[\sigma_x \hateq 1 | \ket{\psi(t)}] &= \frac{1}{4} \left( 2 \cos\left( \frac{\omega}{2} t \right) \right)^2 = \cos^2\left( \frac{\omega}{2} t \right)
\end{align*}
%
Für eine Illustration siehe Abbildung~\ref{fig:2013-10-18-2}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\node[below left] at (0,0) {$0$};
		\draw[->] (-0.2,0) -- (3.5,0) node[below] {$t$};
		\draw[->] (0,-0.2) -- (0,1);
		\draw[DarkOrange3] plot[samples=100,domain=0:3.5] (\x,{0.5*cos(deg(pi/2*\x))^2});
		\draw (-0.1,0.5) node[left] {$1$} -- (0.1,0.5);
		\draw[dotted] (2,-0.1) node[below] {$2\pi/\omega$} -- (2,1);
	\end{tikzpicture}
	\caption{Der Verlauf der Wahrscheinlichkeit während der Präzession.}
	\label{fig:2013-10-18-2}
\end{figure}

