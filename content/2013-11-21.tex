% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 21.11.2013

\renewcommand{\printfile}{2013-11-21}

\begin{notice}[Erinnerung:]
	\[ V(t) = \left( V \mathrm{e}^{\mathrm{i} \omega t} + V^\dag \mathrm{e}^{-\mathrm{i} \omega t} \right) \]
	Übergangsrate in erster Ordnung
	\begin{align*}
		\Gamma_{a \to b}^{(1)} 
		&= \frac{2 \pi}{\hbar} \left[
			|\braket{b|V|a}|^2 \delta(\hbar \omega_0 + (E_b - E_a))
			+ |\braket{b|V^\dag|a}|^2 \delta(\hbar \omega_0 - (E_b - E_a))
		\right]
	\end{align*}
	Das Problem ist jedoch, dass das Matrixelement $\braket{b|V|a}$ oftmals null wird, z.\,B.\ durch Symmetrien oder wegen Distanz.
\end{notice}

\subsection{Übergänge zweiter Ordnung}

Die Übergangsamplitude zweiter Ordnung ist
%
\begin{align*}
	A_{a \to b}^{(2)}(t) =
	\begin{multlined}[t]
		- \frac{1}{\hbar^2} \sum_c \braket{b|A|c}\braket{c|V|a}
		\int_{-\infty}^{t} \diff t_1 \int_{-\infty}^{t_1} \diff t_2 \exp\biggl\{
			- \frac{\mathrm{i}}{\hbar} t E_b - \frac{\mathrm{i}}{\hbar} t_1 (E_c - E_b) \\
			- \frac{\mathrm{i}}{\hbar} t_2 (E_a - E_c)
		\biggr\}
		\exp\left\{ \eta (t_1 + t_2) \right\}
	\end{multlined}
\end{align*}

Für die Übergangswahrscheinlichkeit gilt
%
\begin{align*}
	W_{a \to b}^{(2)}(t)
	&= \left| A_{a \to b}^{(2)}(t) \right|^2 \\
	&= \frac{1}{\hbar^2} \left| \sum_c \frac{\braket{b|A|c}\braket{c|V|a}}{\mathrm{i} \omega_{ca} + \eta} \right|^2 \frac{\mathrm{e}^{4 \eta t}}{4 \eta^2 + \omega_{ba}^2}
\end{align*}

Um die Übergangsrate zu erhalten differenzieren wir nach der Zeit und lassen zusätzlich $\eta \to 0$ laufen:
%
\begin{align*}
	\Gamma_{a \to b}^{(2)} &= \frac{2 \pi}{\hbar^2} \lim\limits_{\eta \to 0} \left| \frac{\braket{b|A|c}\braket{c|V|a}}{(E_a - E_c) + \mathrm{i} \eta \hbar} \right|^2 \delta(E_b - E_a)
\end{align*}

\subsection{Floquet-Theorie}
\index{Floquet!Theorie}

Betrachte ein System mit einem zeitabhängigen Hamiltonoperator $H = H(t)$ und mit einer periodischen Zeitabhängigkeit
\[ H(t+T) = H(t) \]
Es gibt einen Zeitentwicklungsoperator $U(t)$
\[ \mathrm{i} \hbar \partial_t U(t) = H(t) \, U(t) \; , \quad U(t=0) = \mathds{1} \]

\subsubsection{Floquet-Theorem}
\index{Floquet!Theorem}

\begin{enumerate}
	\item $U(t) = Q(t) \mathrm{e}^{\frac{\mathrm{i}}{\hbar} B t}$, $Q(t+T) = Q(t)$, $Q(t) Q^\dag(t) = \mathds{1}$, $B = B^\dag$

	\item Für alle $t$ existiert eine Orthonormalbasis $\mathcal{B} = \{ \ket{u_n(t)} \}$ mit $\ket{u_n(t+T} = \ket{u_n(t)}$
		\[ \Bigl( \mathrm{i}\hbar\partial_t - H(t) \Bigr) \ket{u_n(t)} = \varepsilon_n \ket{u_n(t)} \; , \quad \text{(verallgemeinerte SG)} \]
		Die $\ket{u_n(t)}$ sind die Floquet-Eigenzustände, die $\varepsilon_n \in \mathbb{R}$ die Floquet-Eigenwerte.

	\item Die $\varepsilon_n$ können so gewählt werden, dass
		\[ -\frac{\hbar \omega}{2} \leq \varepsilon_n \leq \frac{\hbar\omega}{2} \; , \quad \left( \omega = \frac{2\pi}{T} \right) \]
\end{enumerate}

\begin{proof}
	\begin{enumerate}
		\item Definiere: $V(t) \equiv U(t+T) U^\dag(T)$
			%
			\begin{align*}
				\mathrm{i} \hbar \partial_t V(t)
				&= H(t+T) U(t+T) U^\dag(T) \\
				&= H(t) V(t) \; , \quad V(t=0) = \mathds{1}
			\end{align*}
			%
			$V(t)$ und $U(t)$ lösen also dasselbe Anfangswertproblem. Folglich ist $V(t) = U(t)$.
			%
			\begin{align*}
				U(T) \equiv \mathrm{e}^{\frac{\mathrm{i}}{\hbar} B T} \; , \quad (B = B^\dag)
			\end{align*}
			%
			Definiere: $Q(t) \equiv U(t) \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} B t}$
			%
			\begin{align*}
				Q(t+T)
				&= U(t+T) \underbrace{\mathrm{e}^{-\frac{\mathrm{i}}{\hbar} B t} \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} B t}}_{U^\dag(T)} \\
				&= V(t) \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} B t} \\
				&= U(t) \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} B t} = Q(t) \\
				\implies U(t) &= Q(t) \mathrm{e}^{\frac{\mathrm{i}}{\hbar} B t} 
			\end{align*}

		\item Sei $\{ \ket{b_n} \}$ eine orthonormale Eigenbasis von $B$
			%
			\begin{align*}
				\ket{u_n(t)}
				&\equiv Q(t) \ket{b_n} = U(t) \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} B t} \ket{b_n} \\
				&= \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} \varepsilon_n t} U(t) \ket{b_n} \\
				\mathrm{i} \hbar \partial_t \ket{u_n(t)} - H(t) \ket{u_n(t)}
				&= \varepsilon_n \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} \varepsilon_n t} U(t) \ket{b_n} + H(t) \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} \varepsilon_n t} U(t) \ket{b_n} \\
				-H(t) \ket{u_n(t)} &= \varepsilon_n \ket{u_n(t)}
			\end{align*}

		\item Definiere $\ket{u_n'(t)} \equiv \mathrm{e}^{-\mathrm{i} \omega m t} \ket{u_n(t)} = \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} (\varepsilon_n + \hbar \omega m t} U(t) \ket{b_n}$.

			$\ket{u_n'(t)}$ ist Floquet-Eigenzustand zum Floquet-Eigenwert $\varepsilon_n + \hbar \omega m$.
	\end{enumerate}
\end{proof}

\begin{notice}
	Floquet-Eigenwerte sind nur bis auf ganzzahlige Vielfache von $\hbar \omega$ definiert.
\end{notice}

\minisec{Wichtige Anwendung: Berechnung von Übergangsamplituden}

\begin{align*}
	A_{\ket{\psi} \to \ket{\phi}} &= \braket{\phi|\psi(t)} \\
	\ket{\psi(t)}
	&= U(t) \ket{\psi} = Q(t) \mathrm{e}^{\frac{\mathrm{i}}{\hbar} B t} \ket{\psi} \\
	&= \sum_n \mathrm{e}^{\frac{\mathrm{i}}{\hbar} \varepsilon_n t} \underbrace{Q(t) \ket{b_n}}_{\ket{u_n(t)}} \underbrace{\bra{b_n}}_{\bra{u_n(t=0)}} \psi\rangle \\
	&= \sum_n \mathrm{e}^{\frac{\mathrm{i}}{\hbar} \varepsilon_n t} \ket{u_n(t)}\braket{u_n(t=0)|\psi} \\
	A_{\ket{\psi} \to \ket{\phi}} &= \sum_n \mathrm{e}^{\frac{\mathrm{i}}{\hbar} \varepsilon_n t} \braket{\phi|u_n(t)}\braket{u_n(t=0)|\psi}
\end{align*}
%
Sind die Floquet-Eigenzustände und Floquet-Eigenwerte bekannt, kann die Zeitentwicklung jedes Anfangszustands berechnet werden.

\subsubsection{Erweiterter Hilbertraum und Berechnung der Floquet-Eigenzustände und Floquet-Eigenwerte}
\index{Floquet!Eigenzustände}
\index{Floquet!Eigenwerte}

Wir haben einen System-Hilbertraum $\mathcal{H}$ und einen Hilbertraum der $T$-periodischen, komplexwertigen Funktionen $\mathcal{H}_T$.
%
\begin{itemize}
	\item Elemente: $f(t) = f(t+T) \in \mathbb{C}$
	\item Skalarprodukt: $\displaystyle \langle f,g \rangle \equiv \frac{1}{T} \int_0^T \diff t f^*(t) g(t)$
\end{itemize}

\newcommand\fket[1]{{\ket{#1}\!\rangle}}
\newcommand\fKet[1]{{\left.\Ket{#1}\!\right\rangle}}
\newcommand\fbra[1]{{\langle\!\bra{#1}}}
\newcommand\fBra[1]{{\left\langle\!\Bra{#1}\right.}}
\newcommand\fbraket[1]{{\langle\!\braket{#1}\!\rangle}}
\newcommand\fBraket[1]{{\left\langle\!\Braket{#1}\!\right\rangle}}
Erweitern wir nun diesen Hilbertraum auf
\[ \mathcal{H}_F \equiv \mathcal{H} \otimes \mathcal{H}_T \]
%
\begin{itemize}
	\item Elemente: $\fket{\psi} = \ket{\psi(t)}$ mit $\ket{\psi(t+T)} = \ket{\psi(t)}$
	\item Skalarprodukt: $\displaystyle \fbraket{\phi|\psi} = \frac{1}{T} \int_{0}^{T} \diff t \braket{\phi(t)|\psi(t)}$
\end{itemize}

Eine wichtige Konsequenz ist, dass $\Bigl(\mathrm{i} \hbar \partial_t - H(t)\Bigr)$ ein hermitescher Operator auf $\mathcal{H}_F$ ist. Die verallgemeinerte Schrödingergleichung lautet
%
\begin{align*}
	(\mathrm{i} \hbar \partial_t - H(t)) \fket{u_n} = \varepsilon \fket{u_n}
\end{align*}
%
und reduziert sich auf ein gewöhnliches Eigenwertproblem.

\begin{example}
	Die \acct{Rabi-Oszillationen}.
	\begin{align*}
		H(t)
		&= - \omega S_z - \Omega (\cos\omega t\ S_x + \sin\omega t\ S_y) \\
		&= - \omega S_z - \Omega \left( S_- \mathrm{e}^{\mathrm{i}\omega t} + S_+ \mathrm{e}^{-\mathrm{i}\omega t} \right)
	\end{align*}
	%
	mit $\omega = \gamma B_z$ und $\Omega = \gamma B_1$
	%
	\begin{align*}
		S_- &= \frac{1}{2} (S_x - \mathrm{i} S_y) \\
		S_+ &= \frac{1}{2} (S_x + \mathrm{i} S_y)
	\end{align*}

	Wir wählen eine Basis im Hilbertraum $\mathcal{H}$: $\mathcal{B} = \{ \ket{z+}, \ket{z-} \}$.
	%
	\begin{align*}
		S_{\pm} \ket{z\mp} &= \frac{\hbar}{2} \ket{z\pm} \\
		S_{\pm} \ket{z\pm} &= 0
	\end{align*}
	%
	Außerdem eine Basis in $\mathcal{H_T}$: $\mathcal{B}_T = \{ \mathrm{e}^{\mathrm{i} \omega n t} \}_{n \in \mathbb{Z}}$. Daraus ergibt sich die Basis in $\mathcal{H}_F$:
	%
	\begin{align*}
		\mathcal{B}_F = \left\{ \fket{n,\alpha} \equiv \mathrm{e}^{\mathrm{i} \omega n t} \ket{z\alpha} \right\}_{n \in \mathbb{Z}}^{\alpha = \pm}
	\end{align*}

	Entwickeln wir nun einen allgemeinen Zustand in dieser Basis.
	%
	\begin{align}
		\fket{\psi} = \sum_{\substack{n \in \mathbb{Z} \\ \alpha=\pm}} c_{n,\alpha} \fket{n,\alpha} \tag{*}\label{eq:2013-11-21-stern1}
	\end{align}
	%
	Betrachten wir als Nächstes, wie der Hamiltonoperator auf diese Zustände $\fket{n,\alpha}$ wirkt
	%
	\begin{align*}
		S_z \fket{n,\alpha} &= \alpha \frac{\hbar}{2} \fket{n,\alpha} \\
		\mathrm{i} \hbar \partial_t \fket{n,\alpha} &= - \hbar \omega n \fket{n,\alpha} \\
		S_+ \mathrm{e}^{-\mathrm{i} \omega t} \fket{n,-} &= \mathrm{e}^{-\mathrm{i} (n-1) \omega t} \ket{z+} = \fket{n-1,+} \\
		S_- \mathrm{e}^{\mathrm{i} \omega t} \fket{n,+} &= \fket{n+1,-} \\
		S_+ \mathrm{e}^{-\mathrm{i} \omega t} \fket{n,+} &= 0 \\
		S_- \mathrm{e}^{\mathrm{i} \omega t} \fket{n,-} &= 0
	\end{align*}
	%
	Mit \eqref{eq:2013-11-21-stern1} in die verallgemeinerte Schrödingergleichung eingesetzt ergibt sich
	%
	\begin{align*}
		\left( -m \hbar \omega - \frac{\hbar\omega}{2} \right) c_{m,+} - \frac{\hbar\Omega}{2} c_{m+1,-} &= \varepsilon c_{m,+} \\
		\left( -m \hbar \omega + \frac{\hbar\omega}{2} \right) c_{m,-} - \frac{\hbar\Omega}{2} c_{m-1,+} &= \varepsilon c_{m,-}
	\end{align*}
	%
	Dies ist die Darstellung der verallgemeinerten Schrödingergleichung in der Basis $\mathcal{B}$.

	Ansatz: Alle Koeffizienten seien $0$ bis auf $c_{-1,+}$ nach $c_{0,-}$
	%
	\begin{align*}
		m=0: && \frac{\hbar\omega}{2} c_{0,-} - \frac{\hbar\Omega}{2} c_{-1,+} &= \varepsilon c_{0,-} \\
		m=-1: && \frac{\hbar\omega}{2} c_{-1,+} - \frac{\hbar\Omega}{2} c_{0,-} &= \varepsilon c_{-1,+}
	\end{align*}
	%
	Alle anderen Gleichungen reduzieren sich auf $0 = 0$. Also
	%
	\begin{align*}
		\frac{\hbar}{2}
		\begin{pmatrix}
			\omega & -\Omega \\
			-\Omega & \omega \\
		\end{pmatrix}
		\begin{pmatrix}
			c_{0,-} \\
			c_{-1,+} \\
		\end{pmatrix}
		&=
		\varepsilon
		\begin{pmatrix}
			c_{0,-} \\
			c_{-1,+} \\
		\end{pmatrix}
	\end{align*}
	%
	Wir erhalten die Floquet-Eigenwerte
	%
	\begin{align*}
		\varepsilon_{\pm} = \frac{\hbar}{2} (\omega \pm \Omega)
	\end{align*}
	%
	und die Floquet-Eigenzustände
	%
	\begin{align*}
		\fket{u\pm}
		&= \frac{1}{\sqrt{2}} \left( \fket{0,-} \mp \fket{-1,+} \right) \\
		&= \frac{1}{\sqrt{2}} \left( \ket{z-} \mp \mathrm{e}^{-\mathrm{i} \omega t} \ket{z+} \right)
	\end{align*}
\end{example}
