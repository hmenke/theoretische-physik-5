% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 17.01.2014

\renewcommand{\printfile}{2014-01-17}

\section{Äquivalenz zur Schrödingergleichung}

Wir leiten die Schrödingergleichung aus dem Feynman-Propagator $K^F$ her und klären die Mittenregel.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$t$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {$\bm{x}$};
		\draw (1,-0.1) node[below] {$t_0$} -- (1,1.5);
		\draw (1-0.1,0.5) node[left] {$\bm{x}'$} -- (1,0.5) coordinate (x1) -- (1+0.1,0.5);
		\draw (2,-0.1) node[below] {$t_0 + \varepsilon$} -- (2,2);
		\draw (2-0.1,1.5) -- (2,1.5) coordinate (x2) -- (2+0.1,1.5) node[right] {$\bm{x}'$};
		\draw[->] (x2) --
			node[pos=0.2,dot,DarkOrange3,label={[DarkOrange3]below:$p$}] {}
			node[pos=0.8,dot,MidnightBlue,label={[MidnightBlue]below right:$r$}] {}
			node[above left] {$\bm{\xi}$} (x1);
		\node[right] at (4,1) {$\aligned p &\in [0,1] \\ r &\in [0,1] \endaligned$};
	\end{tikzpicture}
	\caption{Propagation von einem Punkt zum nächsten in der Raumzeit.}
	\label{fig:2014-01-17-1}
\end{figure}

\begin{align*}
	K(\bm{x},t_0+\varepsilon,\bm{x}',t_0)
	&= \sqrt{\frac{m}{2 \pi \mathrm{i} \hbar \varepsilon}} \exp\left\{
		\frac{\mathrm{i}}{\hbar} \frac{m}{2 \varepsilon} \sum_{\alpha = 1}^{3} \xi_\alpha^2 - \frac{\mathrm{i}}{\hbar} \frac{q}{c} \sum_{\alpha = 1}^{3} \xi_\alpha A_\alpha(\bm{x} + p \bm{\xi}) - \frac{\mathrm{i}}{\hbar} \varepsilon V(\bm{x} + r \bm{\xi})
	\right\} \\
	\braket{\bm{x} | \psi(t_0 + \varepsilon)}
	&= \int K(\bm{x},t_0+\varepsilon,\bm{x}',t_0) \braket{\bm{x}'|\psi(t_0)} \diff \bm{x}'
\end{align*}
%
für kleine $\varepsilon$ kann in $\bm{\xi} = \bm{x}' - \bm{x}$ entwickelt werden. Dazu muss abgeschätzt werden, wie $\bm{\xi}$ mit $\varepsilon$ skaliert:
%
\begin{align*}
	\int_{-\infty}^{\infty} \diff \xi_\alpha \sqrt{\frac{m}{2 \pi \mathrm{i} \hbar \varepsilon}} \exp\left\{ - \frac{\mathrm{i}}{\hbar} \frac{m}{2 \varepsilon} \xi_\alpha^2 \right\}
	\cdot
	\begin{cases}
		1 \\
		\xi_\alpha^{2k+1} \\
		\xi_\alpha^{2} \\
		\xi_\alpha^{2k} \\
	\end{cases}
	&=
	\begin{cases}
		1 \\
		0 \\
		\mathrm{i} \frac{\varepsilon \hbar}{m} \\
		\mathcal{O}(\varepsilon^k) \\
	\end{cases}
\end{align*}

Wir entwickeln den Propagator in der Ordnung $\varepsilon$:
%
\begin{align}
	\exp&\left\{ - \frac{\mathrm{i}}{\hbar} \frac{q}{c} \xi_\alpha A_\alpha(\bm{x} + p \bm{\xi}) \right\} \notag \\
	&\approx 1 - \frac{\mathrm{i}}{\hbar} \frac{q}{c} \xi_\alpha \underbrace{A_\alpha(bm{x} + p \bm{\xi})}_{A_\alpha(\bm{x}) + p \xi_\beta \partial_\beta A_\alpha} - \frac{q^2}{\hbar^2 c^2} \xi_\alpha \xi_\beta A_\alpha(\bm{x} + p \bm{\xi}) A_\beta(\bm{x} + p \bm{\xi}) + \mathcal{O}(\xi^3) \notag \\
	&\approx 1 - \frac{\mathrm{i}}{\hbar} \frac{q}{c} \xi_\alpha A_\alpha(\bm{x}) - \frac{\mathrm{i}}{\hbar} \frac{q}{c} \xi_\alpha \xi_\beta \partial_\beta A_\alpha(\bm{x}) - \frac{q^2}{\hbar^2 c^2} \xi_\alpha \xi_\beta A_\alpha(\bm{x}) A_\beta(\bm{x}) + \mathcal{O}(\xi^3) \tag{*} \label{eq:2014-01-17-stern1} \\
	%
	\exp&\left\{ - \frac{\mathrm{i}}{\hbar} \varepsilon V(\bm{x} + r \bm{\xi}) \right\} \notag \\
	&\approx 1 - \frac{\mathrm{i}}{\hbar} \varepsilon V(\bm{x} + r \bm{\xi}) + \mathcal{O}(\varepsilon^3) \notag \\
	&\approx 1 - \frac{\mathrm{i}}{\hbar} \varepsilon V(\bm{x}) \tag{**} \label{eq:2014-01-17-stern2}
\end{align}

Mit dem genäherten Propagator wird nun $\psi(\bm{x},t_0)$ propagiert:
%
\begin{align*}
	&\int \diff \bm{x}'\ K(\bm{x},t_0+\varepsilon,\bm{x}',t_0)\ \psi(\bm{x}',t_0) \\
	={}& \begin{multlined}[t]
		\prod_{\alpha = 1}^{3} \sqrt{\frac{m}{2 \pi \mathrm{i} \hbar \varepsilon}}
		\int_{-\infty}^{\infty} \diff \xi_\alpha \exp\left\{
			\frac{\mathrm{i}}{\hbar} \frac{m}{2 \varepsilon} \sum_\beta \xi_\beta^2 + \eqref{eq:2014-01-17-stern1} + \eqref{eq:2014-01-17-stern2}
		\right\}
		\Bigg[
			\psi(\bm{x},t_0) + \xi_\alpha \partial_\alpha \psi(\bm{x},t_0) \\
			+ \frac{1}{2} \xi_\alpha \xi_\beta \partial_\alpha \partial_\beta \psi(\bm{x},t_0)
		\Bigg]
	\end{multlined}\\
	={}& \begin{multlined}[t]
		\psi(\bm{x},t_0) + \frac{1}{2} \frac{\mathrm{i} \varepsilon \hbar}{m} \nabla^2 \psi(\bm{x},t_0) - \frac{\mathrm{i}}{\hbar} \varepsilon V(\bm{x}) \psi(\bm{x},t_0) - \frac{\mathrm{i}}{\hbar} \frac{q}{c} \bm{A}\ (\nabla \psi(\bm{x},t_0)) \frac{\mathrm{i} \varepsilon \hbar}{m} \\
		- \frac{\mathrm{i}}{\hbar} \frac{q}{c} p (\nabla \cdot \bm{A}) \psi(\bm{x},t_0) \frac{\mathrm{i} \varepsilon \hbar}{m} - \frac{1}{2 \hbar^2} \frac{q^2}{c^2} \bm{A}^2 \psi(\bm{x},t_0) \frac{\mathrm{i} \varepsilon \hbar}{m} + \mathcal{O}(\varepsilon^3)
	\end{multlined}\\
	={}& \psi(\bm{x},t_0+\varepsilon)
\intertext{nach dem Satz von Taylor gilt aber auch}
	={}& \psi(\bm{x},t_0) + \varepsilon \partial_t \psi(\bm{x},t_0) + \mathcal{O}(\varepsilon^2)
\end{align*}
Also gilt
\begin{align*}
	\partial_t \psi(\bm{x},t_0)
	&= - \frac{\mathrm{i}}{\hbar} \left( - \frac{\hbar^2}{2 m} \nabla^2 + V(\bm{x}) + \frac{q}{c} p (\nabla \cdot \bm{A}) \frac{\mathrm{i} \hbar}{m} + \frac{1}{2} \frac{q^2}{m c^2} \bm{A}^2 + \frac{q \mathrm{i} \hbar}{c m} \bm{A} \cdot \nabla \right) \psi(\bm{x},t_0) \\
	&= - \frac{\mathrm{i}}{\hbar} \left[ \frac{1}{2m} \left( \bm{p} - \frac{q}{c} \bm{A}(\bm{x}) \right)^2 + V(\bm{x}) \right] \psi(\bm{x},t_0) \; , \quad \text{nur für } p = \frac{1}{2}
\end{align*}
%
Die Feynmansche Annahme, über alle Pfade zu summieren, liefert also dieselbe Dynamik wie die Schrödingergleichung.

Für $p = 1/2$ haben wir damit also die Schrödingergleichung hergeleitet, womit die Äquivalenz gezeigt wurde.

\section{Spezialfall: Quadratische Lagrangefunktion}

Die allgemeinste quadratische Lagrangefunktion in einer Dimension lautet
%
\begin{align*}
	L(x,\dot{x},t) &= \frac{1}{2} m(t) \dot{x}^2 + b(t) \dot{x} x - \frac{m(t)}{2} \omega^2(t) x^2 + \alpha(t) \dot{x} + f(t) x + g(t)
\end{align*}
%
Nun parametrisieren wird den Pfad durch $x(t) = x_\textnormal{cl}(t) + y(t)$.
%
\begin{align*}
	S[x(t)]
	&= S[x_\textnormal{cl}(t) + y(t)] \\
	&= S[x_\textnormal{cl}(t)] + \int_{t_a}^{t_b} \diff t \left( \frac{m(t)}{2} \dot{y}^2 + b(t) \dot{y} y - \frac{m(t)}{2} \omega^2(t) y^2 \right) \; , \quad \text{da } \left.\frac{\delta S}{\delta x}\right|_{x_\textnormal{cl}} = 0
\end{align*}
%
Die Integration über alle $x(t)$ kann durch eine Integration über alle $y(t)$ ersetzt werden.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$t$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {${x}$};
		\draw (0.1,0.5) -- (-0.1,0.5) node[left] {$x_a$};
		\draw (0.1,1.5) -- (-0.1,1.5) node[left] {$x_b$};
		\draw (0.5,0.1) -- (0.5,-0.1) node[below] {$t_a$};
		\draw (2.5,0.1) -- (2.5,-0.1) node[below] {$t_b$};
		\foreach \n in {1,2,...,10} {
			\path (0.5,0.5) .. controls (1,2) and (2,0) .. coordinate[pos=\n/11] (n \n) (2.5,1.5);
			\draw (n \n) -- +(0,rand);
		}
		\draw[MidnightBlue] (0.5,0.5) node[dot] {} .. controls (1,2) and (2,0) .. (2.5,1.5) node[dot] {};
	\end{tikzpicture}
	\caption{Abweichungen $y(t)$ von einem Pfad $x(t)$.}
	\label{fig:2014-01-17-2}
\end{figure}

\begin{notice}
	\[ \int_{-\infty}^{\infty} \diff x\ f(x) \stackrel{y = x - \frac{1}{x}}{=} \int_{-\infty}^{\infty} \diff y\ f(y + \bar{x}) \]
\end{notice}

\begin{align*}
	K(x_b,t_b,x_a.t_a)
	&= \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} S[x_a(t)]} \underbrace{\int \mathcal{D}\{ y(t) \} \mathrm{e}^{- \int_0^\infty \diff t \dotsc}}_{F(t_b,t_a)}
\end{align*}
%
wobei $F(t_b,t_a)$ unabhängig von $x_a,x_b$ ist.

