% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 06.02.2014

\renewcommand{\printfile}{2014-02-06}

\section{Lindblad-Formalismus}
\index{Lindblad-Formalismus}

Ein gutes Lehrbuch zu diesem Thema ist \fullcite{breuer2002theory}.

\subsection{Quantenhalbgruppe}

Ein System $S$, das an die Umgebung $E$ gekoppelt ist, lässt sich beschreiben durch den Hamiltonoperator
\[ H = H_S + H_E + H_{SE} \]
mit der Dichtematrix
%
\begin{align*}
	\varrho(t)
	&= \ket{\psi(t)}\bra{\psi(t)} \\
	&= U(t,t_0) \varrho(t_0) U^{-1}(t,t_0)
\end{align*}
%
und dem faktorisierten Anfangszustand
\[ \varrho(t_0) = \varrho_S(t) \otimes \varrho_E(t_0) \]
Durch Ausspuren der Umgebung $E$
%
\begin{align*}
	\varrho_S(t)
	&= \tr_E \varrho(t) \\
	&= \tr_E \left[ U(t,t_0) \varrho_S(t) \otimes \varrho_E(t_0) U^{-1}(t,t_0) \right]
\end{align*}
%
\begin{enumerate}
	\item Für festes $t$ und festes $\varrho_E(t_0)$ ist das eine Abbildung
		\[ V(t) : \varrho_S(t_0) \to \varrho_S(t) \]
		im Raum $\mathcal{S}(S)$ der Dichtematrizen des Systems.
		\begin{center}
			\begin{tikzcd}[every path/.style={}]
				\varrho(0) = \varrho_S(0) \otimes \varrho_E(0) \arrow{r}{\text{unitär}} \arrow{d}{\tr_E} & \varrho(t) \arrow{d}{\tr_E} \\
				\varrho_S(0) \arrow{r}{V(t)} & \varrho_S(t)
			\end{tikzcd}
		\end{center}

	\item Für beliebiges $t$: Einparameterschar von Abbildungen $V(t)$.
	\item Falls die Umgebung schnell relaxiert ($\varrho_E(t) \simeq \varrho_E(0)$), dann gilt die Markov-Eigenschaft.
		\begin{align*}
			V(t_2) V(t_1) = V(t_2 + t_1) \; , \quad \text{(Halbgruppe)}
		\end{align*}
\end{enumerate}

Mit dem \acct{Superoperator} $\mathcal{L}$
\begin{align*}
	V(t) &= \mathrm{e}^{\mathcal{L} t} \\
	\varrho_S(t) &= \mathrm{e}^{\mathcal{L} t} \varrho_S(0) \\
	\partial_t \varrho_S(t) = \mathcal{L} \varrho_S(t)
\end{align*}

\subsection{Liouville-Raum}

Gegeben ist ein Hilbertraum $\mathcal{H}$ mit $\dim \mathcal{H} = n$. Der \acct{Liouville-Raum} $\mathscr{LR}$ ist die Menge aller (Hilbert-Schmidt) Operatoren auf $\mathcal{H}$, für die $\tr A^\dag A$ endlich ist. Dabei ist $\mathscr{LR}$ selbst ein Hilbertraum. Es existiert eine orthonormierte Basis $\{ F_i \}$ mit
\[ (F_i,F_j) = \tr F_i^\dag F_j = \delta_{ij} \]
mit
%
\begin{align*}
	F_{N^2} &= \frac{1}{N^2} \mathds{1} \\
	\leadsto \tr F_i &= 0 \; , \quad \text{für } i = 1,\dotsc,N^2-1
\end{align*}

Jeder Operator auf $\mathscr{LR}$ kann entwickelt werden
\[ W = \sum_{i=1}^{N^2} (F_i,W) F_i \]

\subsection{Lindblad-Gleichung}

\begin{align*}
	\varrho_E^{(0)}
	&= \sum_{\alpha=1}^{\dim\mathcal{H}} \lambda_\alpha \ket{E_\alpha}\bra{E_\alpha} \\
	\varrho_S(t)
	&= \tr_E U(t,0) \varrho_S^{(0)} \otimes \sum_\alpha \lambda_\alpha \ket{E_\alpha}\bra{E_\alpha} U^{-1}(t,0) \\
	&= \sum_\beta \left\langle E_\beta \middle| U(t,0) \varrho_S^{(0)} \otimes \sum_\alpha \lambda_\alpha \ket{E_\alpha}\bra{E_\alpha} U^{-1}(t,0) \middle| E_\beta \right\rangle \\
	&= \sum_{\alpha,\beta} W_{\alpha\beta}(t) \varrho_S(0) W_{\alpha\beta}^\dag(t)
\intertext{mit $\displaystyle W_{\alpha\beta}(t) \equiv \sqrt{\lambda_\alpha} \braket{E_\beta|U(t,0)|E_\alpha} = \sum_{i=1}^{N^2} \Bigl(F_i,W_{\alpha\beta}(t)\Bigr) F_i$}
	&= \sum_{\alpha,\beta} \sum_i \Bigl(F_i,W_{\alpha\beta}\Bigr) F_i \varrho_S^{(0)} \sum_j \Bigl(F_j,W_{\alpha\beta}\Bigr)^* F_j^\dag \\
	&= \sum_{ij} c_{ij}(t) F_i \varrho_S^{(0)} F_j^\dag \\
	\mathcal{L} \varrho_S(t)
	&= \lim_{\varepsilon \to 0} \frac{V(\varepsilon) \varrho_S - \varrho_S}{\varepsilon} \\
	&= \lim_{\varepsilon \to 0} \Biggl\{
		\frac{c_{N^2,N^2}(\varepsilon) - N}{N \varepsilon} \varrho_S
		+ \frac{1}{\sqrt{N}} \left( \sum_{i=1}^{N^2 - 1} \frac{c_{i,N^2}}{\varepsilon} F_i \varrho_S + h.c. \right)
		+ \sum_{ij=1}^{N^2-1} \frac{c_{ij}(\varepsilon)}{\varepsilon} F_i \varrho_S F_j^\dag
	\Biggr\} \\
	\partial_t \varrho_S &= \mathcal{L} \varrho_S = - \mathrm{i} [H,\varrho_S] + [G,\varrho_S]_{+} + \sum_{i,j=1}^{N^2-1} a_{ij} F_i \varrho_S F_j^\dag
\end{align*}
%
mit den Identifikationen
%
\begin{align*}
	a_{i,j} &\equiv \lim_{\varepsilon \to 0} \frac{c_{ij}(\varepsilon)}{\varepsilon} \; , \quad (i,j < N^2) \\
	a_{N^2,N^2} &\equiv \lim_{\varepsilon \to 0} \frac{c_{N^2,N^2}(\varepsilon) - N}{\varepsilon} \\
	a_{i,N^2} &\equiv \lim_{\varepsilon \to 0} \frac{c_{i,N^2}(\varepsilon)}{\varepsilon} \\
	F &\equiv \frac{1}{\sqrt{N}} \sum_{i=1}^{N^2-1} a_{i,N^2} F_i \\
	G &\equiv \frac{1}{2N} a_{N^2,N^2} \mathds{1} + \frac{1}{2}(F^\dag + F) \\
	H &\equiv \frac{1}{2 \mathrm{i}} (F^\dag - F)
\end{align*}

Konsistenzforderung: $\tr_S \varrho_S(t) = 1$, also darf sich die Spur auch nicht ändern
%
\begin{align*}
	\tr_S \partial_t \varrho_S(t) &= 0 = \mathcal{L} \varrho_S \\
	&= \tr\left[ \left( 2 G + \sum_{ij} a_{ij} F_j^\dag F_i \right) \varrho_S \right] \\
	\leadsto G &= - \frac{1}{2} \sum_{ij} a_{ij} F_j^\dag F_i
\end{align*}
%
Dies führt auf die \acct*{1. Standardform der Lindblad-Gleichung}\index{Lindblad-Gleichung!1. Standardform}
\begin{align*}
	\partial_t \varrho_S &= - \mathrm{i} [H,\varrho_S] + \sum_{i,j=1}^{N^2-1} a_{ij} \left( F_i \varrho_S F_j^\dag - \frac{1}{2} [F_j^\dag F_i,\varrho_S]_+ \right)
\end{align*}

Die $a_{ij}$ sind positiv definite Matrizen. Diese lassen sich diagonalisieren
\[ U a U^\dag = \left(
	\begin{smallmatrix}
		\gamma_1 & & \\
		& \ddots & \\
		& & \gamma_{N^2-1} \\
	\end{smallmatrix}\right)
\]
Definiere $L_k$ über \[ F_i = \sum_{k=1}^{N^2-1} U_{ki} L_k. \]

Damit erhalten wir die \acct*{2. Standardform der Lindblad-Gleichung}\index{Lindblad-Gleichung!2. Standardform}
\begin{align*}
	\boxed{
		\partial_t \varrho_S
		= \mathcal{L} \varrho_S = - \mathrm{i} [H,\varrho_S] + \sum_{k=1}^{N^2-1} \gamma_k \left( L_k \varrho_S L_k^\dag - \frac{1}{2} L_k^\dag L_k \varrho_S - \frac{1}{2} \varrho_S L_k^\dag L_k \right)
	}
\end{align*}

\begin{notice}
	\begin{enumerate}
		\item $H \neq H_S$, denn $H_{SE}$ trägt auch bei: \enquote{Lamb-Shift} durch Ankopplung an die Umgebung.
		\item Falls $L_k$ dimensionslos, dann sind die $\gamma_k$ inverse Relaxationszeiten. Diese folgen auch aus einem konkreten mikroskopischen Modell $H = H_S + H_E + H_{SE}$.
	\end{enumerate}
\end{notice}
