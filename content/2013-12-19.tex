% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 19.12.2013

\renewcommand{\printfile}{2013-12-19}

\minisec{Systematischer Aufbau der Zustände}

Die Zustände des Fockraumes können aus dem Vakuum erzeugt werden.
%
\begin{align*}
	\ket{n_1,\dotsc,n_i,\dotsc} &= \prod_{i=1}^{\infty} \frac{1}{\sqrt{n_i!}} (b_i^\dag)^{n_i} \ket{0}
\end{align*}
%
Wichtige Operatoren
%
\begin{itemize}
	\item Besetzungszahloperator $\hat{n}_i = b_i^\dag b_i$ mit \[ \hat{n}_i \ket{n_i} = n_i \ket{n_i} \]
	\item Gesamtzahloperator $\hat{N} \equiv \sum_{i=1}^{\infty} \hat{n}_i$
\end{itemize}

\subsubsection{Fermionenerzeuger und -vernichter}

Definiere den fermionischen Erzeuger
%
\begin{align*}
	c_i^\dag \ket{\dotsc,n_i,\dotsc} &= (1 - n_i) (-1)^{\sum_{j<i} n_j} \ket{\dotsc,n_i+1,\dotsc}
\end{align*}
%
Durch Adjungieren folgt der fermionische Vernichter
%
\begin{align*}
	c_i \ket{\dotsc,n_i,\dotsc} &= n_i (-1)^{\sum_{j<i} n_j} \ket{\dotsc,n_i-1,\dotsc}
\end{align*}
%
insbesondere
%
\begin{align*}
	c_i \ket{0,\dotsc,0,\dotsc,0} = 0
\end{align*}

Betrachte die Vertauschungsrelationen
%
\begin{align*}
	[c_i,c_j]_+ &= c_i c_j + c_j c_i = 0 \\
	[c_i^\dag,c_j^\dag]_+ &= 0 \\
	[c_i,c_j^\dag]_+ &= \delta_{ij}
\end{align*}

\subsubsection{Basiswechsel und Feldoperatoren}

Bisher sind wir von einer festen Ein-Teilchen-Basis $\{ \ket{i} \} = \{ \ket{\phi_i} \}$ ausgegangen. Für eine andere Basis $\{ \ket{j} \} = \{ \ket{\psi_j} \}$ gilt
\[ \ket{\psi_j} = \sum_i \braket{\phi_i|\psi_j}\ket{\phi_i} \]
Dann gibt es Erzeugungs- und Vernichtungsoperatoren
%
\begin{align*}
	d_j^\dag &= \sum_i \braket{\phi_i|\psi_j} a_i^\dag \\
	d_j &= \sum_i \braket{\phi_i|\psi_j}^* a_i
\end{align*}

Ein wichtiger Basiswechsel ist der in die Basis der Ortseigenfunktionen $\{ \ket{j} \} = \{ \ket{\bm{r}} \}$.
%
\begin{align*}
	d_\bm{r}^\dag &\equiv \Psi^\dag(\bm{r}) = \sum_i \braket{\phi_i|\bm{r}} a_i^\dag = \sum_i \phi_i^*(\bm{r}) a_i^\dag
\end{align*}
%
Dieser Operator erzeugt ein Teilchen am Ort $\bm{r}$. Entsprechend vernichtet
%
\begin{align*}
	\Psi(\bm{r}) = d_\bm{r} = \sum_i \phi_i(\bm{r}) a_i
\end{align*}
%
ein Teilchen am Ort $\bm{r}$.

\minisec{Vertauschungsrelationen für Feldoperatoren}

Werten wir aus
%
\begin{align*}
	[\Psi(\bm{r}),\Psi^\dag(\bm{r}')]_{\mp}
	&= \left[ \sum_i \phi_i(\bm{r}) a_i, \sum_j \phi_j^*(\bm{r}') a_j^\dag \right]_{\mp} \\
	&= \sum_{ij} \phi_i(\bm{r}) \phi_j^*(\bm{r}') \underbrace{\left[ a_i, a_j^\dag \right]_{\mp}}_{\delta_{ij}} \\
	&= \sum_i \phi_i(\bm{r}) \phi_i^*(\bm{r}') \\
	&= \delta(\bm{r} - \bm{r}')
\end{align*}

Nun betrachten wir die Impulsdarstellung von Feldoperatoren. Untersuchen wir dazu ebene Wellen im Kasten.
%
\begin{align*}
	\braket{\bm{r}|\bm{k}} &= \frac{1}{\sqrt{V}} \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \\
	\ket{\bm{k}} &= \int_V \diff^3 r\ \braket{\bm{r}|\bm{k}} \ket{\bm{r}} \\
	\Psi^\dag_\bm{k} &= \frac{1}{\sqrt{V}} \int \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \Psi^\dag(\bm{r}) \\
	[\Psi_\bm{k},\Psi_{\bm{k}'}^\dag]_{\mp} &= \dotsb = \delta_{\bm{k},\bm{k}'}
\end{align*}

\subsubsection{Operatoren in Besetzungszahldarstellung}

Wir wollen nun die bekannten Operatoren in Besetzungszahldarstellung finden.

\paragraph{Einteilchenoperatoren}

\begin{example}
	Der Operator der kinetischen Energie.
	%
	\begin{align*}
		\hat{T} = \sum_i \hat{t}_i = \sum_i \frac{\hat{p}_i^2}{2 m}
	\end{align*}
	%
	Potentielle Energie
	%
	\begin{align*}
		\hat{V} = \sum_i \hat{v}_i(\bm{R}_i)
	\end{align*}
	%
	Gesamtspin
	%
	\begin{align*}
		\hat{S} = \sum_i \hat{s}_i
	\end{align*}
\end{example}

Ganz allgemein gilt
%
\begin{align*}
	\hat{W}
	&= \sum_{i=1}^{N} \hat{w}_i
	= \sum_{i=1}^{N} \sum_{k,\ell} \ket{k_i} \underbrace{\braket{k|w|\ell}}_{w_{k\ell}} \bra{\ell_i} \\
	&= \sum_{k,\ell} w_{k\ell} a_k^\dag a_\ell \equiv \bar{w}
\end{align*}

\begin{notice}
	\begin{enumerate}
		\item $\bar{w}$ hat in jeder Basis dieselbe Gestalt.
		\item Wähle Basis so, dass $\bar{w}$ diagonal ist. \[ \bar{w} = \sum_m w_{mm} a_m^\dag a_m = w \]
	\end{enumerate}
\end{notice}

\begin{notice}
	$\displaystyle \sum_i \ket{k_i}\bra{\ell_i} = a_k^\dag a_\ell$
\end{notice}

\paragraph{Vielteilchenoperatoren}

\begin{example}
	\[ \aligned[t]
		V
		&= \sum_{i<j} v(\bm{r}_i,\bm{r}_j) = \frac{1}{2} \sum_{i \neq j} v(\bm{r}_i,\bm{r}_j) \\
		&= \frac{1}{2} \sum_{k,\ell,m,n} \braket{k\ell|v|mn} a_k^\dag a_\ell^\dag a_n a_m
	\endaligned \]
	mit
	\[ \aligned[t]
		\braket{k\ell|v|mn}
		&= \int \diff \bm{r}_1 \diff \bm{r}_2\ \phi_k^*(\bm{r}_1) \phi_\ell^*(\bm{r}_2) v(\bm{r}_1,\bm{r}_2) \phi_m(\bm{r}_1) \phi_n(\bm{r}_2) \\
		&= v_{k\ell,mn}
	\endaligned \]
	Betrachte die Symmetrie von $v_{k\ell,mn}$.
	%
	\begin{align*}
		v_{k\ell,mn} &= v_{\ell k,nm} \\
		v_{k\ell,mn} &= v_{mn,k\ell}^*
	\end{align*}
\end{example}

\begin{proof}
	\begin{align*}
		V
		&= \frac{1}{2} \sum_{i \neq j} v(\bm{r}_i,\bm{r}_j) \\
		&= \frac{1}{2} \sum_{i \neq j} \sum_{k,\ell,m,n} \ket{k_i \ell_j} \underbrace{\braket{k_i \ell_j | v(\bm{r}_1,\bm{r}_2) | m_i n_j}}_{v_{k\ell,mn}} \bra{m_i n_j} \\
		\sum_{i \neq j} \ket{k_i \ell_j} \bra{m_i n_j}
		&= \sum_{i \neq j} (\ket{k_i}\bra{m_i}) (\ket{\ell_j}\bra{n_j}) \\
		&= \sum_{i,j} (\ket{k_i}\bra{m_i}) (\ket{\ell_j}\bra{n_j}) -\sum_{i=1}^{N} \ket{k_i}\underbrace{\braket{m_i|\ell_i}}_{\delta_{m\ell}}\bra{n_i} \\
		&= a_k^\dag a_m a_\ell^\dag a_n - \delta_{m\ell} a_k^\dag a_n \\
		&= a_k^\dag a_m a_\ell^\dag a_n - a_k^\dag a_m a_\ell^\dag a_n \pm a_k^\dag a_\ell^\dag a_m a_n \\
		&= \pm a_k^\dag a_\ell^\dag a_m a_n \\
		&= a_k^\dag a_\ell^\dag a_n a_m
	\end{align*}
\end{proof}
