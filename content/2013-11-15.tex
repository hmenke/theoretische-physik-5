% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 15.11.2013

\renewcommand{\printfile}{2013-11-15}

\begin{example}
	Ein freies Teilchen im Kasten. Der Kasten sei ein Würfel mit Kantenlänge $L$ und periodischen Randbedingungen. Für die Zustände im Kasten gilt
	%
	\begin{align*}
		\Braket{r|n} &= \frac{1}{L^{3/2}} \mathrm{e}^{\mathrm{i}\frac{2 \pi}{L} \bm{n} \cdot \bm{r}} \; , \quad \bm{n} \in \mathbb{Z}^3
	\end{align*}
	%
	Die Zustände sind orthonormiert und genügen der Relation
	%
	\begin{align*}
		\int_{L^3} \diff \bm{r} \Braket{\bm{n}'|\bm{r}}\Braket{\bm{r}|\bm{n}} = \delta_{n,n'}
	\end{align*}
	%
	Die Eins lässt sich zerlegen als
	%
	\begin{align*}
		\mathds{1} &= \sum_{\bm{n} \in \mathbb{Z}^3} \ket{\bm{n}}\bra{\bm{n}}
	\end{align*}
	%
	\paragraph{Übergang in Kontinuum} $L \to \infty$
	%
	\begin{align*}
		\braket{\bm{r}|\bm{k}} &= \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \; , \quad \text{mit } \Braket{\bm{k}|\bm{k}'} = \int \mathrm{e}^{\mathrm{i} (\bm{k}-\bm{k}') \bm{r}} = 2 \pi \delta(\bm{k}-\bm{k}') \\
		\mathds{1} &= \int \frac{\diff \bm{k}}{(2 \pi)^3} \ket{\bm{k}}\bra{\bm{k}} = \int \diff \bm{k} \varrho(\bm{k}) \ket{\bm{k}}\bra{\bm{k}}
	\end{align*}
	%
	Also muss
	\[ \varrho(\bm{k}) = \frac{1}{(2\pi)^3} \]
	Der Einfachheit halber geht man zum Raumwinkel über, also
	\[ \bm{k} = k \bm{e}_\Omega \]
	Damit erhalten wir
	%
	\begin{align*}
		\varrho(\bm{k}) \diff \bm{k}
		&= \underbrace{\varrho(\bm{k}) k^2}_{\equiv \varrho(k)} \diff k \diff \Omega \\
		\varrho(k)
		&= \frac{k^2}{(2\pi)^3}
	\end{align*}
	%
	Umrechnung auf eine Energie liefert
	%
	\begin{align*}
		E &= \frac{\hbar^2}{2 m} k^2 \\
		k &= \frac{(2 m E)^{1/2}}{\hbar}
	\end{align*}
	%
	Damit ergibt sich
	%
	\begin{align*}
		\varrho(E,\Omega)
		&= \varrho(k,\Omega) \frac{\diff k}{\diff E} \\
		&= \frac{k^2}{(2\pi)^3} \frac{m}{\hbar^2 k} = \frac{k m}{(2\pi) \hbar^2} \\
		&= \frac{\sqrt{2 m E}}{(2 \pi \hbar)^3}
	\end{align*}
	%
	Ausgedrückt durch den Impuls lautet die Zustandsdichte
	%
	\begin{align*}
		\varrho(p,\Omega) &= \varrho(k,\Omega) \frac{\diff k}{\diff p} = \frac{p^2}{(2 \pi \hbar)^3}
	\end{align*}
\end{example}

\subsection{Fermis Goldene Regel}

\acct{Fermis Goldene Regel} gilt für Übergänge vom Zustand $\ket{a}$ ins Kontinuum.

\begin{example}
	Auger-Elektron des Heliums.

	\begin{figure}[htpb]
		\centering
		\begin{tikzpicture}
			\draw (0,0) node[left] {(1s)} -- (1,0);
			\draw (0,1) node[left] {(2s)$^2$} (1.5,1) node[left] {$\ket{a}$} -- (2.5,1);
			\draw[MidnightBlue] (3.5,2) -- (4.5,2);
			\node[right] (Em) at (4.5,2) {$E^-$};
			\node[right] (Ep) at (4.5,3) {$E^+$};
			\draw[DarkOrange3,decorate,decoration={brace}] (Ep.west) -- node[right] {$B = \Delta E$} (Em.west);
			\draw[->] (1.9,0.75) -- (1.9,1.25);
			\draw[<-] (2.1,0.75) -- (2.1,1.25);
			\draw (0.5,0) edge[->,MidnightBlue,bend left] (2,1);
			\draw (2,1) edge[->,MidnightBlue,bend left] (3.5,2.5);
			\foreach \i in {2.2,2.4,...,3} {
				\draw (3.6,\i) -- (4.4,\i);
			}
			\node[left] at (3.6,3) {Niveauabstand $\delta E$};
		\end{tikzpicture}
		\caption{Flucht des Auger-Elektrons ins Kontinuum}
		\label{fig:2013-11-15-1}
	\end{figure}

	\begin{align*}
		W_{a \to b}(t)
		&= \sum_{b \in B} W_{a \to b}(t) \\
		&= \sum_{b \in B} \frac{|\Braket{b|V|a}|^2}{\hbar^2} \frac{4 \sin^2 \omega_{ba} t/2}{\omega_{ba}^2} \\
		&\approx \int_{E^-}^{E^+} \diff E\ \varrho(E) \frac{|\Braket{b|V|a}|^2}{\hbar^2} \frac{4 \sin^2 \omega_{ba} t/2}{\omega_{ba}^2} \\
		&\approx |\Braket{b|V|a}|^2 \varrho(E_b)|_{E_b = E_a} \frac{2 \pi}{\hbar} t
	\end{align*}
	%
	Wegen des linearen $t$ Verhaltens können wir eine Übergangsrate berechnen.
	%
	\begin{align*}
		\boxed{
			\Gamma_{a \to b} \equiv \frac{\diff W}{\diff t} = \frac{2 \pi}{\hbar} |\Braket{b|V|a}|^2 \varrho(E_b)|_{E_b = E_a}
		}
	\end{align*}
	%
	Dieser Ausdruck ist \acct{Fermis Goldene Regel}.
\end{example}

\paragraph{Zum Gültigkeitsbereich}
%
Der Niveauabstand $\delta E$ in Abbildung~\ref{fig:2013-11-15-1} geht $\delta E \to 0$ für $L \to \infty$.
Dieser Ausdruck ist somit $\sim t^2$
%
\begin{align*}
	\frac{2 \pi \hbar}{\Delta E} \ll t \ll
	\begin{dcases}
		\frac{2 \pi \hbar}{\delta E} & (\infty \text{ für } L = \infty) \\
		\frac{1}{\Gamma_{a \to b}} \\
	\end{dcases}
\end{align*}

\subsection{Adiabatisches Einschalten}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\begin{scope}[yshift=0cm]
			\node at (0,1) {bisher};
			\draw[->] (0,0) -- (4,0) node[below] {$t$};
			\draw[MidnightBlue] (0,0) -- (2,0) -- (2,1) -- (4,1);
		\end{scope}
		\begin{scope}[yshift=-2cm]
			\node at (0,1) {realistisch};
			\draw[->] (0,0) -- (4,0) node[below] {$t$};
			\draw[MidnightBlue] (0,0) .. controls (2,0) and (2,1) .. (4,1);
		\end{scope}
		\begin{scope}[yshift=-5cm]
			\node at (0,2) {idealisiert};
			\draw[->] (0,0) -- (4,0) node[below] {$t$};
			\draw[->] (2,0) -- (2,2);
			\draw (2.1,1) -- (1.9,1) node[left] {$V_0$};
			\draw (1,0.1) -- (1,-0.1) node[below] {$-\frac{1}{\eta}$};
			\draw (3,0.1) -- (3,-0.1) node[below] {$\frac{1}{\eta}$};
			\draw[DarkOrange3] plot[domain=0:2.5] (\x,{(exp(\x-2))}) node[right] {$V(t) = V_0 \mathrm{e}^{\eta t}$};
		\end{scope}
	\end{tikzpicture}
	\caption{Unterschied zwischen plötzlichem, realistischem und adiabatischem Einschalten der Störung.}
	\label{fig:2013-11-15-2}
\end{figure}

\begin{align*}
	A_{a \to b}^{(1)}(t)
	&= - \frac{\mathrm{i}}{\hbar} \braket{b|V_0|a} \int_{-\infty}^{t} \diff t_1\ \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} E_a t_1} \mathrm{e}^{\eta t_1} \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} E_b (t-t_1)} \\
	&= - \frac{\mathrm{i}}{\hbar} \braket{b|V_0|a} \frac{\mathrm{e}^{\eta t - \mathrm{i} \omega_{ba} t}}{\eta - \mathrm{i} \omega_{ba}} \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} E_b t}
\end{align*}
%
Die Übergangswahrscheinlichkeit ist
%
\begin{align*}
	W_{a \to b}(t)
	&= \frac{|\braket{b|V_0|a}|^2}{\hbar^2} \frac{1}{\eta^2 + \omega_{ba}^2} \mathrm{e}^{2 \eta t}
\end{align*}
%
Die Übergangsrate berechnet sich zu
%
\begin{align*}
	\Gamma_{a \to b}(t) &= \frac{\diff W}{\diff t} = \frac{|\braket{b|V_0|a}|^2}{\hbar^2} \frac{2 \eta}{\eta^2 + \omega_{ba}^2} \mathrm{e}^{2 \eta t}
\end{align*}
%
Wir wissen
%
\begin{enumerate}
	\item Für $|t| < \eta^{-1}$ erhalten wir eine konstante Rate

	\item
		\begin{itemalign}
			\lim\limits_{\eta \to 0} \frac{2 \eta}{\eta^2 + \omega_{ba}^2} = 2 \pi \delta(\omega_{ba}) = 2 \pi \delta(E_b - E_a)
		\end{itemalign}
\end{enumerate}
%
Damit
%
\begin{align*}
	\Gamma_{a \to b}(t) &\to |\braket{b|V_0|a}|^2 \ \frac{2 \pi}{\hbar} \delta(E_b - E_a)
\end{align*}

In der Anwendung aufs Kontinuum muss $\varrho(E)$ berücksichtigt werden.
%
\begin{align*}
	\Gamma_{a \to B} = \int \diff E_b\ \varrho(E_b) \Gamma_{a \to b} = \frac{2 \pi}{\hbar} \varrho(E_b)|_{E_b = E_a} |\braket{b|V_0|a}|^2
\end{align*}
%
Es kommt also dasselbe Ergebnis heraus wie beim plötzlichen Einschalten.

\subsection{Harmonische Störung}

Wir setzen ein harmonisches Störpotential $V(t)$ an mit
\[ V(t) = \left( V \mathrm{e}^{\mathrm{i} \omega_0 t} + V^\dag \mathrm{e}^{-\mathrm{i} \omega_0 t} \right) \mathrm{e}^{\eta t} \]

Durch dieselbe Rechnung wie vorher erhält man dieses Mal
%
\begin{align*}
	A_{a \to b}^{(1)}(t) = - \frac{\mathrm{i}}{\hbar} \mathrm{e}^{- \frac{\mathrm{i}}{\hbar} E_b t} \left\{
		\braket{b|V|a} \frac{\mathrm{e}^{\eta + \mathrm{i} (\omega_0 - \omega_{ba}) t}}{\eta + \mathrm{i} (\omega_0 - \omega_{ba})}
		+\braket{b|V^\dag|a} \frac{\mathrm{e}^{\eta - \mathrm{i} (\omega_0 + \omega_{ba}) t}}{\eta - \mathrm{i} (\omega_0 + \omega_{ba})}
	\right\}
\end{align*}
%
Die Wahrscheinlichkeit ergibt sich damit zu
%
\begin{align*}
	W_{a \to b}(t) &= \left| A_{a \to b}^{(1)}(t) \right|^2 = \dots
\end{align*}
%
Die Übergangsrate ist also
%
\begin{align*}
	\Gamma_{a \to b}(t) &= \frac{\diff W}{\diff t} \\
	&\stackrel{\eta \to 0}{=}
	\begin{multlined}[t]
		\frac{2 \pi}{\hbar} \biggl\{
			|\braket{b|V|a}|^2 \delta( \hbar \omega_0 + (E_b-E_a) )
			+|\braket{b|V^\dag|a}|^2 \delta( \hbar \omega_0 - (E_b-E_a) ) \\
			+\mathcal{O}\left( \cos\omega_0 t, \sin \omega_0 t \right)
		\biggr\}
	\end{multlined}
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$t$};
		\draw[->] (0,-0.3) -- (0,3);
		\draw[dashed] (-0.1,2) node[left] {$E_a$} -- (4,2);
		\draw[DarkOrange3] (1,1) node[left] {$E_b$} -- node[below] {(I)} (2,1);
		\draw[DarkOrange3] (3,3) -- node[above] {(II)} (4,3);
		\draw[DarkOrange3,->] (1.5,2) -- node[right] {$\hbar \omega_0$} (1.5,1);
		\draw[DarkOrange3,->] (3.5,2) -- node[right] {$\hbar \omega_0$} (3.5,3);
		\node[right] at (4,1) {Emission};
		\node[right] at (4,3) {Absorption};
	\end{tikzpicture}
	\caption{Termschema der stimulierten Emission.}
	\label{fig:2013-11-15-3}
\end{figure}

Explizit mit den Zustandsdichten
%
\begin{align*}
	\Gamma_{a \to B} = 
	\begin{dcases}
		\frac{2 \pi}{\hbar} |\braket{b|V|a}|^2 \varrho(E_b) & \text{für } E_b = E_a - \hbar \omega \text{ (I)} \\
		\frac{2 \pi}{\hbar} |\braket{b|V^\dag|a}|^2 \varrho(E_b) & \text{für } E_b = E_a + \hbar \omega \text{ (II)} \\
	\end{dcases}
\end{align*}
%
damit detaillierte Balance herleiten:
%
\begin{align*}
	\braket{b|V^\dag|a} &= \braket{a|V|b}^* \\
	|\braket{a|V|b}| &= |\braket{b|V^\dag|a}| \\
	\Gamma_{b \to A} &= \frac{2 \pi}{\hbar} |\braket{a|V|b}|^2 \varrho(E_a)|_{E_a = E_b - \hbar \omega}
\end{align*}
%
Vergleicht man die Raten $\Gamma_{a \to B}$ und $\Gamma_{b \to A}$ (II), so sieht man
%
\begin{align*}
	\boxed{
		\varrho(E_a) \Gamma_{a \to B} = \varrho(E_b) \Gamma_{b \to A}
	}
\end{align*}
