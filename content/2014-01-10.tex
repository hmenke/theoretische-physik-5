% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 10.01.2014

\renewcommand{\printfile}{2014-01-10}

\subsection{Störungstheorie nach der Coulomb-Wechselwirkung}

Wir gehen davon aus, dass der Hamiltonian die Form \[ H = T + V \] hat. Sei nun $\ket{F}$ ein Eigenzustand (z.\,B.\ Grundzustand) zu $T$, dann führen wir Störungstheorie in $V$ durch. Dazu
%
\begin{align*}
	\braket{F|T|F}
	&= \frac{e^2}{2 a_0} \frac{c}{r_S^2} N \; , \quad \text{mit $\displaystyle a_0 = \frac{\hbar^2}{m e^2}$ (Bohrscher Radius)} \\
	\frac{1}{n}
	&= \frac{4 \pi}{3} (r_S a_0)^3 \; , \quad \text{(Elektronendichte)}
\end{align*}

Für das Potential gilt
%
\begin{align*}
	\braket{F|V|F}
	&= \frac{e^2}{2 V} 4 \pi \sum_{\substack{\bm{q} \neq 0 \\ \bm{k}, \sigma \\ \bm{p},\sigma'}} \frac{1}{q^2} \braket{F| c_{\bm{k} + \bm{q},\sigma}^\dag c_{\bm{p} - \bm{q},\sigma'}^\dag c_{\bm{p},\sigma'} c_{\bm{k},\sigma} |F}
\intertext{nach dem Wick-Theorem bleiben nur zwei Kontraktionen erhalten}
	& \braket{F| \underbrace{c_{\bm{k} + \bm{q},\sigma}^\dag c_{\bm{k},\sigma}}_{\mathclap{=\delta_{\bm{q},0}=0 \text{, weil } \bm{q} \neq 0}} |F} \braket{F| c_{\bm{p} - \bm{q},\sigma'}^\dag c_{\bm{p},\sigma'} |F}
\end{align*}

Damit
%
\begin{align*}
	\braket{F|V|F}
	&= -\frac{e^2}{2 V} 4 \pi 2 \sum_{\substack{\bm{q} \neq 0 \\ \bm{k},\bm{p}}} \frac{1}{q^2} \braket{F| c_{\bm{k} + \bm{q}}^\dag c_{\bm{k}} |F} \braket{F| c_{\bm{p} - \bm{q}}^\dag c_{\bm{p}} |F} \\
	&= - \frac{e^2}{2 V} 4 \pi 2 \sum_{\substack{\bm{q} \neq 0 \\ \bm{k}}} \frac{1}{q^2} \mathop{\Theta}(k_F - |\bm{k} + \bm{q}|) \mathop{\Theta}(k_F - |\bm{k}|) \\
	&= -\frac{e^2}{2 V} 4 \pi 2 \frac{V^2}{(2\pi)^6} \int_0^\infty \diff q\ 4 \pi q^2 \frac{1}{q^2}
	\tikz[every path/.style={},baseline=-2ex,x=2ex,y=2ex]{\filldraw[pattern=north east lines] (0,0) arc (45:-45:1) arc (45:-45:-1) -- cycle;}
	\left( \frac{q}{k_F} \right) \\
	&= -\frac{e^2}{2 a_0} \frac{c_2}{r_S} N \; , \quad \text{mit } c_2 = \left( \frac{9 \pi}{4} \right)^{1/3} \frac{3}{2 \pi}
\end{align*}

\begin{figure}[htpb]
	\centering
	\def\r{1.5}
	\begin{tikzpicture}
		\coordinate (A) at (-0.75*\r,0);
		\coordinate (B) at (0.75*\r,0);
		\draw (A) circle (\r) (B) circle (\r);
		\draw (A) node[dot,label={below:$q$}] {} -- (B) -- ++(\r,0);
		\draw[->] (B) -- ++(45:\r);
		\draw[->] (B) -- ++(45:\r);
		\draw (B)+(0:0.6*\r) arc (0:45:0.6*\r);
		\path (B)+(22.5:0.4*\r) node {$\theta_k$};
		\begin{scope}[every path/.style={}]
			\path[clip] (A) circle (\r);
			\fill[pattern=north east lines,pattern color=DimGray] (B) circle (\r);
		\end{scope}
		\draw[<-] (0,-0.75*\r) -- (0,-2) node[below] {%
			$\displaystyle=2 \pi \int_{\frac{q}{2 k_F}}^{1} \diff(\cos \theta_k) \int_{\frac{q}{2} \cos \theta_k}^{k_F} k^2 \diff k$
		};
	\end{tikzpicture}
	\caption{Zur Herleitung des Matrixelements des Potentials.}
	\label{fig:2014-01-10-1}
\end{figure}

Der Störungsparameter ist $r_S$. Je kleiner $r_S$ ist, desto besser ist die Störungstheorie.

Die Energie ist negativ, weil die Elektronen-Ionen-Wechselwirkung die Ionen-Ionen-Wechselwirkung und die Elektronen-Elektronen-Wechselwirkung, die repulsiv sind, übertrifft.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4.2,0) node[below] {$r_S$};
		\draw[->] (0,-1.3) -- (0,3) node[left] {$E$};
		\draw plot[smooth,domain=0.4:4] (\x,{-1.5/(2*\x)});
		\draw plot[smooth,domain=0.3:4] (\x,{1/(4*\x*\x)});
		\draw[MidnightBlue] plot[smooth,samples=41,domain=0.19:4] (\x,{-1.5/(2*\x)+1/(4*\x*\x)});
		\draw (2/3,0.1) -- (2/3,-0.1) node [below] {\num{4.8}};
		\draw (0.1,-0.5625) -- (-0.1,-0.5625) node [left] {\SI{-1.3}{\electronvolt}};
	\end{tikzpicture}
	\caption{$1/r_S$ und $1/r_S^2$-Anteile zusammmen.}
	\label{fig:2014-01-10-2}
\end{figure}

Aber: $r_S$ ist nicht klein, deswegen müssen in der Störungstheorie höhere Ordnungen berücksichtigt werden.
%
\begin{align*}
	E^{(2)}
	&= \sum_{\ket{Z}} \frac{\braket{F|V|Z}\braket{Z|V|V}}{E_Z - E_F}
\end{align*}
%
erwarte
%
\begin{align*}
	E^{(2)} = \pm\frac{e^2}{2 a_0} c_3 N
\end{align*}
%
aber die Integrale divergieren.

Tatsächlich erhält man durch aufsummieren aller möglichen Wege
%
\begin{align*}
	E^{(2)} \sim +\num{0.06} \ln r_S - \num{0.09} + \const \cdot r_S
\end{align*}

\subsubsection[Wigner-Kristall]{Wigner-Kristall (1938)}

Bei kleiner Dichte überwiegt die Coulomb-Wechselwirkung, somit ist eine regelmäßige Anordnung günstiger.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\foreach \x in {1,2,3} {
			\foreach \y in {1,2} {
				\node[dot] at (\x,\y) {};
			}
		}
		\filldraw[pattern=north east lines] (1,2) circle (0.5);
		\path (1,2)+(45:0.5) node[text width=8.4em,above right] {Ionen in Kugel mit Radius $r_S \cdot a_0$};
	\end{tikzpicture}
	\caption{Ionen auf einem Gitter.}
	\label{fig:2014-01-10-3}
\end{figure}

Potential für ein Elektron im Hintergrund einer Positiven homogenen Ladung:
%
\begin{align*}
	\nabla^2 \phi
	&= 4 \pi \varrho^+ \; , \quad \text{für } |\bm{r}| < r_S a_0 \\
	&= 4 \pi e \frac{1}{\frac{4\pi}{3} r_S^3 a_0^3} \\
	&= \frac{3 e}{a_0^3 r_S^3} \\
	\leadsto \phi(r)
	&= \const + \frac{3}{2} \frac{e}{a_0^3} \frac{r^2}{r_S^3}
\end{align*}
%
mit der Anfangsbedingung
\[ \phi(r = r_S a_0) = - \frac{e}{r} \]
ergibt sich
%
\begin{align*}
	\const &= - \frac{3}{2} \frac{e}{a_0 r_S} \\
	\phi(r) &= - \frac{3}{2} \frac{e}{a_0 r_S} + \frac{3}{2} \frac{e}{a_0^3} \frac{r^2}{r_S^3} \; , \quad \text{für } r < r_S a_0
\end{align*}

Mit $r_0 = r_S a_0$ lässt sich schreiben
%
\begin{align*}
	V(r) &= e\ \phi(r) = - \frac{3}{2} \frac{e^2}{r_0} + \frac{e^2}{2} \frac{r^2}{r_0^3} \\
	H_\textnormal{eff} &= U(r) + \frac{p^2}{2m}
\end{align*}

Mit
%
\begin{align*}
	\omega = \sqrt{\frac{2 e^2}{m (r_S a_0)^3}}
\end{align*}
%
folgt
%
\begin{align*}
	\frac{E}{N} &= \braket{0|H_\textnormal{eff}|0} = \frac{1}{2} \frac{e^2}{a_0} \left( - \frac{3}{r_S} + \frac{\const}{r_S} \right)
\end{align*}

Mit Gitterschwingungen
%
\begin{align*}
	\frac{E}{N} &= \frac{e^2}{2 a_0} \left( - \frac{\num{179}}{r_S} + \frac{\num{2.68}}{r_S^{3/2}} \right)
\end{align*}

\subsubsection{Volles Phasendiagramm}

Für mittlere Dichte ist das Fermigas polarisiert, $\braket{\sigma_z} \neq 0$.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (5.2,0) node[right] {$r_S$};
		\draw[->] (0,-0.3) -- (0,3) node[left] {$\langle \sigma_z \rangle$};
		\draw (0.1,2) -- (-0.1,2) node[left] {$1$};
		\draw (1.5*3,0) -- (1.5*3,2.5);
		\foreach \i [count=\j] in {20,40,65} {
			\draw (1.5*\j,0.1) -- (1.5*\j,-0.1) node[below] {$\i \pm 5$};
		}
		\draw[MidnightBlue] (0,0) -- (1.5*1,0) .. controls +(0.7,2) .. (1.5*2,2) -- (1.5*3,2);
		\node[right,text width=1.5cm] at (0,1) {Störungs\-theorie};
		\node[right] at (1.5*3,1) {bcc-Wigner};
	\end{tikzpicture}
	\caption{Phasendiagramm.}
	\label{fig:2014-01-10-4}
\end{figure}

Dieses Eregbnis beruht auf Rechnungen mit $2000$ Elektronen, \fullcite{PhysRevLett.82.5317}.
