% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 25.10.2013

\renewcommand{\printfile}{2013-10-25}

\subsection{von Neumann Entropie}

\begin{theorem}[Definition]
	Wir definieren die \acct{von Neumann-Entropie} $S(\varrho)$ als
	%
	\begin{align*}
		S(\varrho) = - \tr( \varrho \ln \varrho )
	\end{align*}
\end{theorem}

Sei $\{\ket{\nu}\}$ die Basis, in der $\varrho$ diagonal ist, also $\braket{\nu|\varrho|\nu'} \sim \delta_{\nu\nu'}$. Folglich lässt sich $S(\varrho)$ darstellen als
%
\begin{align*}
	S(\varrho) = - \sum\limits_{\nu} \braket{\nu|\varrho \ln \varrho|\nu} = - \sum\limits_{\nu} \varrho_\nu \ln \varrho_\nu
\end{align*}

Dies führt auf folgende Eigenschaften:
%
\begin{enumerate}
	\item Ein reiner Zustand liegt vor, wenn alle $\varrho_\nu = 0$ bis auf eines, hier das $\mu$-te.
		\[ \varrho = \ket{\mu}\bra{\mu} \; , \quad \varrho_\mu = 1 \; , \quad \varrho_{\nu \neq \mu} = 0 \]
		Dann ist $S(\varrho) = 0$.

	\item
		\begin{itemalign}
			\varrho
			&= \frac{1}{M} \sum\limits_{m=1}^{M} \ket{\psi_m}\bra{\psi_m} \; , \quad \text{mit } \braket{\psi_m|\psi_{m'}} = \delta_{mm'} \\
			&= \frac{1}{M} \mathds{1} \\
			S(\varrho)
			&= - \sum\limits_{m=1}^{M} \varrho_m \ln \varrho_m \\
			&= - \sum\limits_{m=1}^{M} \frac{1}{M} \ln \frac{1}{M} = \ln M
		\end{itemalign}
		%
		Die Entropie wird maximal, wenn jeder Zustand gleich wahrscheinlich ist: $0 \leq S(\varrho) \leq M$.
\end{enumerate}

\subsection{Systeme im thermischen Gleichgewicht}

Wir betrachten ein quantenmechanisches System, das durch die Dichtematrix $\varrho$ beschrieben wird in einem Wärmebad der Temperatur $T$, wie in Abbildung~\ref{fig:2013-10-25-1} dargestellt.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\node[draw,rectangle] at (0.5,0.5) {$\varrho$};
		\draw (0,0) rectangle (1.5,1.5) node[below left] {$T$};
	\end{tikzpicture}
	\caption{Quantenmechanisches System im Wärmebad}
	\label{fig:2013-10-25-1}
\end{figure}

Das System hat die Basis $H \ket{n} = E_n \ket{n}$. Für die Temperatur gilt $\beta = 1/\kB T$.

Im thermischen Gleichgewicht wird das System beschrieben durch die Dichtematrix $\varrho^\textrm{eq}$
%
\begin{align*}
	\varrho^\textrm{eq}
	&= \mathrm{e}^{-\beta (H - F)} \; , \quad (F = - \kB T \ln Z \text{ mit } Z = \tr \mathrm{e}^{-\beta H}) \\
	&= \sum\limits_{n} \mathrm{e}^{-\beta (E_n - F)} \ket{n}\bra{n}
\end{align*}

Die Wahrscheinlichkeit ein System im thermischen Gleichgewicht in einem Zustand $\ket{m}$ anzutreffen ist
%
\begin{align*}
	\prob[\ket{\psi} = \ket{m} | \varrho^\textrm{eq} ]
	&= \mathrm{e}^{-\beta (H - F)}
\end{align*}
%
Dies ist gerade eine Boltzmann-Verteilung um die freie Energie $F$. Eine Skizze findet sich in Abbildung~\ref{fig:2013-10-25-2}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (3,0) node[below] {$E$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {$p^\textrm{eq}$};
		\draw[MidnightBlue] plot[domain=1:3] (\x,{4*\x*\x*exp(-\x*\x});
		\draw[dotted] (1,1.5) -- (1,0) node[below] {$F$};
	\end{tikzpicture}
	\caption{Boltzmann-Verteilung}
	\label{fig:2013-10-25-2}
\end{figure}

Der Erwartungswert von $H$ im Gleichgewicht ist
%
\begin{align*}
	\braket{H}_{\varrho^\textrm{eq}}
	&= \tr H \varrho^\textrm{eq} \\
	&= \sum\limits_{n} E_n p_n \\
	&= \sum\limits_{n} E_n \mathrm{e}^{-\beta (H-F)}
\end{align*}

Die Entropie ist
%
\begin{align*}
	S(\varrho^\textrm{eq})
	&= - \tr \varrho^\textrm{eq} \ln \varrho^\textrm{eq} \\
	&= \sum\limits_{n} p_n \beta (E_n - F) \\
	&= \beta \braket{H}_{\varrho^\textrm{eq}} - \beta F
\end{align*}
%
(vgl.\ $F = U - TS$)

\begin{example}
	Betrachten wir ein Spin-$1/2$-Teilchen im Wärmebad mit einem Magnetfeld $\bm{B} = B \bm{e}_z$.\footnote{vgl.\ Nuclear Magnetic Resonance}
	%
	\begin{align*}
		H = \frac{\hbar \omega}{2} \sigma_z
	\end{align*}

	Die Dichtematrix ist gegeben durch
	%
	\begin{align*}
		\varrho^\textrm{eq} = \mathrm{e}^{-\beta (H-F)}
	\end{align*}

	Berechnen wir nun die freie Energie
	%
	\begin{align*}
		F
		&= - \kB T \ln Z \\
		&= - \kB T \ln \tr \mathrm{e}^{-\beta H} \\
		&= - \kB T \ln \left( \braket{z+|\mathrm{e}^{-\beta H}|z+} + \braket{z-|\mathrm{e}^{-\beta H}|z-} \right) \\
		&= - \kB T \ln \left( \mathrm{e}^{-\beta \frac{\hbar \omega}{2}} + \mathrm{e}^{\beta \frac{\hbar \omega}{2}}\right) \\
		&= - \kB T \ln \left( 2 \cosh\left( \beta \frac{\hbar \omega}{2} \right) \right)
	\end{align*}

	Das Verhältnis der Wahrscheinlichkeiten, Spin-Up und Spin-Down zu messen, ist
	%
	\begin{align*}
		\frac{\prob[\sigma_z \hateq +1 | \varrho^\textrm{eq}]}{\prob[\sigma_z \hateq -1 | \varrho^\textrm{eq}]}
		&= \frac{\tr\left( \varrho^\textrm{eq} \ket{z+}\bra{z+} \right)}{\tr\left( \varrho^\textrm{eq} \ket{z-}\bra{z-} \right)}
		= \mathrm{e}^{-\beta \hbar \omega}
	\end{align*}
\end{example}

\section{Zusammengesetzte und verschränkte Systeme}

Ein Beispiel für ein zusammengesetztes System ist das Wasserstoffatom, bei welchem ein unendlich-dimensionaler Hilbertraum für die Ortsbewegung mit einem zwei-dimensionalen Hilbertraum für den Spin verknüpft ist.

\subsection{Hilbertraum als Tensorprodukt}

Wir betrachten zwei Systeme, die jeweils durch einen eigenen Hilbertraum beschrieben werden
%
\begin{align*}
	\mathcal{H}^{(1)} &\text{ mit Basis } \{ \ket{n} , n = 1,\dotsc,N \} \\
	\mathcal{H}^{(2)} &\text{ mit Basis } \{ \ket{m} , m = 1,\dotsc,N \}
\end{align*}

\begin{example}
	Wir betrachten ein Elektron auf dem Benzolring, das zwischen den einzelnen Atomen hin- und herspringen kann.
	
	\begin{figure}[htpb]
		\centering
		\begin{tikzpicture}
			\foreach \i [count=\j] in {60,120,...,360} {
				\draw (\i:1) -- +(\i+120:1) node[dot,label={\i+60:$\ket{\j}$}] (n\j) {};
			}
			\draw (0,0) circle[radius=0.5];
			\draw[DarkOrange3,->] ($(n5)+(0,-0.3)$) -- ($(n5)+(0,0.3)$) node[above right] {$\ket{\psi}$};
		\end{tikzpicture}
		\caption{Benzolring}
		\label{fig:2013-10-25-3}
	\end{figure}

	Der Ort kann auch eine Überlagerung mehrerer Positionen sein. Zusätzlich besitzt das Elektron einen Spin. Wir müssen also die beiden Systeme betrachten
	%
	\begin{align*}
		\ket{\psi_1} &= \sum\limits_{n=1}^{N} c_n \ket{n} \\
		\ket{\psi_2} &= d_+ \ket{z+} + d_- \ket{z-}
	\end{align*}
\end{example}

Der Gesamtraum ergibt sich aus dem Tensorprodukt der beiden Räume
%
\begin{align*}
	\mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)}
\end{align*}
%
mit der Basis
%
\begin{align*}
	\mathcal{B} = \left\{ \ket{n} \otimes \ket{m} \right\} \text{ mit } \dim \mathcal{B} = N \cdot M
\end{align*}
%
Für unseren Benzolring heißt das konkret
%
\begin{align*}
	\mathcal{B} = \left\{ \ket{1} \otimes \ket{z+}, \ket{1} \otimes \ket{z-}, \dotsc, \ket{6} \otimes \ket{z+}, \ket{6} \otimes \ket{z-} \right\}
\end{align*}

Für einen beliebigen reinen Zustand aus dem Gesamtraum $\mathcal{H}$ bedeutet dies
%
\begin{align*}
	\ket{\psi}
	&= \sum\limits_{nm} a_{nm} \ket{n} \otimes \ket{m} \\
	&= \sum\limits_{j=1}^{N \cdot M} a_j \ket{j}
\end{align*}

\begin{notice}[Beachte:]
	Nicht jedes $\ket{\psi} \in \mathcal{H}$ lässt sich schreiben als $\ket{\psi} = \ket{\psi_1} \otimes \ket{\psi_2}$ mit $\ket{\psi_1} \in \mathcal{H}^{(1)}$ und $\ket{\psi_1} \in \mathcal{H}^{(2)}$.

Beispielsweise faktorisiert für den Benzolring der Zustand
%
\begin{align*}
	\ket{\psi} = \frac{1}{\sqrt{2}} \Big( \ket{1} \otimes \ket{z+} \pm \ket{2} \otimes \ket{z-} \Big) \neq \ket{\psi_1} \otimes \ket{\psi_2}
\end{align*}
%
nicht in Orts- und Spinanteil.
Dieser Zustand ist \acct*{verschränkt}!\index{Verschränkung}
\end{notice}

\minisec{Skalarprodukt in $\mathcal{H}$}

\begin{align*}
	\left( \bra{n'} \otimes \bra{m'} \middle| \ket{n} \otimes \ket{m} \right) = \delta_{nn'} \delta_{mm'}
\end{align*}
%
Allgemein gilt
%
\begin{align*}
	\ket{\psi} &= \sum\limits_{nm} a_{nm} \ket{n} \otimes \ket{m} \\
	\ket{\phi} &= \sum\limits_{nm} b_{nm} \ket{n} \otimes \ket{m} \\
	\braket{\phi|\psi} &= \sum\limits_{nm} b_{nm}^* a_{nm}
\end{align*}

\subsection{Operatoren in verschränkten Räumen}

Sei $A^{(1)}$ ein Operator in $\mathcal{H}^{(1)}$ und $B^{(2)}$ ein Operator in $\mathcal{H}^{(2)}$. Im Gesamtraum $\mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)}$ gilt
%
\begin{align*}
	A^{(1)} &\to A^{(1)} \otimes \mathds{1}^{(2)} \\
	B^{(1)} &\to \mathds{1}^{(1)} \otimes B^{(2)}
\end{align*}

Für einen allgemeinen Operator, der in beiden Unterräumen wirkt gilt
%
\begin{align*}
	A^{(1)} \otimes B^{(2)} \ket{\psi}
	&= \left( A^{(1)} \otimes B^{(2)} \right) \left( \ket{\psi_1} \otimes \ket{\psi_2} \right) \\
	&= \left( A^{(1)} \ket{\psi_1} \right) \otimes \left( B^{(2)}  \ket{\psi_2} \right)
\intertext{in Spektraldarstellung}
	&= \left( A^{(1)} \otimes B^{(2)} \right) \left( \sum\limits_{nm} a_{nm} \ket{n} \otimes \ket{m} \right) \\
	&= \sum\limits_{nm} a_{nm} \left( A^{(1)} \ket{n}^{(1)} \right) \otimes \left( B^{(2)} \ket{m}^{(2)} \right) \\
	&= \sum\limits_{nm} a_{nm} \left( \sum\limits_{k} A^{(1)}_{kn} \ket{k}^{(1)} \right) \otimes \left( \sum\limits_{\ell} B^{(2)}_{\ell m} \ket{\ell}^{(2)} \right) \\
	&= \sum\limits_{k\ell} c_{k\ell} \ket{k}^{(1)} \otimes \ket{\ell}^{(2)}
\intertext{wobei}
	c_{k\ell} &= \sum\limits_{nm} a_{nm} A^{(1)}_{kn} B^{(2)}_{\ell m}
\end{align*}

\begin{notice}[Vereinfachte Notation:]
	\[ \ket{n}^{(1)} \otimes \ket{m}^{(2)} = \ket{n}^{(1)} \ket{m}^{(2)} = \ket{n^{(1)} m^{(2)}} = \ket{n,m} = \ket{nm} \]
\end{notice}

\section[Zwei Spin \texorpdfstring{$1/2$}{1/2} Systeme]{Zwei Spin \boldmath $1/2$ Systeme}

Wir betrachten zwei (durch ihre Orte unterscheidbare) Teilchen. Der gemeinsame Hilbertraum ist gegeben durch
\[ \mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)} \]
Die Basis dieses Hilbertraums ist
\[ \mathcal{B} = \left\{
	\ket{z+}^{(1)} \otimes \ket{z+}^{(2)},
	\ket{z+}^{(1)} \otimes \ket{z-}^{(2)},
	\ket{z-}^{(1)} \otimes \ket{z+}^{(2)},
	\ket{z-}^{(1)} \otimes \ket{z-}^{(2)}
\right\} \]
oder in der Kurzdarstellung
\[ \mathcal{B} = \left\{ \ket{++}, \ket{+-}, \ket{-+}, \ket{--} \right\} \]

\subsection{Bell-Zustand}

Der \enquote{wichtigste} Zustand ist wohl der \acct{Bell-Zustand}\footnote{nach John Bell}
\[ \boxed{\ket{B} = \frac{1}{\sqrt{2}} \left( \ket{+-} - \ket{-+} \right)} \]
Als Beispiel für die Wirkung von Operatoren betrachten wir die beiden folgenden $\in \mathcal{H} = \mathcal{H}^{(1)} \otimes \mathcal{H}^{(2)}$
%
\begin{align*}
	\bm{S} &= \frac{\hbar}{2} \left(\bm{\sigma}^{(1)} \otimes \mathds{1}^{(2)} + \mathds{1}^{(1)} \otimes \bm{\sigma}^{(2)} \right) \\
	S_z  &= \frac{\hbar}{2} \left(\sigma_z^{(1)} \otimes \mathds{1}^{(2)} + \mathds{1}^{(1)} \otimes \sigma_z^{(2)} \right) .
\end{align*}
%
Für $S_z\Ket{B}$ bedeutet dies:
%
\begin{align*}
	S_z\Ket{B} &= \frac{\hbar}{2} \left(\sigma_z^{(1)} \otimes \mathds{1}^{(2)} + \mathds{1}^{(1)} \otimes \sigma_z^{(2)} \right) \frac{1}{\sqrt{2}} \left(\Ket{+-} - \Ket{-+}\right) \\
	&= \frac{\hbar}{2\sqrt{2}} \left\{ \Ket{+-} + \Ket{-+} - \Ket{+-} - \Ket{-+} \right\} \\
	&= \ket{0} = 0 \Ket{B}
\end{align*}
%
Analog folgt für $\bm{S}^2 \Ket{B}$:
%
\begin{align*}
	\bm{S}^2 \Ket{B}
	&= \frac{\hbar^2}{4} \left\{ (\bm{\sigma}^{(1)})^2 \otimes \mathds{1}^{(2)} + \mathds{1}^{(1)} \otimes (\bm{\sigma}^{(2)})^2 + 2 \bm{\sigma}^{(1)} \otimes \bm{\sigma}^{(2)}  \right\} \Ket{B} \\
	&= \frac{\hbar^2}{4} \left\{6 \cdot \mathds{1} + 2 \bm{\sigma}^{(1)} \otimes \bm{\sigma}^{(2)} \right\} \Ket{B}
\end{align*}
%
\begin{notice*}[Nebenrechnung:]
	%
	\begin{align*}
		\bm{\sigma}^{(1)} \otimes \bm{\sigma}^{(2)} &= \sigma_x^{(1)} \otimes \sigma_x^{(2)} +\sigma_y^{(1)} \otimes \sigma_y^{(2)} +\sigma_z^{(1)} \otimes \sigma_z^{(2)} \\
	\end{align*}
	%
	Betrachte nur den mittleren Term:
	%
	\begin{align*}
		\sigma_y^{(1)} \otimes \sigma_y^{(2)} \Ket{+-} &= \begin{pmatrix} 0 & -\mathrm{i} \\ \mathrm{i} & 0  \end{pmatrix} \begin{pmatrix} 1 \\ 0 \end{pmatrix} \otimes \begin{pmatrix} 0 & -\mathrm{i} \\ \mathrm{i} & 0  \end{pmatrix} \begin{pmatrix} 0 \\ 1 \end{pmatrix} \\
		&= \begin{pmatrix} 0 \\ \mathrm{i} \end{pmatrix} \otimes \begin{pmatrix} -\mathrm{i} \\ 0 \end{pmatrix} \\
		&= \begin{pmatrix} 0 \\ 1 \end{pmatrix} \otimes \begin{pmatrix} 1 \\ 0 \end{pmatrix} \\
		&= \Ket{-+} \\
		\sigma_y^{(1)} \otimes \sigma_y^{(2)} \Ket{B} &= -\Ket{B}.
	\end{align*}
\end{notice*}

Dies bedeutet also für $\bm{S}^2 \Ket{B}$:
\[
	\boxed{\bm{S}^2 \Ket{B} = 0 \Ket{B}}
\]
