% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 12.12.2013

\renewcommand{\printfile}{2013-12-12}

\chapter{Vielteilchensysteme}

\section{Identische Teilchen}

\subsection{Permutationssymmetrie}

Der Hamiltonoperator ist invariant unter Permutation identischer Teilchen.
%
\begin{align*}
	H(\bm{R}_1,\bm{P}_1,\chi_1,\bm{R}_2,\bm{P}_2,\chi_2) = 	H(\bm{R}_2,\bm{P}_2,\chi_2,\bm{R}_1,\bm{P}_1,\chi_1)
\end{align*}
%
wobei $\chi_i$ innere Freiheitsgrade (z.\,B.\ Spin) darstellen.

\begin{example}
	$\aligned H = \frac{\bm{p}_1^2}{2 m} + \frac{\bm{p}_2^2}{2 m} + \bm{\sigma}_1 \bm{\sigma}_2 J(|\bm{r}_1 - \bm{r}_2|) \endaligned$
\end{example}

\minisec{Zwei identische Teilchen}

Betrachte den Hilbertraum von zwei Teilchen
%
\begin{align*}
	\Big\{ \ket{\alpha} \otimes \ket{\beta} \Big\} \in \mathcal{H} \otimes \mathcal{H} \equiv \mathcal{H}^{(2)}
\end{align*}
%
In diesem Hilbertraum gibt es Zustände $\ket{\psi}$ mit
%
\begin{align*}
	\ket{\psi} = \sum_{\alpha,\beta} c_{\alpha\beta} \ket{\alpha} \otimes \ket{\beta}
\end{align*}
%
wählen wir zum Beispiel die Ortsdarstellung ergibt sich
%
\begin{align*}
	\braket{\bm{x}_1, \bm{x}_2 | \psi} = \psi(\bm{x}_1, \bm{x}_2) = \sum_{\alpha,\beta} c_{\alpha\beta} \psi_\alpha(\bm{x}_1) \otimes \psi_\beta(\bm{x}_2)
\end{align*}

\begin{theorem}[Definition]
	Der Permutationsoperator $\mathcal{P}_{12}$
	%
	\begin{align*}
		\mathcal{P}_{12} \ket{\alpha} \otimes \ket{\beta} = \ket{\beta} \otimes \ket{\alpha}
	\end{align*}
	%
	mit
	%
	\begin{itemize}
		\item $\mathcal{P}_{12}^2 = \mathds{1}$
		\item $\mathcal{P}^{-1} = \mathcal{P}$
		\item $\mathcal{P}^2 = \pm 1$
		\item Die Eigenwerte sind $\pm 1$
	\end{itemize}
	%
	In Ortdarstellung
	%
	\begin{align*}
		\braket{x_1,x_2 | \mathcal{P}_{12} | \psi}
		&= \psi(x_2,x_1) \\
		&= \sum_{\alpha\beta} c_{\alpha\beta} \psi_\alpha(x_2) \psi_\beta(x_1)
	\end{align*}

	Die Eigenvektoren zum Eigenwert $+1$ sind
	%
	\begin{align*}
		\mathcal{P}_{12} \ket{\psi} &= + \ket{\psi} \\
		\psi(x_1,x_2) &= \psi(x_2,x_1)
	\end{align*}
	%
	Für den Eigenwert $-1$ analog
	%
	\begin{align*}
		\psi(x_1,x_2) &= - \psi(x_2,x_1)
	\end{align*}

	Falls $H$ mit $\mathcal{P}_{12}$ vertauscht gibt es eine gemeinsame Basis. Die Eigenzustände von $H$ können entweder als symmetrisch oder antisymmetrisch unter $\mathcal{P}_{12}$ gewählt werden.
	%
	\begin{align*}
		\mathcal{H}^{(2)} = \mathcal{H} \otimes \mathcal{H} = \mathcal{H}_s^{(2)} \oplus \mathcal{H}_a^{(2)}
	\end{align*}
	%
	Dies entspricht der Zerlegung des gesamten Hilbertraums in zwei invariante Teilräume.
\end{theorem}

\minisec{Drei oder mehr identische Teilchen}

Für drei Teilchen ist die Basis
%
\begin{align*}
	\Big\{ \ket{\alpha} \otimes \ket{\beta} \otimes \ket{\gamma} \Big\}
	&= \mathcal{H} \otimes \mathcal{H} \otimes \mathcal{H} = \mathcal{H}^{(3)}
\end{align*}
%
Bei drei Teilchen gibt es sechs verschiedene Permutationen:
%
\begin{align*}
	\Big\{ \mathds{1}, \mathcal{P}_{12}, \mathcal{P}_{23}, \mathcal{P}_{13}, \mathcal{P}_{123}, (\mathcal{P}_{123})^2 \Big\} = \mathcal{S}_3 \; , \quad \text{Permutationsgruppe}
\end{align*}
%
zum Beispiel
%
\begin{align*}
	\mathcal{P}_{123} \ket{\alpha\beta\gamma} = \ket{\gamma\alpha\beta}
\end{align*}

Diese Permutationen vertauschen aber im allgemeinen nicht untereinander.
%
\begin{align*}
	\mathcal{P}_{12} \mathcal{P}_{23} \neq \mathcal{P}_{23} \mathcal{P}_{12}
\end{align*}
%
Wieder ein Beispiel
%
\begin{align*}
	\mathcal{P}_{12} \mathcal{P}_{23} \ket{\alpha\beta\gamma} &= \mathcal{P}_{12} \ket{\alpha\gamma\beta} = \ket{\gamma\alpha\beta} \\
	\mathcal{P}_{23} \mathcal{P}_{12} \ket{\alpha\beta\gamma} &= \mathcal{P}_{23} \ket{\beta\alpha\gamma} = \ket{\beta\gamma\alpha}
\end{align*}

Es gibt folglich keine gemeinsame Eigenbasis für alle $\mathcal{P}$. In diesem Fall sind nicht alle Eigenzustände von $H$ symmetrisch oder antisymmetrisch unter Paarvertauschung.

$\mathcal{H}^{(2)}$ kann aber in $\psi$ unter allen permutationsinvarianten Unterräumen aufgestellt werden:
%
\begin{align*}
	\mathcal{H}^{(3)} = \mathcal{H}_s^{(3)} \oplus \mathcal{H}_a^{(3)} \oplus \mathcal{H}_{M_1}^{(3)} \oplus \mathcal{H}_{M_2}^{(3)}
\end{align*}
%
Dabei wird $\mathcal{H}_s^{(3)}$ aufgespannt durch
%
\begin{align*}
	\ket{\psi_s} &= \frac{1}{\sqrt{6}} \Big\{
		\ket{\alpha\beta\gamma} +
		\ket{\beta\alpha\gamma} +
		\ket{\gamma\beta\alpha} +
		\ket{\alpha\gamma\beta} +
		\ket{\beta\gamma\alpha} +
		\ket{\gamma\alpha\beta}
	\Big\}
\end{align*}
%
$\mathcal{H}_a^{(3)}$ wird aufgespannt durch
%
\begin{align*}
	\ket{\psi_a} &= \frac{1}{\sqrt{6}} \Big\{
		\ket{\alpha\beta\gamma} -
		\ket{\beta\alpha\gamma} -
		\ket{\gamma\beta\alpha} -
		\ket{\alpha\gamma\beta} +
		\ket{\beta\gamma\alpha} +
		\ket{\gamma\alpha\beta}
	\Big\}
\end{align*}
%
$\mathcal{H}_{M_1}^{(3)}$ ist ein zweidimensionaler Unterraum aufgespannt durch
%
\begin{align*}
	\ket{\psi_1} &= \frac{1}{\sqrt{12}} \Big\{
		2 \ket{\alpha\beta\gamma} +
		2 \ket{\beta\alpha\gamma} -
		\ket{\alpha\gamma\beta} -
		\ket{\gamma\beta\alpha} -
		\ket{\gamma\alpha\beta} -
		\ket{\beta\gamma\alpha}
	\Big\} \\
	\ket{\psi_2} &= \frac{1}{\sqrt{12}} \Big\{
		2 \ket{\alpha\beta\gamma} +
		2 \ket{\beta\alpha\gamma} -
		\ket{\alpha\gamma\beta} +
		\ket{\gamma\beta\alpha} +
		\ket{\gamma\alpha\beta} -
		\ket{\beta\gamma\alpha}
	\Big\}
\end{align*}
%
Entsprechend für $\mathcal{H}_{M_2}^{(3)}$.

Es gilt also: Für $\psi \in \mathcal{H}_{M_1}^{(3)}$ ist $\mathcal{P} \psi \in \mathcal{H}_{M_1}^{(3)}$ aber im Allgemeinen $\mathcal{P} \psi \neq \lambda \psi$.

\begin{notice}[Wichtig:]
	Die Symmetrieeigenschaften ändern sich unter der unitären Zeitentwicklung
	\[ U(t) = \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H t} \] nicht.
	Das bedeutet
	\[ \psi(t_0) \in \mathcal{H}_{a/s} \leadsto \psi(t) \in \mathcal{H}_{a/s} \]
\end{notice}

\subsection{Ununterscheidbarkeit}

\begin{notice}[Prinzip:]
	Dynamische Zustände, die sich nur durch die Permutation identischer Teilchen unterscheiden, können durch keine Observable unterschieden werden.
\end{notice}

Das heißt $\ket{\psi}$ und $\mathcal{P}_{ij} \ket{\psi}$ sind nicht unterscheidbar durch die Observable $A$, also
%
\begin{align*}
	\braket{\psi|A|\psi}
	&\stackrel{!}{=} \braket{\mathcal{P}_{ij} \psi | A | \mathcal{P}_{ij} \psi} \\
	&= \braket{\psi \mathcal{P}_{ij} | A | \mathcal{P}_{ij} \psi} \\
	\leadsto [\mathcal{P}_{ij}, A] &= 0
\end{align*}

\begin{example}
	Betrachte $N$ Spins auf freien Elektronen
	\[ S_x \equiv \sum_{i} S_x^{(i)} \]
	womit sofort folgt
	\[ [\mathcal{P}_{ij}, S_x ] = 0. \]
	Der Operator eines einzelnen Spins $A = S_x^{(1)}$ ist jedoch keine zulässige Observable\footnote{Für Spins auf einem Gitter wird der Operator eines einzelnen Spins jedoch wieder eine gültige Observable, da die Spins durch ihren Ort unterscheidbar werden}.
	% Außerdem hat Cedric zum ersten Mal in diesem Semester was Richtiges gesagt!
\end{example}

\begin{theorem}[Superauswahlregel]
	\index{Superauswahlregel}
	Die Interferenz zwischen Zuständen verschiedener Permutationssymmetrie sind nicht beobachtbar.
\end{theorem}

\begin{proof}
	Sei $\ket{s}$ symmetrisch mit $\mathcal{P}_{ij} \ket{s} = \ket{s}$ und $\ket{a}$ antisymmetrisch mit $\mathcal{P}_{ij} \ket{a} = - \ket{a}$.
	Dann gilt für die Observable $A$:
	%
	\begin{align*}
		\braket{s|A|a}
		&= \braket{\mathcal{P}_{ij} s|A|a} \\
		&= \braket{s | \mathcal{P}_{ij} A | a} \\
		&= \braket{s | A \mathcal{P}_{ij} | a} \\
		&= - \braket{s | A | a} \\
		\leadsto \braket{s | A | a} &= 0
	\end{align*}

	Wir bilden eine hypothetische Superposition von $\ket{s}$ und $\ket{a}$.
	%
	\begin{align*}
		\ket{\psi} &= \ket{s} + \mathrm{e}^{\mathrm{i} \phi} \ket{a}
	\end{align*}
	%
	Damit gilt für die Observable $A$
	%
	\begin{align*}
		\braket{\psi|A|\psi}
		&= \braket{s|A|s} + \braket{a|A|a}
	\end{align*}
	%
	ist unabhängig von $\phi$.
\end{proof}

\subsection{Symmetrisierungspostulat}

\begin{enumerate}
	\item\label{itm:2013-12-12-1} Teilchen mit ganzzahligem Spin (einschließlich $S = 0$) besitzen nur symmetrische Zustände ($\in \mathcal{H}_s^{(N)}$). Sie heißen Bosonen und erfüllen die Bose-Einstein-Statistik.
	\item\label{itm:2013-12-12-2} Teilchen mit halbzahligem Spin besitzen nur antisymmetrische Zustände ($\in \mathcal{H}_a^{(N)}$). Sie heißen Fermionen und erfüllen die Fermi-Dirac-Statistik.
	\item Teilchen mit gemischter Statistik (Parastatistik) treten in der Natur nicht auf.
\end{enumerate}

Dieses Postulat kann in der nicht-relativistischen Quantenmechanik nicht hergeleitet werden, sondern ist ein empirisches Faktum. In drei Dimensionen folgen \ref{itm:2013-12-12-1} und \ref{itm:2013-12-12-2} aus der Quantenfeldtheorie (Spin-Statistik-Theorem). In zwei Dimensionen gibt es Anyonen; dies führt auf den fraktionalen Quanten-Hall-Effekt ($\mathcal{P}_{12} \ket{\psi} = \mathrm{e}^{\mathrm{i} \pi \nu} \ket{\psi}$ mit $\nu \neq 0,1$).

\section{Konsequenzen des Symmetrisierungspostulats}

\subsection{Pauli-Prinzip}

Zwei Fermionen können nicht im selben Zustand sein. Denn $\ket{\psi} = \ket{\alpha} \otimes \ket{\alpha}$ erfüllt $\mathcal{P}_{12} \ket{\psi} = \ket{\psi}$. Die Wellenfunktion wäre somit symmetrisch und müsste ein Boson sein.

\begin{center}
	\tikz[every path/.style={}]{\node[draw=red,thick,rectangle,rounded corners] {Das Pauli-Prinzip ist verantwortlich für die Stabilität der Materie.};}
\end{center}

\subsection{Streuung identischer Teilchen}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\begin{scope}
			\draw[arrow inside={pos=0.3},rounded corners=0.5cm] (0,0) node[dot,label={left:$A$}] {} -- (2,0) -- (4,1);
			\draw[arrow inside={pos=0.3},rounded corners=0.5cm] (4,0) node[dot,label={right:$B$}] {} -- (2,0) -- (0,-1);
			\draw[dotted] (3,0) -- (2,0) -- (3,0.5);
			\draw (3,0) arc (0:30:{1*cos(30)});
			\node[right] at ($(2,0)+(15:1)$) {$\theta$};
		\end{scope}
		\begin{scope}[xshift=6cm]
			\draw[arrow inside={pos=0.3},rounded corners=0.5cm] (0,0) node[dot,label={left:$A$}] {} -- (2,0) -- (0,-1);
			\draw[arrow inside={pos=0.3},rounded corners=0.5cm] (4,0) node[dot,label={right:$B$}] {} -- (2,0) -- (4,1);
		\end{scope}
	\end{tikzpicture}
	\caption{Die beiden Streuprozesse sind nicht zu unterscheiden.}
	\label{fig:2013-12-12-1}
\end{figure}

Für den differentiellen Wirkungsquerschnitt eines Streuprozesses gilt
%
\begin{align*}
	\frac{\diff \sigma}{\diff \Omega}
	&\sim \left| f(\theta) \pm f(\pi - \theta) \right|^2 \\
	&= |f(\theta)|^2 + |f(\pi-\theta)|^2 \pm \underbrace{2 \Re\Bigl( f(\theta) f^*(\pi-\theta) \Bigr)}_\text{Interferenz-, Austauschterm}
\end{align*}
%
Insbesondere
%
\begin{gather*}
	\frac{\diff \sigma}{\diff \Omega}(\theta) \; , \quad \text{symm. um } \theta = \frac{\pi}{2} \\
	\frac{\diff \sigma}{\diff \Omega}\left(\theta = \frac{\pi}{2}\right) = 0 \; , \quad \text{für Fermionen}
\end{gather*}
