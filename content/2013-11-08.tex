% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 08.11.2013

\renewcommand{\printfile}{2013-11-08}

\subsection{Kochen-Specker-Theorem und Kontextualität}

\begin{enumerate}
	\item Gegeben ist ein Satz kommutierender Variablen $A,B,C,\dots$ mit $f(A,B,C,\dots) = 0$.
	\item Die Vorschrift $\tau(A)$ legt die Messwerte fest.
		\[ \tau(A) \leadsto f(\tau(A),\tau(B),\tau(C)) = 0 \]
\end{enumerate}

Das \acct{Kochen-Specker-Theorem} zeigt: Die beiden Sätze sind falsch. Der Beweis erfolgt durch Gegenbeispiel.

\begin{notice}
	Die Einschränkung auf kommutierende Variablen ist entscheidend.
\end{notice}

\begin{example}
	Wir wählen ein Spin-$1/2$-System und die beiden Variablen $A = \sigma_x$ und $B = \sigma_y$.
	Dann liefert die Vorschrift $\tau(\cdot)$:
	\[ \tau(A) = \pm 1 \; , \quad \tau(B) = \pm 1 \]
	also $\tau(A) + \tau(B) = -2,0,2$ während jedoch
	%
	\begin{align*}
		A + B
		&= \sigma_x + \sigma_y \\
		&= \sqrt{2} \sigma_n \\
		\tau(\sqrt{2} \sigma_n) =
		\begin{cases}
			\sqrt{2} \\
			- \sqrt{2}
		\end{cases}
	\end{align*}

	(Dieser Widerspruch ist auf einen Fehler in von Neumanns Beweis zurückzuführen)
\end{example}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\foreach \i in {18,90,...,360} {
			\draw (\i:2) node[dot] (n\i) {} -- (\i-144:2);
		}
		\draw[DarkOrange3] (18:2) -- node[below] {$H$} (162:2);
		\foreach \i in {18,90,...,360} {
			\node[MidnightBlue,dot] (i\i) at (-\i:0.75) {};
		}
		\node[right] at (n18) {$\sigma_x^1 \sigma_y^2 \sigma_y^3$};
		\node[above] at (n90) {$\sigma_y^2$};
		\node[left] at (n162) {$\sigma_x^1 \sigma_x^2 \sigma_x^3$};
		\node[below] at (n234) {$\sigma_y^2$};
		\node[below] at (n306) {$\sigma_x^2$};
		\node[MidnightBlue,right=1ex] at (i18) {$\sigma_y^3$};
		\node[MidnightBlue,below=1ex] at (i90) {$\sigma_x^1$};
		\node[MidnightBlue,left=1ex] at (i162) {$\sigma_x^3$};
		\node[MidnightBlue,above left] at (i234) {$\sigma_y^1 \sigma_y^2 \sigma_x^3$};
		\node[MidnightBlue,above right] at (i306) {$\sigma_y^1 \sigma_x^2 \sigma_y^3$};
	\end{tikzpicture}
	\caption{Skizze zum Kochen-Specker-Theorem}
	\label{fig:2013-11-08-1}
\end{figure}

\begin{enumerate}
	\item Die vier Observablen auf jeder Linie vertauschen unter sich. Dies ist trivial bis auf die $H$-Linie.
	\item Das Produkt entlang jeder Linie, bis auf der eingefärbten, ist $+1$. Auf der $H$-Linie ist es $-1$.
	\item Dann gilt das auch für die $\tau(\cdot)$-Vorschirften.
	\item Das Produkt der $\tau$ über alle fünf Linien ist $-1$. Im Produkt taucht aber jede Observable genau zweimal auf. Das Ergebnis ist also $+1$.
\end{enumerate}
Siehe dazu auch \fullcite{RevModPhys.65.803}.

\subsection{Bohmsche Theorie}

Die \acct{Bohmsche Theorie} handelt von nichtlokalen verborgenen Variablen. Sie ist detailliert dargelegt in \fullcite{PhysRev.85.166}.

\begin{theorem}[Theorie]
	Wir betrachten ein System von $N$-Teilchen.
	Es sei gegeben
	%
	\begin{enumerate}
		\item $\psi(\bm{r}_1,\dotsc,\bm{r}_N,t)$ mit dem Anfangswert $\psi(\cdot,t=0)$.
		\item Die $N$ Teilchen besitzen die Trajektorien $\bm{Q}_1(t),\dotsc,\bm{Q}_N(t)$ mit den Anfangswerten $\bm{Q}_{1,\dotsc,N}(t=0)$.
	\end{enumerate}

	Die Dynamik des Systems wird beschrieben durch die beiden Gleichungen
	%
	\begin{enumerate}
		\item 
			\begin{itemalign}
				\mathrm{i}\hbar\partial_t \psi(\bm{r}_1,\dotsc,\bm{r}_N,t)
				&= H \psi(\bm{r}_1,\dotsc,\bm{r}_N,t) \\
				&= \left\{ \sum_{i=1}^{N} \frac{\hbar^2}{2 m} \nabla_i^2 + \sum_{i<j} V_{ij}(\bm{r}_i - \bm{r}_j) \right\} \psi(\bm{r}_1,\dotsc,\bm{r}_N,t)
			\end{itemalign}

		\item
			\begin{itemalign}
				\frac{\diff \bm{Q}_k}{\diff t} = \frac{\hbar}{m} \Im \frac{\psi^*(\bm{r}_1,\dotsc,\bm{r}_N,t) \nabla \psi(\bm{r}_1,\dotsc,\bm{r}_N,t)}{|\psi(\bm{r}_1,\dotsc,\bm{r}_N,t)|^2}
			\end{itemalign}
	\end{enumerate}
\end{theorem}

Die Anfangsorte der Teilchen werden mit der Wahrscheinlichkeit
%
\begin{align*}
	\varrho(\bm{Q}_1,\dotsc,\bm{Q}_N,t=0) = |\psi(\bm{Q}_1,\dotsc,\bm{Q}_N,0)|^2
\end{align*}
%
bestimmt. Es gilt für $\varrho(\bm{Q}_1,\dotsc,\bm{Q}_N,t) = |\psi(\cdot,t)|^2$
%
\begin{align*}
	\frac{\partial \varrho}{\partial t} &= \frac{\partial |\psi|^2}{\partial t} = - \nabla_\bm{k} \bm{j}_k
\intertext{mit}
	\bm{j} &= \frac{\hbar}{m} \Im \left[ \psi^*(\bm{Q}_1,\dotsc,\bm{Q}_N,t) \nabla \psi(\bm{Q}_1,\dotsc,\bm{Q}_N,t) \right]
\end{align*}
%
Diese Theorie ist hochgradig nichtlokal.

\section{Zeitabhängige Störungstheorie}

\subsection{Heisenberg-Bild}\label{sec:Heisenberg-Bild}

Bisher waren Zustände zeitabhängig und Operatoren im allgemeinen zeitunabhängig. Diese Darstellung der Quanten-Mechanik nennt man \acct{Schrödinger-Bild}.

Alternativ kann man zur Berechnung von Erwartungswerten auch in das \acct{Heisenberg-Bild} übergehen. 

Seien der Zustand $\Ket{\psi}$, als auch der Operator $A$ gegeben:
%
\begin{align*}
	\Braket{A}_{\psi} &= \Braket{\psi(t)|A|\psi(t)} \\
	&= \Bigg\langle \psi(t_0) \Bigg| \underbrace{\exp\left(\frac{\mathrm{i}}{\hbar}H(t-t_0)\right) A \exp\left(-\frac{\mathrm{i}}{\hbar}H(t-t_0)\right)}_{\coloneq A_H(t)} \Bigg| \psi(t_0) \Bigg\rangle \\
	&= \Braket{\psi_H | A_H | \psi_H}
\end{align*}
%
Mit 
%
\begin{align*}
	\psi_H &= \Ket{\psi(t_0)} \\
	A_H &= \exp\left(\frac{\mathrm{i}}{\hbar}H(t-t_0)\right) \, A \, \exp\left(-\frac{\mathrm{i}}{\hbar}H(t-t_0)\right)
\end{align*}
%
Dabei sind hier nun die Operatoren zeitabhängig und die Zustände zeitunabhängig. Der Übergang vom Schrödinger-Bild in das Heisenberg-Bild erfolgt durch eine unitäre Transformation.
%
\begin{align*}
	\Ket{\psi} &= \underbrace{\exp\left(\frac{\mathrm{i}}{\hbar} H (t-t_0)\right)}_{U^\dagger(t,t_0)} \Ket{\psi(t)} = \Ket{\psi(0)} \\
	A_H &= U^\dagger (t,t_0) \, A \, U (t,t_0) \\
	\implies \mathrm{i} \hbar \frac{\diff}{\diff t} A_H &= [A_H, H_H] + \mathrm{i} \hbar \underbrace{\left(\frac{\partial A}{\partial_t} \right)_H}_{\mathclap{\text{falls $A_S = A_S(t)$}}}
\end{align*}
%
Dabei steht $A_S$ für den Operator $A$ im Schrödinger-Bild, der explizit zeitabhängig ist.

\begin{example}
	Der Hamiltonoperator des harmonischen Oszillators lautet
	\[ H = \hbar \omega \left( a^\dag a + \frac{1}{2} \right) \]
	Im Heisenbergbild lautet der Absteigeoperator
	%
	\begin{align*}
		a_H(t) &= \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H t} \, a \, \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H t} \\
	\intertext{mit}
		\frac{\diff}{\diff t} a_H(t) &= \frac{\mathrm{i}}{\hbar} [ H_H, a_H ] = \frac{\mathrm{i}}{\hbar} (- \hbar \omega a_H) = - \mathrm{i} \omega a_H(t) \\
		\implies a_H(t) &= \mathrm{e}^{-\mathrm{i} \omega t} a
	\end{align*}
	%
	Ebenso für den Aufsteigeoperator
	%
	\begin{align*}
		a_H^\dag(t) &= \mathrm{e}^{\mathrm{i} \omega t} a^\dag
	\end{align*}
\end{example}

\subsection{Dirac- oder Wechselwirkungsbild}
\index{Dirac-Bild}
\index{Wechselwirkungsbild}

Sei nun der Hamiltonoperator zeitabhängig in der Form, dass
\[ H(t) = H_0 + V(t) \]
Im Schrödingerbild gilt dann die Zeitevolution des Systems durch die Schrödingergleichung
%
\begin{align*}
	\mathrm{i} \hbar \partial_t \ket{\psi(t)} &= (H_0 + V(t)) \ket{\psi(t)}
\intertext{mit der formalen Lösung}
	\ket{\psi(t)} &= U(t,t_0) \ket{\psi(0)}
\end{align*}

Definiere nun
%
\begin{align*}
	\ket{\psi_I(t)} &= \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \ket{\psi(t)}
\intertext{mit}
	\frac{\diff}{\diff t} \ket{\psi_I(t)}
	&= \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \left( \frac{1}{\mathrm{i}\hbar} (H_0 + V) \right) \ket{\psi(t)} + \frac{\mathrm{i}}{\hbar} H_0 \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \ket{\psi(t)} \\
	&= - \frac{\mathrm{i}}{\hbar} \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \, V(t) \ket{\psi(t)} \\
	&= - \frac{\mathrm{i}}{\hbar} \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \, V(t) \, \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H_0 t} \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \ket{\psi(t)} \\
	&= - \frac{\mathrm{i}}{\hbar} \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \, V(t) \, \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H_0 t} \ket{\psi_I(t)} \\
	&= - \frac{\mathrm{i}}{\hbar} V_I(t) \ket{\psi_I(t)}
\end{align*}
%
mit der Störung im Wechselwirkungsbild
%
\begin{align*}
	\boxed{
		V_I(t) \equiv \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \, V(t) \, \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H_0 t}
	}
\end{align*}

\minisec{Erwartungswerte}

Im Schrödingerbild gilt für den Erwartungswert
%
\begin{align*}
	\Braket{A(t)}_{\ket{\psi(t)}}
	&= \Braket{\psi(t)|A(t)|\psi(t)}
\intertext{unter Verwendung der Wechselwirkungsbildes}
	&= \Braket{\psi_I(t)| \mathrm{e}^{\frac{\mathrm{i}}{\hbar} H_0 t} \, A(t) \, \mathrm{e}^{-\frac{\mathrm{i}}{\hbar} H_0 t} |\psi_I(t)} \\
	&= \Braket{\psi_I(t)| A(t) |\psi_I(t)} 
\end{align*}

Die Zeitentwicklung des Erwartungswerts verläuft also nach der Gleichung
%
\begin{align*}
	\frac{\diff A_I}{\diff t} &= \frac{\mathrm{i}}{\hbar} [H_0,A_I(t)] + \left( \frac{\partial A}{\partial t} \right)_I
\end{align*}

\begin{notice}[Zusammenfassung:]
	Im Wechselwirkungsbild wird die Dynamik der Zustände mit $V(t)$, die der Operatoren nach $H_0$ propagiert.
\end{notice}

\subsection{Zeitentwicklungsoperator, Dyson-Reihe und Feynman-Diagramme}

Wir definieren den Zeitentwicklungsoperator $U_I(t,t_0)$ so, dass
%
\begin{align*}
	\ket{\psi_I(t)} &= U_I(t,t_0) \ket{\psi_I(t_0)} \\
	\frac{\diff}{\diff t} \ket{\psi_I(t)} &= - \frac{\mathrm{i}}{\hbar} V_I(t) \ket{\psi_I(t)} \overset{!}{{}={}} \frac{\diff}{\diff t} U_I(t,t_0) \ket{\psi_I(t_0)}
\end{align*}
%
Folglich gilt für $U_I(t,t_0)$
%
\begin{align*}
	\frac{\diff}{\diff t} U_I(t,t_0) = - \frac{\mathrm{i}}{\hbar} V_I(t) \, U_I(t,t_0)
\end{align*}
%
Eigentlich bleibt nur noch diese Differentialgleichung zu lösen, was im Allgemeinen jedoch nicht möglich ist. Wir integrieren also beide Seiten und addieren eins.
%
\begin{gather*}
	\begin{aligned}
		1 + \int_{t_0}^{t} \frac{\diff}{\diff t'} U_I(t',t_0) \diff t'
		&= 1 - \frac{\mathrm{i}}{\hbar} \int_{t_0}^{t} V_I(t') \, U_I(t',t_0) \diff t' \\
		1 + U_I(t,t_0) - U_I(t_0,t_0)
		&= 1 - \frac{\mathrm{i}}{\hbar} \int_{t_0}^{t} V_I(t') \, U_I(t',t_0) \diff t' \\
	\end{aligned} \\
	\begin{aligned}
		&= 1 - \frac{\mathrm{i}}{\hbar} \int_{t_0}^{t} \diff t' V_I(t') \left( 
			1 - \frac{\mathrm{i}}{\hbar} \int_{t_0}^{t} \diff t'' V_I(t'') \left(
				1 - \frac{\mathrm{i}}{\hbar} \int_{t_0}^{t} \diff t''' V_I(t''') \bigg( \cdots \bigg)
			\right)
		\right) \\
		\Aboxed{
			U_I(t,t_0)
			&= 1 + \sum_{n=1}^{\infty} \left( - \frac{\mathrm{i}}{\hbar} \right)^n \int_{t_0}^{t} \diff t_1 \dots \int_{t_0}^{t_{n-1}} \diff t_n \, V_I(t_1) \dots V_I(t_n)
		}
	\end{aligned}
\end{gather*}
%
Dies ist die \acct{Dyson-Reihe}.
