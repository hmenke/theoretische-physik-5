% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 29.11.2013

\renewcommand{\printfile}{2013-11-29}

\section{Zustände des Strahlungsfeldes}

\subsection{Der elektrische Feldoperator}

Aus der klassischen Elektrodynamik ist bekannt
%
\begin{align*}
	\bm{E}(\bm{r},t)
	&= - \frac{1}{c} \partial_t \bm{A}(\bm{r},t)
\intertext{mit der quantiserten Form von $\bm{A}$}
	&= - \mathrm{i} \sum_{\bm{k}} \sum_{\alpha} \sqrt{\frac{2 \pi \hbar \omega_k}{V}} \bm{\varepsilon}_\alpha \left( a_{\bm{k},\alpha}(t)\ \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} - a_{\bm{k},\alpha}^\dag(t)\ \mathrm{e}^{- \mathrm{i} \bm{k} \cdot \bm{r}} \right)
\end{align*}

Entsprechend erhält man $\bm{B}$ via
\[ \bm{B} = \nabla \times \bm{A} \]

\subsection{Fock-Zustände}

Wie bereits erwähnt sind die Fock-Zustände gegeben durch
%
\begin{gather*}
	\ket{\{n\}} = \ket{n_{\bm{k}_1,\alpha_1},n_{\bm{k}_2,\alpha_2},\dotsc}
\intertext{mit}
	\braket{\{n\}|\bm{E}(\bm{r},t)|\{n\}} = 0
\end{gather*}
%
Klassisch zeigt sich jedoch, dass der Erwartungswert des elektrischen Feldes unmöglich null sein kann, da man aus der Realität weiß, dass man elektrische Felder durchaus messen kann.

\subsection{Glauber-Zustände}
\index{Glauber-Zustände}

Wir wollen nun Zustände konstruieren, die der klassischen Erwartung $\braket{\cdot|\bm{E}(\bm{r},t)|\cdot} \neq 0$ entsprechen.

\begin{notice}[Erinnerung:]
	Bevor wir mit der Konstruktion beginnen, eine kurze Erinnerung an die \acct{Baker-Campbell-Hausdorff-Formel}:
	%
	\begin{align*}
		\mathrm{e}^{A+B} = \mathrm{e}^A \mathrm{e}^B \mathrm{e}^{-\frac{1}{2} [A,B]} \tag{BCH} \label{eq:2013-11-29-1}
	\end{align*}
	%
	falls $[ [A,B], B ] = [ [A,B], A ] = 0$.
\end{notice}

\begin{theorem}[Definition]
	Wir definieren den unitären Verschiebungsoperator.
	%
	\begin{align*}
		\mathcal{D}
		&\equiv \mathrm{e}^{\alpha a^\dag - \alpha^* a} \\
		&\stackrel{\eqref{eq:2013-11-29-1}}{=} \mathrm{e}^{\alpha a^\dag} \mathrm{e}^{\alpha^* a} \mathrm{e}^{-\frac{1}{2}\alpha\alpha^*}
	\end{align*}
	%
	Es gilt
	%
	\begin{align*}
		\mathcal{D}^\dag(\alpha) = \mathcal{D}^{-1}(\alpha) &= \mathcal{D}(-\alpha) \\
		\mathcal{D}^\dag(\alpha) a \mathcal{D}(\alpha) &= a + \alpha \\
		\mathcal{D}(\alpha) a^\dag \mathcal{D}^\dag(\alpha) &= a^\dag + \alpha
	\end{align*}
\end{theorem}

\begin{theorem}[Glauber-Zustand]
	Der \acct{Glauber-Zustand} ist gegeben durch
	%
	\begin{align*}
		\ket{\alpha} \equiv \mathcal{D}(\alpha) \ket{0}
	\end{align*}
	%
	Der Glauber-Zustand $\ket{\alpha}$ ist Eigenzustand zum Vernichtungsoperator, also
	%
	\begin{align*}
		a \ket{\alpha} &= \alpha \ket{\alpha}
	\end{align*}
\end{theorem}

\begin{proof}
	\begin{align*}
		a \ket{\alpha}
		&= a \mathcal{D}(\alpha) \ket{0} \\
		&= \mathcal{D}(\alpha) \mathcal{D}^\dag(\alpha) a \mathcal{D}(\alpha) \ket{0} \\
		&= \mathcal{D}(\alpha) (a + \alpha) \ket{0} \\
		&= \alpha \ket{\alpha}
	\end{align*}
\end{proof}

Wir drücken $\ket{\alpha}$ in der Basis $\{\ket{n}\}$ aus:
\begin{align*}
	a \ket{\alpha} &= \alpha \ket{\alpha} \\
	\braket{n|a|\alpha} &= \braket{n|\alpha|\alpha} \\
	\sqrt{n+1} \braket{n+1|\alpha} &= \alpha \braket{n|\alpha} \\
	\braket{n|\alpha} &= \frac{\alpha^n}{\sqrt{n!}} \braket{0|\alpha} \\
	\ket{\alpha} &= \sum_{n} \braket{n|\alpha} \ket{n} = \sum_{n} \frac{\alpha^n}{\sqrt{n!}} \braket{0|\alpha} \ket{n}
\end{align*}
%
Betrachten wir nun die Norm von $\ket{\alpha}$.
%
\begin{align*}
	\braket{\alpha|\alpha} &= \sum_{n} \frac{(\alpha\alpha^*)^n}{n!} |\braket{n|\alpha}|^2 \\
	&= \mathrm{e}^{|\alpha|^2} |\braket{0|\alpha}|^2 = 1
\end{align*}
%
wobei $\braket{0|\alpha} = \mathrm{e}^{-|\alpha|^2/2}$. Es zeigt sich also, dass $\ket{\alpha}$ normiert ist.

Die $\ket{\alpha}$ sind jedoch nicht orthogonal
%
\begin{align*}
	\braket{\alpha|\beta}
	&= \Braket{0 \mathcal{D}^\dag(\alpha)|\mathcal{D}(\beta) 0} \\
	&= \mathrm{e}^{-|\alpha|^2/2} \mathrm{e}^{-|\beta|^2/2} \Braket{0\ \mathrm{e}^{-\alpha a^\dag} \mathrm{e}^{\alpha^* a} | \mathrm{e}^{\beta a^\dag} \mathrm{e}^{-\beta^* a}\ 0} \\
	&= \mathrm{e}^{-|\alpha|^2/2} \mathrm{e}^{-|\beta|^2/2} \underbrace{\Braket{0\ \mathrm{e}^{\alpha^* a} | \mathrm{e}^{\beta a^\dag}\ 0}}_{\mathrm{e}^{\alpha^*\beta}} \\
	&= \mathrm{e}^{-|\alpha-\beta|^2}
\end{align*}

\begin{notice}[Nebenbemerkungen:]
	\begin{itemize}
		\item 
			\begin{itemalign}
				\int \diff \alpha \ket{\alpha}\bra{\alpha} = \pi\ \mathds{1}
			\end{itemalign}
		\item
			\begin{itemalign}
				P(n|\alpha) = |\braket{n|\alpha}|^2 = \frac{|\alpha|^{2n} \mathrm{e}^{-|\alpha|^2}}{n!}
			\end{itemalign}
	\end{itemize}
\end{notice}

Wir betrachten nun den Erwartungswert des elektrischen Feldes im Glauber-Zustand
%
\begin{align*}
	\braket{\alpha|E|\alpha}
	&\stackrel{t=0}{=} - \mathrm{i} \sqrt{\frac{2\pi\hbar\omega}{V}} \bm{\varepsilon}_\alpha \Braket{\alpha | a\ \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} - a^\dag\ \mathrm{e}^{-\mathrm{i} \bm{k} \cdot \bm{r}} | \alpha} \\
	&= 2 |\alpha| \sqrt{\frac{2\pi\hbar\omega}{V}} \bm{\varepsilon}_\alpha \sin(\bm{k} \cdot \bm{r} + \phi_\alpha)
\end{align*}
%
mit $\alpha = |\alpha| \mathrm{e}^{\mathrm{i}\phi_\alpha}$. Hätten wir die Zeitabhängigkeit nicht vernachlässigt, dann hätte man das typische oszillierende Verhalten gefunden.

\subsection{Thermisches Photonengas}

\begin{notice}[Erinnerung:]
	In einem eindimensionalen harmonischen Oszillator ist die Dichtematrix im Gleichgewicht gegeben durch
	%
	\begin{align*}
		\varrho^\textnormal{eq} = \frac{1}{Z(\beta)} \mathrm{e}^{-\beta(H-F)} = \frac{1}{Z(\beta)} \sum_n \mathrm{e}^{-\beta\hbar\omega\left( n+\frac{1}{2} \right)} \ket{n}\bra{n}
	\end{align*}
\end{notice}

Wenden wir dies an auf Photonen, so müssen wir über alle Moden summieren.
%
\begin{align*}
	\varrho^\textnormal{eq} &= \frac{1}{Z} \sum_{\{n_{\bm{k},\alpha}\}} \mathrm{e}^{-\beta\hbar\omega_k\left( n_{\bm{k},\alpha}+\frac{1}{2} \right)} \ket{\{n\}}\bra{\{n\}} \\
	\braket{n_{\bm{q},\beta}} &= \frac{1}{Z} \sum_{n_{\bm{q},\alpha}} n_{\bm{q},\beta} \mathrm{e}^{-\beta\hbar\omega_q\left( n_{\bm{q},\alpha}+\frac{1}{2} \right)} \\
	&= \frac{1}{\mathrm{e}^{-\beta\hbar\omega_q} - 1} \\
	&\approx
	\begin{cases}
		\frac{\kB T}{\hbar \omega} & \kB T \gg \hbar \omega \\
		\mathrm{e}^{-\beta\hbar\omega} & \kB T \ll \hbar \omega
	\end{cases}
\end{align*}

Mittlere Energie dieser Mode
%
\begin{align*}
	\braket{E_{\bm{q},\beta}}
	&= \hbar \omega_q \left( \braket{n_{\bm{q},\beta}} + \frac{1}{2} \right) \\
	&= \bar{E}_{\bm{q},\beta} + \frac{\hbar\omega_q}{2}
\end{align*}
%
Die mittlere Gesamtenergie im Strahlungsfeld ist also
%
\begin{align*}
	\braket{\bar{E}}_\textnormal{eq} &= 2 V \int \frac{\diff^3 q}{(2 \pi)^3} \frac{\hbar \omega_q}{\mathrm{e}^{-\beta\hbar\omega_q} - 1} = \frac{\pi^2}{15} \frac{(\kB T)^4}{(\hbar c)^3} V && \text{(Stefan-Boltzmann-Gesetz)}
\end{align*}

\subsection{Nullpunktsenergie und Casimir-Effekt}

Im Hamiltonoperator des elektrischen Feldes steht $\dots + \frac{1}{2} \hbar \omega_q$. Nun stellt sich die Frage, ob dieser Term ernst zu nehmen ist oder vernachlässigt werden kann.
Man kann dies meistens tun, aber \ldots

\begin{figure}[htpb]
	\centering
	\tdplotsetmaincoords{75}{140}
	\begin{tikzpicture}[tdplot_main_coords]
		\draw[dashed] (0,0,0) -- (3,0,0) (0,3,0) -- (0,0,0) -- (0,0,3); 
		\draw (3,0,0) -- node[below] {$L$} (3,3,0) (3,3,0) -- (0,3,0);
		\draw (0,0,3) -- (3,0,3) -- (3,3,3) -- (0,3,3) -- cycle;
		\draw (3,0,0) -- (3,0,3) (3,3,0) -- (3,3,3) (0,3,0) -- (0,3,3);
		\draw[Purple,->] (2,1.5,3) -- (1,1.5,3) node[right] {$t$};A
		\node at (0,1.5,1.5) {$S$};
		\tdplotsetrotatedcoords{180}{90}{90}
		\coordinate (Shift) at (3,3,1);
		\tdplotsetrotatedcoordsorigin{(Shift)}
		\draw[DarkOrange3,tdplot_rotated_coords] plot[smooth,samples=61,domain=0:3] (\x,{0.5*sin(2/3*pi*deg(\x))});
		\draw[MidnightBlue,tdplot_rotated_coords] plot[smooth,samples=61,domain=0:3] (\x,{0.5*sin(pi/3*deg(\x)) + 1});
	\end{tikzpicture}
	\caption{Hohlraumresonator.}
	\label{fig:2013-11-29-1}
\end{figure}

Wir berechnen für $V = L^3$ mit leitenden Flächen und Rändern (Abb.~\ref{fig:2013-11-29-1})
%
\begin{align*}
	\bm{E} \cdot \bm{t} |_S  = \bm{A}|_S \stackrel{!}{=} 0
\end{align*}
%
Da die Box endlich ist, erhalten wir ein diskretes Spektrum mit
%
\begin{align*}
	\bm{k} = (k_x,k_y,k_z) = \frac{\pi}{L} (n_x,n_y,n_z) \; , \quad n_i \geq 1
\end{align*}
%
Ob mit oder ohne periodische Randbedingungen hat keine Auswirkung, da die Welle am Rand in jedem Fall null sein muss.

Betrachte nun das elektrische Feld, wobei wir die Moden nach oben hin bei $k_c$ abschneiden.
%
\begin{align*}
	E_0
	&= \frac{1}{2} \hbar c \sum_{\bm{k},\alpha} |k| \\
	&= \frac{1}{2} \hbar c 2 \frac{V}{(2 \pi)^3} \int \diff k\ 4 \pi k^2 |k| \\
	&= \frac{V}{8 \pi^3} \hbar c k_c
\end{align*}

\minisec{Messbarer Effekt}

Wir bringen in die Box vom Anfang eine weitere leitende Platte ein (Abb.~\ref{fig:2013-11-29-2}).
%
\begin{figure}[htpb]
	\centering
	\tdplotsetmaincoords{75}{140}
	\begin{tikzpicture}[tdplot_main_coords]
		\draw[dashed] (0,0,0) -- (3,0,0) (0,3,0) -- (0,0,0) -- (0,0,3); 
		\draw (3,0,0) -- node[below] {$L$} (3,3,0) (3,3,0) -- (0,3,0);
		\draw (0,0,3) -- (3,0,3) -- (3,3,3) -- (0,3,3) -- cycle;
		\draw (3,0,0) -- (3,0,3) (3,3,0) -- (3,3,3) (0,3,0) -- (0,3,3);
		\draw[DarkOrange3] (3,1,0) -- (3,1,3) -- (0,1,3);
		\draw[dashed,DarkOrange3] (0,1,3) -- (0,1,0) -- (3,1,0);
		\draw[<->] (3,0,1.5) -- node[above] {$R$} (3,1,1.5);
	\end{tikzpicture}
	\caption{Hohlraumresonator mit leitender Platte.}
	\label{fig:2013-11-29-2}
\end{figure}
%
\begin{align*}
	W(R) \equiv E_0(R) + E_0(L-R) - E_0(L)
\end{align*}
%
mit
%
\begin{align*}
	E_0(L) &= \hbar c \frac{L^3}{\pi^2} \iiint \diff^3 k\ \sqrt{k_x^2 + k_y^2 + k_z^2} f\left( \frac{|k|}{k_c} \right) \\
	E_0(L-R) &= \hbar c \frac{L^2(L-R)}{\pi^3} \iiint \diff^3 k\ \sqrt{k_x^2 + k_y^2 + k_z^2} f\left( \frac{|k|}{k_c} \right) \\
	E_0(R) &= \hbar c \frac{L^2}{\pi^2} \int_{0}^{\infty}\diff k_y \int_{0}^{\infty}\diff k_z\ \sum_{n_x=0}^{\infty} \sqrt{\frac{\pi}{L} n_x^2 + k_y^2 + k_z^2} f\left( \frac{|k|}{k_c} \right) \theta_{n_x}
\end{align*}
%
mit $\theta_{n_x} = 1$ für $n_x \geq 1$ und $\theta_{n_x} = 1/2$ für $n_x = 0$.
Wir setzen diese ganzen Terme ein
%
\begin{align*}
	W(R) &= \frac{\hbar c L^2}{\pi^2} \sum_{n=0}^{\infty} \theta_n g\left( \frac{n \pi}{R} \right) - \frac{R}{\pi} \int_{0}^{\infty} g(k_x) \diff k_x
\end{align*}
%
dabei gilt
%
\begin{align*}
	\omega &= n^2 + \frac{R^2}{\pi^2}(k_y^2 + k_z^2) \\
	g(k_x) &= \int_{0}^{\infty}\diff k_y \int_{0}^{\infty}\diff k_z \sqrt{k_x^2 + k_y^2 + k_z^2} f\left( \frac{|k|}{k_c} \right) = \frac{\pi^4}{4 R^3} F(n) \\
	F(n) &= \int_{0}^{\infty} \sqrt{\omega} f\left( \frac{\pi \sqrt{\omega}}{R k_c} \right) \diff \omega
\end{align*}
%
damit
%
\begin{align*}
	W(R) = \frac{\hbar c L^2 \pi^4}{4 \pi^2 R^3} \underbrace{\left\{ \sum_{n=0}^{\infty} \theta_n F(n) - \int_{0}^{\infty} F(n) \diff n \right\}}_{-\frac{1}{12} F'(0) + \frac{1}{720} F'''(0)+\dots}
\end{align*}
%
mit $F'''(0) = -4$.
%
\begin{align*}
	W(R) = - \hbar c \frac{\pi^2}{720} \frac{L^2}{R^3}
\end{align*}

Damit ist die Casimir-Kraft
%
\begin{align*}
	F = - \frac{\partial W}{\partial R} = - \frac{\hbar c \pi^2}{240} \frac{L^2}{R^4}
\end{align*}

Für ausführliche Erklärungen und Messung dieses Phänomens siehe: \fullcite{PhysRevLett.81.4549}.
