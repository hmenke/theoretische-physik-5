% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 28.11.2013

\renewcommand{\printfile}{2013-11-28}

\chapter{Wechselwirkung von Strahlung und Materie}

\section{Quantisierung des Strahlungsfeldes}

\subsection{Klassisches Strahlungsfeld}

Die Maxwellschen Gleichungen ohne äußere Ladungen und Ströme lauten
%
\begin{align}
	\nabla \cdot \bm{E} &= 0 \label{eq:2013-11-28-1} \\
	\nabla \times \bm{B} - \frac{1}{c} \partial_t \bm{E} &= 0 \label{eq:2013-11-28-2} \\
	\nabla \cdot \bm{B} &= 0 \label{eq:2013-11-28-3} \\
	\nabla \times \bm{E} + \frac{1}{c} \partial_t \bm{B} &= 0 \label{eq:2013-11-28-4}
\end{align}

Die Felder können ausgedrückt werden durch ein Vektorpotential $\bm{A}$ und ein skalares Potential $\phi$ mit
%
\begin{align*}
	\bm{B} &= \nabla \times \bm{A} \\
	\bm{E} &= - \frac{1}{c} \partial_t \bm{A} - \nabla \phi
\end{align*}

\paragraph{Coulombeichung:} Wir verwenden die Eichtransformation
%
\begin{align*}
	\bm{A} &\to \tilde{\bm{A}} \equiv \bm{A} + \nabla \Lambda(\bm{r},t) \\
	\phi &\to \tilde{\phi} \equiv \phi - \frac{1}{c} \partial_t \Lambda
\end{align*}
%
Die Coulombeichung sagt nun aus, dass $\Lambda(\bm{r},t)$ so gewählt werden kann, dass $\nabla \cdot \tilde{\bm{A}} = 0$.
Sie wird auch \acct*{transversale Eichung} oder \acct*{Strahlungseichung} genannt.

Mit der Coulombeichung sind \eqref{eq:2013-11-28-3} und \eqref{eq:2013-11-28-4} identisch erfüllt. Aus \eqref{eq:2013-11-28-1} finden wir
\[ - \nabla^2 \phi = 0 \]
daraus folgt, dass $\phi(\bm{r},t) = 0$, weil $\phi(\bm{r},t)$ für $\bm{r} \to \infty$ auf null abfallen muss.

Aus \eqref{eq:2013-11-28-2} folgt
%
\begin{align*}
	\nabla \times \left( \nabla \times \bm{A} \right) + \frac{1}{c^2} \partial_t^2 \bm{A} &= 0 \\
	- \nabla^2 \bm{A} + \frac{1}{c^2} \partial_t \bm{A} &= 0
\end{align*}
%
Dies ist eine Wellengleichung für $\bm{A}$.

\subsection{Einschub: Lösung der Wellengleichung in einer Dimension}

Wir betrachten die Wellengleichung
%
\begin{align*}
	- \partial_x^2 f(x,t) + \frac{1}{c^2} \partial_t^2 f(x,t) &= 0
\end{align*}

%\begin{figure}[htpb]
%	\centering
%	\begin{tikzpicture}
%		\draw (-0.5,0) -- (4.5,0);
%		\draw (0,0.5) -- (0,-0.5) node[below] {$0$};
%		\draw (4,0.5) -- (4,-0.5) node[below] {$L$};
%		\draw[MidnightBlue] plot[smooth,domain=0:4] (\x,{random()-0.5});
%		\node[MidnightBlue,above] at (2,0.5) {$f(x,t_1)$};
%		\draw[DarkOrange3] plot[smooth,domain=0:4] (\x,{random()-0.5});
%		\node[DarkOrange3,below] at (2,-0.5) {$f(x,t_2)$};
%	\end{tikzpicture}
%	\caption{Missing caption.}
%	\todo[inline]{Missing caption.}
%	\label{fig:2013-11-28-1}
%\end{figure}

Als Ansatz wählen wir eine Potenzreihe
%
\begin{align*}
	f(x,t) &= \sum_{k} a_k(t)\ \mathrm{e}^{\mathrm{i} k x}
\end{align*}
%
\begin{itemize}
	\item Außerdem seien die Randbedingungen periodisch
		\begin{align*}
			f(0,t) = \sum_{k} a_k(t) &= f(L,t) = \sum_{k} a_k(t)\ \mathrm{e}^{\mathrm{i} k L} \\
			k &= \frac{2 \pi}{L} z \; , \quad (z \in \mathbb{Z})
		\end{align*}

	\item $f(x,t)$ soll reell sein.
		\begin{align*}
			f^*(x,t)
			&= \sum_k a_k^*(t)\ \mathrm{e}^{-\mathrm{i} k x} \\
			&= f(x,t) = \sum_k a_k(t)\ \mathrm{e}^{\mathrm{i} k x} \\
			&= \sum_k a_{-\tilde{k}}(t)\ \mathrm{e}^{-\mathrm{i} \tilde{k} x} \\
			a_k^*(t) &= a_{-k}(t)
		\end{align*}
\end{itemize}
%
Es folgt, dass $a_0$ reell sein muss. Die restlichen $a_k \in \mathbb{C}$ mit $k \in \mathbb{N}^+$ sind weiterhin frei wählbar.

Setzen wir den Ansatz in die Wellengleichung ein
%
\begin{align*}
	\sum_k k^2 a_k(t)\ \mathrm{e}^{\mathrm{i} k x} + \frac{1}{c^2} \sum_k \partial_t^2 a_k(t)\ \mathrm{e}^{\mathrm{i} k x} = 0
\end{align*}
%
Damit die Gleichung erfüllt ist, reicht es die Entwicklungskoeffizienten zu betrachten.
%
\begin{align*}
	\partial_t^2 a_k(t) &= - k^2 c^2 a_k(t)
\end{align*}

\begin{notice}[Analogie:]
	Der harmonische Oszillator aus der klassischen Mechanik lautet
	%
	\begin{align*}
		m \ddot{x} &= - K x \\
		\ddot{x} &= - \frac{K}{m} x
	\end{align*}
\end{notice}

Jede komplexe Amplitude verhält sich wie ein klassischer harmonischer Oszillator.

\subsection{Allgemeine Lösung für das Vektorpotential}\index{Vektorpotential}

Nun müssen wir das Volumen $V = L^3$ betrachten.
%
\begin{align*}
	\bm{A}(\bm{r},t) &=
	\sum_{\bm{k}|_{k_z > 0}} \sum_{\alpha=1,2}
	\sqrt{\frac{2 \pi \hbar c^2}{V \omega_\bm{k}}}
	\left[ a_{\bm{k},\alpha}(t)\ \bm{u}_{\bm{k},\alpha}(\bm{r}) + a_{\bm{k},\alpha}^*(t)\ \bm{u}_{\bm{k},\alpha}^*(\bm{r}) \right]
\end{align*}
%
mit $u_{\bm{k},\alpha} = \bm{\varepsilon}^{(\alpha)} \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}}$. Dabei sind $\bm{\varepsilon}^{(1)}$ und $\bm{\varepsilon}^{(2)}$ die Polarisationsvektoren und $(\bm{\varepsilon}^{(1)},\bm{\varepsilon}^{(2)},\bm{k})$ bildet ein rechtshändiges Dreibein.

Durch Einsetzen in die Wellengleichung ergibt sich
%
\begin{align*}
	\partial_t^2 a_{\bm{k},\alpha}(t) &= - \omega^2 a_{\bm{k},\alpha}(t) \; , \quad \text{mit } \omega_k = c |\bm{k}| \\
	a_{\bm{k},\alpha}(t) &= a_{\bm{k},\alpha}^{(1)}\ \mathrm{e}^{-\mathrm{i} \omega t} +  a_{\bm{k},\alpha}^{(2)}\ \mathrm{e}^{\mathrm{i} \omega t}
\end{align*}

Daraus erhalten wir als Endergebnis
%
\begin{align*}
	\bm{A}(\bm{r},t) &=
	\sum_{\bm{k}} \sum_{\alpha=1,2}
	\sqrt{\frac{2 \pi \hbar c^2}{V \omega_\bm{k}}} \bm{\varepsilon}^{\alpha}
	\left[ a_{\bm{k},\alpha}(t)\ \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} + a_{\bm{k},\alpha}^*(t)\ \mathrm{e}^{-\mathrm{i} \bm{k} \cdot \bm{r}} \right]
\end{align*}
%
eingesetzt in die Energie
%
\begin{align*}
	H = \frac{1}{8 \pi} \int \diff \bm{r} \left( \bm{B} \cdot \bm{B} + \bm{E} \cdot \bm{E} \right) = \sum_\bm{k} \sum_\alpha \hbar \omega_\bm{k} a_{\bm{k},\alpha}^* a_{\bm{k},\alpha}
\end{align*}

Das klassische Strahlungsfeld kann also als eine Kollektion von harmonischen Oszillatoren für jedes $\bm{k}$ und für jede Polarisation aufgefasst werden.

\subsection{Quantisierung des Strahlungsfeldes}
\index{Quantisierung des Strahlungsfeldes}

In der eben hergeleiteten klassischen Form für $H$ gehen wir von den klassischen in die quantenmechanischen Größen über
%
\begin{align*}
	a_{\bm{k},\alpha}(t) &\to \hat{a}_{\bm{k},\alpha}(t) && \text{Vernichtungsoperator im Heisenberg-Bild} \\
	a_{\bm{k},\alpha}^*(t) &\to \hat{a}_{\bm{k},\alpha}^\dag(t) && \text{Erzeugungsoperator im Heisenberg-Bild}
\end{align*}
%
Für diese Operatoren werden die kanonischen Vertauschungsrelationen postuliert:
%
\begin{align*}
	\left[ \hat{a}_{\bm{k},\alpha}(t), \hat{a}_{\bm{k}',\alpha'}^\dag(t) \right] = \delta_{\bm{k},\bm{k}'} \delta_{\alpha,\alpha'}
\end{align*}
%
Damit wird das klassische Feld $\bm{A}(\bm{r},t)$ zu einem (Feld-)Operator $\hat{A}(\bm{r},t)$ im Heisenberg-Bild. Entsprechend werden $E \to \hat{E}$ und $B \to \hat{B}$ eingesetzt in die Energie
%
\begin{align*}
	\boxed{ H = \sum_k \sum_\alpha \hbar \omega_k \left[ \hat{a}_{\bm{k},\alpha}^\dag(t) \hat{a}_{\bm{k},\alpha}(t) + \frac{1}{2} \right] }
\end{align*}

\subsection{Fock-Zustände}
\index{Fock-Zustände}

Seien die Eigenzustände zu $H$ gegeben durch
%
\begin{align*}
	\Ket{\{n\}} = \Ket{n_{\bm{k}_1,\alpha_1},n_{\bm{k}_2,\alpha_2},n_{\bm{k}_3,\alpha_3},\ldots}
\end{align*}
%
wobei gilt $n_{\bm{k}_i,\alpha_i} \in \mathbb{N}^0$. Der Grundzustand wird auch als Vakuum bezeichnet und lautet
%
\begin{align*}
	\Ket{\{0\}} &= \Ket{0,0,0,\ldots}
\end{align*}

Der Erwartungswert der Energie im Grundzustand ist
%
\begin{align*}
	\Braket{\{0\}|H|\{0\}}
	&= \sum_{\bm{k},\alpha} \frac{\hbar \omega_k}{2} = \infty
\end{align*}

\paragraph{Das elektrische Feld:}
\begin{itemize}
	\item Erwartungswert
		\begin{align*}
			\Braket{ \{n\} | \hat{E}(\bm{r},t) | \{n\} }
			&= \begin{multlined}[t]
				\mathrm{i} \sum_\bm{k} \sum_\alpha \sqrt{\frac{2 \pi \hbar c^2}{V \omega_\bm{k}}} \bm{\varepsilon}^{\alpha}
				\left\langle \{n\} \middle| a_{\bm{k},\alpha}(t)\ \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \right. \\
				\left. + a_{\bm{k},\alpha}^\dag(t)\ \mathrm{e}^{-\mathrm{i} \bm{k} \cdot \bm{r}} \middle| \{n\} \right\rangle
			\end{multlined} \\
			&= 0
		\end{align*}

	\item Varianz
		\begin{align*}
			\Braket{ \{n\} | \hat{E}^2 | \{n\} }
			&\sim \sum_{\bm{k},\alpha} \hbar \omega_k \left( n_{\bm{k},\alpha} + \frac{1}{2} \right) \\
			&\neq 0
		\end{align*}
\end{itemize}
