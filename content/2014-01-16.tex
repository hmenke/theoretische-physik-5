% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 16.01.2014

\renewcommand{\printfile}{2014-01-16}

\chapter{Pfadintegraldarstellung}
\index{Pfadintegraldarstellung}

Neben der Schrödingerschen Wellenmechanik und der Heisenbergschen Matrizenmechanik existiert ein dritter äquivalenter Zugang zur Quantenmechanik.

Vorteile:
%
\begin{itemize}
	\item Große Anschaulichkeit
	\item Anwendbarkeit jenseits der Quantenmechanik, z.\,B.\ in der Quantenfeldtheorie bzw.\ der statistischen Physik.
\end{itemize}

\section{Definition}

\subsection{Schrödinger-Propagator}

\begin{figure}[htpb]
	\centering
	\def\a{1}
	\def\b{3}
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$t$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {${x}$};
		\draw (\a,0.1) -- (\a,-0.1) node[below] {$t_a$};
		\draw (\b,0.1) -- (\b,-0.1) node[below] {$t_b$};
		\draw (\a,1.0) node[dot,label={left:$x_a$}] (xa) {};
		\draw (\b,1.5) node[dot,label={right:$x_b$}] (xb) {};
		\draw[MidnightBlue] (xa) -- (xb);
		\draw[DarkOrange3] plot[smooth] coordinates {(xa) (1.5,0.8) (2,1.5) (2.5,0.5) (xb)};
		\draw[Purple] plot[smooth] coordinates {(xa) (1.5,1.8) (1.8,1.7) (2.2,2) (2.5,1.8) (2.8,2) (xb)};
	\end{tikzpicture}
	\caption{Mögliche Pfade zwischen zwei Punkten.}
	\label{fig:2014-01-16-1}
\end{figure}

Betrachte nun
%
\begin{align*}
	\ket{\psi(t_a)} &= \ket{x_a} \\
	\ket{\psi(t_b)} &= \exp\left( -\frac{\mathrm{i}}{\hbar} H (t_b - t_a) \right) \ket{x_a}
\end{align*}
%
Damit definieren wir den \acct{Schrödinger-Propagator}
%
\begin{align*}
	K^S(x_b,t_b,x_a,t_a)
	&\equiv \braket{x_b|\psi(t_b)} \\
	&= \Braket{x_b|\exp\left( -\frac{\mathrm{i}}{\hbar} H (t_b - t_a) \right)|x_a} \\
	&= \sum_n \psi_n(x_b) \exp\left( -\frac{\mathrm{i}}{\hbar} E_n (t_b - t_a) \right) \psi_n^*(x_a)
\end{align*}
%
wobei ausgenutzt wurde, dass $H \ket{\psi_n} = E_n \ket{\psi_n}$.

\subsection[Feynmans Idee]{Feynmans Idee (ca.\ 1940)}

Schreibe den Propagator als
%
\begin{align*}
	K^F(x_b,t_b,x_a,t_a)
	&= \sum_{\Gamma} \exp\left( \frac{\mathrm{i}}{\hbar} S[x(t)] \right)
\end{align*}
%
wobei $\Gamma$ die Menge aller Pfade ist, mit $x(t_a) = x_a$ und $x(t_b) = x_b$ und
\[ S[x(t)] = \int_{t_a}^{t_b} \diff t\ L(x,\dot{x},t) \]
die klassische Wirkung. Jeder Pfad trägt dann eine komplexe Amplitude bei.

\begin{example}
	Betrachte ein freies Teilchen in einer Dimension.
	%
	\begin{gather*}
		x_a = 0, t_a = 0 \\
		L = \frac{m}{2} \dot{x}^2
	\end{gather*}
	%
	Mit Hilfe der Euler-Lagrange-Gleichungen
	%
	\begin{align*}
		0 = \frac{\delta S[x(t)]}{\delta x(t)} = \frac{\diff}{\diff t} \frac{\partial L}{\partial \dot{x}} - \frac{\partial L}{\partial x} = m \ddot{x}
	\end{align*}
	%
	folgt der klassische Pfad
	%
	\begin{align*}
		x_\textnormal{cl}(t) = \frac{x_b}{t_b} t
	\end{align*}
	%
	mit der Wirkung
	%
	\begin{align*}
		S[x_\textnormal{cl}(t)] = \frac{m}{2} \frac{x_b^2}{t_b}
	\end{align*}

	\begin{figure}[htpb]
		\centering
		\begin{tikzpicture}
			\draw[->] (-0.1,0) -- (4,0) node[below] {$t$};
			\draw[->] (0,-0.1) -- (0,2) node[left] {${x}$};
			\draw (0,0.1) -- (0,-0.1) node[below] {$t_a$};
			\draw (3,0.1) -- (3,-0.1) node[below] {$t_b$};
			\draw (0,0) node[dot,label={left:$x_a$}] (xa) {};
			\draw (3,1.5) node[dot,label={right:$x_b$}] (xb) {};
			\draw[MidnightBlue] (xa) -- node[above left] {$\alpha = 1$} (xb);
			\draw (xa) .. controls (xa -| xb) .. node[pos=0.8,right] {$\alpha = 4$} (xb);
			\draw (xa) .. controls ++(2.2,0) .. node[above left] {$\alpha = 2$} (xb);
			\draw (xa) .. controls (xa |- xb) .. node[pos=0.8,above] {$\alpha = \frac{2}{3}$} (xb);
		\end{tikzpicture}
		\caption{Verschiedene Pfade zwischen den Punkten $(x_a,t_a)$ und $(x_b,t_b)$ in Abhängigkeit des Parameters $\alpha$.}
		\label{fig:2014-01-16-2}
	\end{figure}

	Andere Pfade mit diesen Randbedingen können parametrisiert werden mit, z.\,B.\
	%
	\begin{align*}
		x_\alpha(t) &= x_b \left( \frac{t}{t_b} \right)^\alpha \\
		\dot{x}_\alpha &= \frac{\alpha x_b}{t_b} \left( \frac{t}{t_b} \right)^{\alpha-1} \\
		\leadsto
		S[x_\alpha(t)] &= \frac{m}{2} \int_0^{t_b} \diff t\ \dot{x}^2 = \frac{m}{2} \frac{x_b^2}{t_b} \frac{\alpha^2}{2 \alpha - 1}
	\end{align*}

	\begin{figure}[htpb]
		\centering
		%\begin{tikzpicture}
		%	\begin{scope}
		%		\draw[->] (-0.1,0) -- (4,0) node[below] {$\alpha$};
		%		\draw[->] (0,-0.1) -- (0,2) node[left] {$S[x_\alpha]$};
		%		\foreach \x in {{1/2},1,2} {
		%			\draw (\x,0.1) -- (\x,-0.1) node[below] {$\x$};
		%		}
		%		\draw[dotted] (0.5,0) -- (0.5,2);
		%		\draw plot[smooth,domain=0.6:3.5] (\x,{(\x)^2/(2*\x - 1)});
		%	\end{scope}
		%	\begin{scope}[yshift=-2.5cm]
		%		\draw[->] (-0.1,0) -- (4,0) node[below] {$\alpha$};
		%		\draw[->] (0,-1.2) -- (0,1.5) node[left] {$\exp\left(-\frac{\mathrm{i}}{\hbar} S[x_\alpha]\right)$};
		%		\draw[dotted] (0.5,-1.2) -- (0.5,1.5);
		%		\draw plot[smooth,samples=41,domain=0.501:3.5] (\x,{cos((\x)^2/(2*\x - 1) r)});
		%	\end{scope}
		%\end{tikzpicture}
		\begin{tikzpicture}
			\begin{axis}[
					height=4cm,
					width=8cm,
					no markers,
					domain=0.6:3,
					xmin=0.4,
					axis y line=left,
					axis x line=middle,
					samples=101,
					xlabel={$\alpha$},
					ylabel={$S[x(t)]$},
				]
				\addplot[color=MidnightBlue] {((x^2)/(2*x-1))};
			\end{axis}
		\end{tikzpicture}
                
		\begin{tikzpicture}
			\begin{axis}[
					height=4cm,
					width=8cm,
					no markers,
					domain=0.501:3,
					xmin=0.4,
					axis y line=left,
					axis x line=middle,
					samples=1000,
					xlabel={$\alpha$},
					ylabel={$\exp\left(\frac{\mathrm{i}}{\hbar}S[x(t)]\right)$},
				]
				\addplot[color=MidnightBlue] {cos(deg((x^2)/(2*x-1)))};
			\end{axis}
		\end{tikzpicture}
		\caption{Wilde Oszillationen treten auf, sobald $S[x(t)]$ in die Größenordnung von $\hbar$ kommt. Bei Integration mitteln sich die Oszillationen weg, d.\,h.\ nur Pfade nahe des klassischen Pfades liefern einen Beitrag.}
		\label{fig:2014-01-16-3}
	\end{figure}

	Für makroskopische Objekte interferieren sich fast alle Pfade weg.
	%
	\begin{align*}
		m &= \SI{1}{\gram} \\
		x_b &= \SI{1}{\centi\metre} \\
		t_b &= \SI{1}{\second} \\
		\leadsto
		\Delta s &= \frac{\frac{1}{6}\cdot\num{e-3} \cdot \num{e-4}}{1} \approx \SI{e-8}{\joule\second}
	\end{align*}
	%
	(vgl.\ $\hbar \sim \SI{e-34}{\joule\second}$)

	Für ein Elektron
	%
	\begin{align*}
		m &= \SI{e-30}{\kilo\gram} \\
		\leadsto
		\Delta s &\approx \num{0.1}\ \hbar
	\end{align*}

	Für diese extrem langsamen Elektronen muss die konstruktive Interferenz zwischen $x_{\alpha=2}(t)$ und $x_\alpha(t)$ berücksichtigt werden.

	Quantale Objekte schöpfen fast alle Wege aus.
\end{example}

\subsection{Diskretisierung}

Das Zeitinterval zwischen $t_a$ und $t_b$ wird durch $N$ Zeitschritte der Länge $\varepsilon$ ersetzt. Nun wird der Pfad durch lineare Stücke angenähert, so wie in Abbildung~\ref{fig:2014-01-16-4}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$t$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {$x$};
		\foreach \x in {0.5,1.0,...,3.5} {
			\draw (\x,0.1) -- (\x,-0.1);
		}
		\path (0.5,-0.1) node[below left,rotate=45] {$t_a = t_0$} (3.5,-0.1) node[below left,rotate=45] {$t_b = t_N$};
		\foreach \y in {0.5,1.5} {
			\draw (0.1,\y) -- (-0.1,\y);
		}
		\path (-0.1,0.5) node[left] {$x_a$} (-0.1,1.5) node[left] {$x_b$};
		\draw[MidnightBlue] plot coordinates {(0.5,0.5) (1,2) (1.5,1) (2,1.2) (2.5,0.8) (3,1) (3.5,1.5)};
		\path[MidnightBlue] (0.5,0.5) node[dot] {} (3.5,1.5) node[dot] {};
	\end{tikzpicture}
	\caption{Lineare Interpolation eines Pfades.}
	\label{fig:2014-01-16-4}
\end{figure}

Die Wirkung des interpolierten Pfades geht nun vom Integral in eine Summe über
%
\begin{align*}
	S[\bm{x}(t)]
	&= \int_{t_0}^{t_N} \diff t \left( \frac{m}{2} \dot{\bm{x}}^2 + \frac{q}{c} \dot{\bm{x}} \cdot \bm{A} - V(\bm{x}) \right) \\
	&\to \sum_{j=1}^{N} \varepsilon \left( \frac{m}{2} \left( \frac{\bm{x}_{j} - \bm{x}_{j-1}}{\varepsilon} \right)^2 + \frac{q}{c} \left( \frac{\bm{x}_{j} - \bm{x}_{j-1}}{\varepsilon} \right) \cdot \bm{A} \left( \frac{\bm{x}_{j} - \bm{x}_{j+1}}{2} \right) - V\left( \frac{\bm{x}_{j} - \bm{x}_{j+1}}{2} \right) \right)
\end{align*}

Damit
%
\begin{align*}
	\sum_\textnormal{alle Pfade} = \lim_{\substack{\varepsilon \to 0 \\ N \to \infty \\ \text{mit } N \varepsilon = t_b - t_a}} \mathcal{A}_N \int \diff \bm{x}_1 \int \diff \bm{x}_2 \ldots \int \diff \bm{x}_{N-1}
\end{align*}
%
wobei $\mathcal{A}_N$ eine Normierung ist. Dies sind $N-1$ Integrale über den Konfigurationsraum $\mathbb{R}^2$.

\subsection{Freies Teilchen}

Damit gilt für ein freies Teilchen
%
\begin{align*}
	K_N^F(x_N,t_n,x_0,t_0)
	&= \mathcal{A}_N \int_{-\infty}^{\infty} \diff x_1 \ldots \int_{-\infty}^{\infty} \diff x_{N-1} \exp\left( \frac{\mathrm{i}}{\hbar} \frac{m}{2 \varepsilon} \sum_{j=1}^{N} (x_j - x_{j-1})^2 \right)
	\intertext{Substituiere $y_i = x_i \sqrt{\frac{m}{2 \varepsilon \hbar}}$}
	&= \mathcal{A}_N \left( \frac{2 \varepsilon \hbar}{m} \right)^{\frac{N-1}{2}} \underbrace{\int_{-\infty}^{\infty} \diff y_1 \ldots \int_{-\infty}^{\infty} \diff y_{N-1} \exp\left( \mathrm{i} \sum_{j=1}^{N} (y_j - y_{j-1})^2 \right)}_{I_N(y_N-y_0)}
\end{align*}
%
Durch sukzessive Integration dieser $N$ Gaussintegrale (oder per vollständiger Induktion) erhält man
%
\begin{align*}
	I_N(y_N-y_0) = (\mathrm{i} \pi)^{\frac{N-1}{2}} \frac{1}{\sqrt{N}} \exp\left( \mathrm{i} \frac{(y_N - y_0)^2}{N} \right)
\end{align*}
%
Eingesetzt in den Propagator und unter Verwendung von $N = (t_N - t_0)/\varepsilon$
%
\begin{align*}
	K^F
	&= \mathcal{A}_N \left( \frac{2 \mathrm{i} \pi \varepsilon \hbar}{m} \right)^{N/2} \underbrace{\sqrt{\frac{m}{2 \pi \mathrm{i} \hbar}} \frac{1}{\sqrt{t_N - t_0}} \exp\left( \frac{\mathrm{i} m}{2 \hbar} \frac{(x_N - x_0)^2}{t_N - t_0} \right)}_{{}= K^S} \\
	\mathcal{A}_N &= \left( \frac{m}{2 \mathrm{i} \pi \varepsilon \hbar} \right)^{N/2} = \left( \frac{1}{B_\varepsilon} \right)^N
\end{align*}
