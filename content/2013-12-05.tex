% Henri Menke 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 05.12.2013

\renewcommand{\printfile}{2013-12-05}

\section[Wechselwirkung von Materie und Photonen]{Wechselwirkung von Materie und Photonen (nicht relativistisch)}

\subsection{Hamiltonoperator}

Der Hamiltonoperator des Systems lautet
%
\begin{align*}
	H &= H_0 + H_\textnormal{WW} + H_\textnormal{EM}
\intertext{mit}
H_0 &= \sum_i \frac{\bm{p}_i^2}{2 m_i} + \sum_{i < j} \frac{q_i q_j}{4 \pi |\bm{r}_i - \bm{r}_j|} + V^\textnormal{ext}(\{\bm{r}_i\})
\end{align*}

Mit \enquote{minimaler Kopplung}
%
\begin{align*}
	\bm{p}_i \to \bm{p}_i- \frac{q}{c} \bm{A}(\bm{r}_i)
\end{align*}
%
setzt sich der Wechselwirkungsterm zusammen aus
%
\begin{align*}
	H_\textnormal{WW} &= H_1 + H_2 \\
	H_1 &= \sum_i \frac{e}{2 m c} (\bm{p}_i \bm{A}(\bm{r}_i) + \bm{A}(\bm{r}_i) \bm{p}_i) = \sum_i \frac{e}{m c} \bm{A}(\bm{r}_i) \bm{p}_i \\
	H_2 &= \frac{e^2}{2 m c^2} \sum_i \bm{A}(\bm{r}_i)^2
\end{align*}
%
wobei der Index $i$ über alle Elektronen läuft. $H_1$ ausgeschrieben für ein Elektron:
%
\begin{align*}
	H_1 &= \frac{e}{m c} \sum_{\bm{k},\alpha} \underbrace{\sqrt{\frac{2 \pi \hbar c^2}{V \omega_\bm{k}}}}_{\gamma_\bm{k}} \bm{\varepsilon}^\alpha \bm{p}_i \Bigl(
	\underbrace{a_{\bm{k},\alpha} \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}_i}}_{H_1^\textnormal{Absorption}}
		+ \underbrace{a_{\bm{k},\alpha}^\dag \mathrm{e}^{-\mathrm{i} \bm{k} \cdot \bm{r}_i}}_{H_1^\textnormal{Emission}}
	\Bigr) \\
	&= \frac{e}{m c} \sum_{\bm{k},\alpha} \gamma_\bm{k} (H_1^\textnormal{Absorption} + H_1^\textnormal{Emission})
\end{align*}
%
Dieser Operator ändert die Photonenzahl im Stahlungsfeld um $\pm 1$.

\begin{align*}
	H_2 &=
	\begin{multlined}[t]
		\frac{e^2}{2 m c^2} \sum_{\bm{k},\alpha} \sum_{\bm{k}',\alpha'} \gamma_\bm{k} \gamma_{\bm{k}'} \bm{\varepsilon}^{\alpha} \bm{\varepsilon}^{\alpha'}
		\Bigl[
			a_{\bm{k},\alpha} a_{\bm{k}',\alpha'} \mathrm{e}^{\mathrm{i} (\bm{k} + \bm{k}') \cdot \bm{r}_i}
			+ a_{\bm{k},\alpha} a_{\bm{k}',\alpha'} \mathrm{e}^{\mathrm{i} (\bm{k} - \bm{k}') \cdot \bm{r}_i} \\
			+ a_{\bm{k},\alpha}^\dag a_{\bm{k}',\alpha'} \mathrm{e}^{\mathrm{i} (- \bm{k} + \bm{k}') \cdot \bm{r}_i}
			+ a_{\bm{k},\alpha}^\dag a_{\bm{k}',\alpha'}^\dag \mathrm{e}^{- \mathrm{i} (\bm{k} + \bm{k}') \cdot \bm{r}_i}
		\Bigr]
	\end{multlined} \\
	&= \frac{e^2}{2 m c^2} \sum_{\bm{k},\alpha} \sum_{\bm{k}',\alpha'} \gamma_\bm{k} \gamma_{\bm{k}'} (H_2^\textnormal{Abs,Abs} + H_2^\textnormal{Abs,Em} + H_2^\textnormal{Em,Abs} + H_2^\textnormal{Em,Em})
\end{align*}
%
Dieser Operator ändert die Photonenzahl im Stahlungsfeld um $0, \pm 2$.

\subsection{Matrixelemente und Dipolnäherung}

Der Hilbertraum der Materie-Zustände besitzt die Elemente $\{ \ket{a}, \ket{b}, \dotsc \}$, die in den Quantenzahlen $n,\ell,m$ organisiert sind. Der Hilbertraum der Photonen besitzt die Zustände $\{ \ket{\dotsc,n_{\bm{k},\alpha},\dotsc} \}$.
Das Gesamtsystem besteht also aus Zuständen der Form $\ket{A} = \ket{a} \otimes \ket{\{ n_{\bm{k},\alpha} \}}, \ket{B} = \ket{b} \otimes \ket{\{ n_{\bm{k},\alpha} \}}, \dotsc$

Betrachten wir die Absorption eines Photons aus $\ket{A}$. Dazu berechnen wir das Matrixelement des Übergangs.
%
\begin{align*}
	\braket{B|H|A}
	&= \braket{B|H_1^\textnormal{Abs}|A} \\
	&= \braket{b,n_{\bm{k},\alpha}-1|H_1^\textnormal{Abs}|a,n_{\bm{k},\alpha}} \\
	&= \frac{e}{m c} \gamma_{\bm{k}} \braket{b,n_{\bm{k},\alpha}-1|a_{\bm{k},\alpha} \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \bm{\varepsilon}^\alpha \bm{p}|a,n_{\bm{k},\alpha}} \\
	&= \frac{e}{m c} \gamma_{\bm{k}} \sqrt{n_{\bm{k},\alpha}} \braket{b|\mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \bm{\varepsilon}^\alpha \bm{p}|a}
\end{align*}

Für die Emission verläuft die Rechnung analog:
%
\begin{align*}
	\braket{B|H_1^\textnormal{Em}|A}
	&= \braket{b,n_{\bm{k},\alpha}+1|H_1^\textnormal{Em}|a,n_{\bm{k},\alpha}} \\
	&= \frac{e}{m c} \gamma_{\bm{k}} \sqrt{n_{\bm{k},\alpha} + 1} \braket{b|\mathrm{e}^{-\mathrm{i} \bm{k} \cdot \bm{r}} \bm{\varepsilon}^\alpha \bm{p}|a}
\end{align*}

\begin{theorem}[Dipolnäherung]
	Für $r \approx \SI{1}{\angstrom}$ und $k = \frac{2 \pi}{\lambda} \approx \frac{6}{\SI{600}{\nano\m}}$ gilt \[ \bm{k} \cdot \bm{r} \approx \num{e-3} \]
	Dann kann man entwickeln
	%
	\begin{align*}
		\braket{b|\mathrm{e}^{\pm \mathrm{i} \bm{k} \cdot \bm{r}} \bm{\varepsilon}^\alpha \bm{p}|a}
		&\approx \braket{b|\left( 1 \pm \mathrm{i} \bm{k} \cdot \bm{r} - \frac{1}{2} (\bm{k} \cdot \bm{r})^2 + \ldots \right) \bm{\varepsilon}^\alpha \bm{p}|a} \\
		&\approx \braket{b|\bm{\varepsilon}^\alpha \bm{p}|a}
	\intertext{mit $\bm{p} = - \frac{1}{2\mathrm{i}\hbar} [\bm{p}^2,\bm{r}] = - \frac{m}{\mathrm{i} \hbar} [H_0,\bm{r}]$ wobei $H_0 = \frac{\bm{p}^2}{2 m} + V(\bm{r})$}
	&= - \frac{m}{\mathrm{i}\hbar} \braket{b|H_0 \bm{r} \bm{\varepsilon}^\alpha - \bm{r} \bm{\varepsilon}^\alpha H_0|a} \\
	&= \frac{\mathrm{i} m}{\hbar} (E_b - E_a) \braket{b|\bm{r} \bm{\varepsilon}^\alpha|a}
	\end{align*}
	%
	Der Term $\braket{b|\bm{r} \bm{\varepsilon}^\alpha|a}$ ist auch besser bekannt als \acct{Dipolmatrixelement}.
\end{theorem}

\subsection{Übergangsraten}

Störungsrechnung nach einer Kopplungskonstanten $\lambda = \frac{e^2}{\hbar c} = \frac{1}{137} \equiv \alpha$ (Feinstrukturkonstante) ergibt
%
\begin{align*}
	\Gamma_{A \to B} &= \frac{2 \pi}{\hbar} |M_{BA}|^2 \varrho(E_B)|_{E_B=E_A}
	\intertext{mit}
	M_{BA} &= \braket{B|V|A} + \sum_J \frac{\braket{B|V|J}\braket{J|V|A}}{E_A + E_J + \mathrm{i} \eta} + \mathcal{O}(V^3)
\end{align*}

\subsection{Beispiel}

Spontane Emission von ($2p$, $m=0$) beim Wasserstoffatom. Die Emission findet in $\bm{k}$-Richtung statt mit Polarisation $\bm{\varepsilon}^\alpha$ mit
%
\begin{align*}
	\bm{\varepsilon}^{(1)} \bm{r} &= x \\
	\bm{\varepsilon}^{(2)} \bm{r} &= x \cos\vartheta - z \sin\vartheta
\end{align*}
%
Dann lautet das Übergangsmatrixelement
%
\begin{align*}
	\sum_{\alpha=1,2} |\braket{0,0|\bm{r} \bm{\varepsilon}^\alpha|\ell=1,m=0}|^2
	&= \sin^2\vartheta |\braket{0,0|z|1,0}|^2 \\
	&= 4 \sqrt{2} \left( \frac{2}{3} \right)^5 a_0 \sin^2\vartheta
\end{align*}
%
Damit ergibt sich die Übergangsrate
%
\begin{align*}
	\Gamma_{2p \to 1s}
	&= \begin{multlined}[t]
		\frac{2\pi}{\hbar} \int_0^{2\pi} \diff\varphi_k \int_0^\pi \diff(\cos\vartheta_k) \int_0^\infty k^2 \diff k\ \frac{e^2}{m c^2} \left( \frac{2 \pi \hbar c^2}{V \omega_k} \right) \frac{m^2}{\hbar^2} \\
		\cdot \underbrace{(E_b - E_a)^2}_{\hbar^2 \omega^2} \sin^2\vartheta M^2 \frac{\delta(k - \frac{\omega}{c})}{\hbar c}
	\end{multlined} \\
	&= \frac{4}{3} \frac{e^2 \omega^2}{\hbar c} M^2 \\
	&= (\SI{1.6e-9}{\s})^{-1}
\end{align*}
%
Der Übergang findet also auf der Nanosekunden-Skala statt.
